# CQRS/Event sourcing: EventStore, Eventbus en Commandbus

Een beschrijving van de interface en de werking van een **framework** voor 
**CQRS** (Command Query Responsibility Segregation) en **event sourcing**.

## Event sourcing

Event sourcing is een methode waarbij alle wijzigingen in applicatie status worden opgeslagen
als een serie van gebeurtenissen. Hierdoor kan de status van (een deel van) de applicatie op elk tijdstip
worden hersteld door de gebeurtenissen opnieuw af te spelen vanaf het begin tot het gewenste tijdstip.
Dit heeft voor- en nadelen die hier niet worden besproken, omdat de focus hier ligt op de beschrijving
van het framework.

Vanaf nu zullen alle termen in het Engels gebruikt worden, omdat het framework daar ook gebruik
van maakt dus:

* gebeurtenis &rarr; Event
* gegevens object &rarr; Aggregate

### Aggregates & Events

De status van alle aggregate instanties samen bepaalt de status van de applicatie. 

Elke aggregate instantie heeft een *aggregate type* en *id*. 
Het *aggregate type* bepaalt het soort status dat de aggregate instantie bevat, het *id* 
identificeert een specifiek item van dat soort status. De combinatie *aggregate type* en
*id* is uniek, van elke combinatie kan er maximaal één zijn.

Een event instantie heeft een *event type* en *id*. 
Een event instantie met een bepaald *id* geeft de wijziging in status aan van 
alle aggregate instanties (van verschillende *aggregate types*) met hetzelfde *id*. 
Het kan zijn dat een bepaald type event (soort wijziging) niet van toepassing is op een aggregate type
omdat de aggregate dat soort status niet bevat. De aggregate negeert het event type dan ook al is
het *id* gelijk. 

Alle event instanties die ooit zijn opgetreden worden opgeslagen.
Wanneer een *aggregate type* met bepaald *id* nodig is dan wordt een nieuwe aggregate instantie 
van het *aggregate type* aangemaakt en worden alle event opgeslagen instanties met het gespecificeerde *id* 
afgespeeld op de nieuwe aggragete instantie (Het *aggregate type* bepaalt welke *event type*s worden
afgehandeld en welke worden genegeerd). Nadat alle event instanties zijn afgespeeld bevat 
de Aggregate instantie de laatste status.

### EventStore

Events zijn leidend in een event sourcing applicatie.
Voor het opslaan van event instanties is een opslag nodig: een EventStore. De EventStore in het framework
kent de volgende functionaliteit:

* `fun accept(event: Event)`
* `fun retrieve(aggregateClass: KClass<out Aggregate>, id: DataId): Aggregate`

De programmeertaal die gebruikt wordt om de outline van het framework te demonstreren is Kotlin, omdat
een implementatie van het framework ook in Kotlin is geschreven. De interface van de EventStore 
maakt gebruik van een aantal definities:

* `typealias DataId`: een alias voor het data type dat wordt gebruikt als ID voor, onder andere, event
instanties en aggregate instanties. In het framework is dat `UUID`.
* `abstract class DataWithId(val id: DataId)`: een object met een DataId.
* `abstract class Message(id: DataId): DataWithId(id)`: basis klasse voor alle Messages.
* `abstract class Event(id: DataId): Message(id)`: een Event is een gebeurtenis die heeft plaatsgevonden op een
`MessageHandlingObject`. Het ID van het Event bepaalt op welke MessageHandlingObject instantie het Event 
heeft plaatsgevonden, namelijk de MessageHandlingObject instantie met hetzelfde ID. 
* `abstract class MessageHandlingObject(id: DataId): DataWithId(id)`: basis klasse voor gegevens objecten 
die Messages (zoals Events) kunnen afhandelen. Ze hebben meestal status die wordt beïnvloed door Messages.
* `abstract class Aggregate(id: DataId): MessageHandlingObject(id)`: een gegevens object dat status heeft. 
Elke instantie van het gegevens object heeft een eigen ID en eigen status (gegevens).

#### accept

De `accept` methode dient om event instanties toe te voegen aan de EventStore.
De event instanties worden opgeslagen op volgorde van aanroep van de `accept` methode.

#### retrieve

De `retrieve` methode dient om de laatste status van een aggregate instantie op te halen.
De parameters van de `retrieve` methode geven aan om welke aggregate instantie het gaat (type en id).
Het resultaat van de methode is een aggregate instantie van het gespecificeerde
aggregate type en id, met de laatste status.

### @EventSourcingHandler

Wanneer de `retrieve` methode wordt aangeroepen moet het framework weten hoe de aggregate instantie
event instanties met hetzelfde id verwerkt.
Een voorbeeld van een event en een aggregate definitie:

```kotlin
class NewEvent(id: DataId [, more properties]): Event(id) {
   ... implementation if necessary ...
}

class SomeAggregate(id: DataId): Aggregate(id){
    ...
    @EventSourcingHandler
    fun handleNewEvent(event: NewEvent [, more parameters]) {
        ... event afhandelen: Aggregate status aanpassen ...
    }
}
```

Het event genaamd 'NewEvent' is een afgeleide van de `abstract class Event`.
'NewEvent' is het type van het Event.

De aggregate genaamd 'SomeAggregate' is een afgeleide van de
`abstract class Aggregate`, 'SomeAggregate' is het type van de Aggregate. 
Hij heeft een methode 'handleNewEvent' die gemarkeerd is met
`@EventSourcingHandler`. Hij heeft als eerste parameter een event van het type 'NewEvent'. 

Dit betekent dat, als bij het rebuilden van een aggregate instantie een event instantie van het type 'NewEvent' 
(of subtype daarvan) moet worden verwerkt, de methode 'handleNewEvent' wordt aangeroepen 
met het 'NewEvent' als eerste parameter. 
Als de aggregate definitie geen event sourcing handler voor het type NewEvent heeft 
wordt het genegeerd bij rebuilden, ook als de *id* gelijk is aan die van de aggregate instantie. 

Het framework weet dat de aggregate definitie het NewEvent kan verwerken doordat de eerste parameter 
van de 'handleNewEvent' methode van het type NewEvent is, de naam van de methode heeft geen invloed daarop.

Een aggregate definitie kan meerdere methodes hebben met *@EventSourcingHandler* annotatie, 
omdat verschillende typen events verschillende afhandeling kunnen vereisen.

Het is de bedoeling dat *@EventSourcingHandler* handlers de status van de aggregate instantie bijwerken aan de hand
van de gegevens in de event instantie.

Het opslaan van event instanties met de `accept` methode en weer rebuilden van aggregate instanties
met de `retrieve` methode is, in het framework, de basis voor event sourcing.

## Commands & Events

### Eventbus

In de voorafgaande beschrijving worden events alleen gebruikt om op te slaan in de EventStore en om
aggregate instanties te rebuilden, het zogenaamde event sourcing, een aggregate handelt events alleen af als 
de `retrieve` methode wordt aangeroepen. In een applicatie kan het ook handig zijn dat een aggregate een
event meteen afhandelt als het optreedt om side effects uit te voeren (meestal communicatie met 
andere applicaties), bijvoorbeeld updaten van
een ander systeem via een REST aanroep, of versturen van een email. Deze afhandeling is eenmalig, en moet
niet plaatsvinden bij het rebuilden van een aggregate isntantie. Eenmalige afhandeling van events wordt in het 
framework door een Eventbus uitgevoerd. De Eventbus is als volgt gedefiniëerd:

```kotlin
interface EventBus {
    fun publish(event: Event)
}
```

Op de Eventbus gepubliceerde event instanties worden als volgt afgehandeld door de Eventbus
(wanneer een event instantie op de Eventbus wordt gepubliceerd is hij nog niet opgeslagen in de EventStore):

1. De Eventbus zoekt uit door welke aggregate types de event instantie wordt afgehandeld,  
1. Voor ieder aggregate type:
   1. Rebuild de aggregate instantie,
   1. Roep de eventhandler op de aggregate instantie aan (excepties in de eventhandler worden
      door het framework opgevangen en kunnen via een Future worden opgevraagd nadat de
      event instantie is opgeslagen in de EventStore),
1. Sla de event instantie op in de EventStore.

### @EventHandler

De eventhandler die wordt aangeroepen na de rebuild van de aggregate instantie is niet dezelfde
eventhandler als die gemarkeerd met `@EventSourcingHandler`. Deze eventhandler moet alleen
worden aangeroepen wanneer een event instantie wordt *gepubliceerd* op de Eventbus en niet
bij rebuilden (vanwege mogelijke side-effects). Daarom is er een aparte annotatie om 
dit soort eventhandlers te markeren: `@EventHandler`. Deze eventhandlers worden ook
gemarkeerd in de aggregate definitie. In andere frameworks wordt dit soort eventhandlers soms
in een ander soort types aangegeven: 'reactors'. In dit framework staan ze bij de aggregate definitie.

```kotlin
class NewEvent(id: DataId [, more properties]): Event(id) {
   ... implementation if necessary ...
}

class SomeAggregate(id: DataId): Aggregate(id){
    ...
    @EventSourcingHandler
    fun handleNewEvent(event: NewEvent [, more parameters]) {
        ... event afhandelen: Aggregate status aanpassen ...
    }
    @EventHandler
    fun handleNewEventSideEffects(event: NewEvent [, more parameters]) {
        ... handle the event: Side effects ...
    }
}
```

De methode 'handleNewEvent' wordt aangeroepen iedere keer bij het rebuilden van een aggregate instantie. De methode
'handleNewEventSideEffects' wordt aangeroepen wanneer een NewEvent wordt gepubliceerd op de Eventbus.

Stel de EventStore bevat event1 van het type NewEvent en de applicatie publiceert event2 van het type
NewEvent op de Eventbus, dan
zorgt de Eventbus voor het rebuilden van de SomeAggregate instantie door event1 aan 'handleNewEvent' te geven
(en eventueel meer parameters). Daarna is het rebuilden klaar en wordt event2 aan 'handleNewEventSideEffects'
gegeven. Een event instantie die aan *@EventHandler*s wordt gegeven is dus nog niet verwerkt door de
*@EventSourcingHandler*s. Wanneer event3 van het type NewEvent wordt gepubliceerd rebuild SomeAggregate instantie
met event1 en event2, daarna wordt event3 aan de *@EventHandler*s gegeven. 

Events met hetzelfde id worden geserialiseerd afgehandeld.

Door het publiceren van event instanties met de `publish` methode van de Eventbus 
en weer rebuilden van aggregate instanties met de `retrieve` methode van de EventStore
wordt het framework uitgebreid met het meteen afhandelen van events.

### Commandbus

Events zijn interne wijzigingen in de applicatie status. Ze werken applicatie status bij
via *@EventSourcingHandler*s en kunnen externe (eenmalige) effecten hebben via *@EventHandler*s.
Het zijn gebeurtenissen die effect hebben binnen en naar buiten toe.
De meeste applicaties reageren ook op gebeurtenissen van buiten af. Andere applicaties kunnen de applicatie
aanroepen via bijvoorbeeld een REST API.

Reageren op gebeurtenissen van buiten af wordt in het framework door een Commandbus uitgevoerd.
De Commandbus is als volgt gedefinieerd:
 
```kotlin
abstract class Command(id: DataId): Message(id)

interface CommandBus {
    fun handle(command: Command)
}
```

`Command`s zijn bedoelingen om wijzigingen in de applicatie status aan te brengen. 
Een command instantie heeft een *command type* en *id* dat bepaalt op welke aggregate instantie
de command instantie van toepassing is. 
Een command instantie met een bepaald *id* kan van toepassing zijn op 
alle aggregate instanties (van verschillende *aggregate types*)
met hetzelfde *id*. De aggregate definitie bepaalt aan de hand van het *command type* of 
hij de command instantie afhandelt of negeert.

Op de Commandbus gestuurde command instanties worden als volgt afgehandeld door de Commandbus:

1. De Commandbus zoekt uit door welke aggregate types de command instantie wordt afgehandeld,  
1. Voor ieder aggregate type:
   1. Rebuild de aggregate instantie,
   1. Roep de commandhandler op de aggregate instantie aan (excepties in de commandhandler worden
      door het framework opgevangen en kunnen via een Future worden opgevraagd nadat het 
      command is afgehandeld).
      
### @CommandHandler      

Commandhandlers op aggregate definities worden aangegeven met de annotatie `@CommandHandler`.
Een voorbeeld van een *@CommandHandler* in een aggregate definitie:

```kotlin
class NewCommand(id: DataId [, more properties]): Command(id) {
   ... implementation if necessary ...
}

class NewEvent(id: DataId [, more properties]): Event(id) {
   ... implementation if necessary ...
}

class SomeAggregate(id: DataId): Aggregate(id){
    ...
    @EventSourcingHandler
    fun handleNewEvent(event: NewEvent [, more parameters]) {
        ... event afhandelen: Aggregate status aanpassen ...
    }
    @EventHandler
    fun handleNewEventSideEffects(event: NewEvent [, more parameters]) {
        ... event afhandelen: Side effects ...
    }
    @CommandHandler
    fun handleNewCommand(command: NewCommand [, more parameters]) {
        ... command afhandelen: events/commands publiceren ...
    }
}
```

Stel de EventStore bevat event1 van het type NewEvent en de applicatie publiceert command1 van het type
NewCommand op de Commandbus, dan
zorgt de Commandbus voor het rebuilden van de SomeAggregate instantie door event1 aan 'handleNewEvent' te geven
(en eventueel meer parameters). Daarna is het rebuilden klaar en wordt command1 aan 'handleNewCommand'
gegeven. 'handleNewCommand' kan nu:

1. Events publiceren: Via Eventbus::publish worden events gepubliceerd.
1. Commands publiceren: Via Commandbus::handle worden commands gepubliceerd.

Commands met hetzelfde id worden geserialiseerd afgehandeld.

Door het publiceren van command instanties met de `handle` methode van de Commandbus 
wordt het framework uitgebreid met het reageren op gebeurtenissen van buiten af.

# CQRS/Event sourcing: Projecties en Queries

Applicaties werken met gegevens die nodig zijn:
 
1. om beslissingen te nemen bij verwerking van commands en events,
1. om naar buiten toe te ontsluiten, via een REST api of events. 

Het ligt voor de hand om voor beide toepassingen de eerder gedefinieerde aggregates te nemen, 
maar het is beter om dat niet te doen. Eén van de redenen daarvoor is:
scheiding van verantwoordelijkheden.

CQRS (Command Query Responsibility Segregation) is een methode waarbij opslaan en opvragen van
gegevens door verschillende data modellen wordt gedaan. Het framework biedt hier geen speciale
faciliteiten voor, maar de Commandbus, Eventbus en EventStore maken het wel makkelijk om
zelf te implementeren.

Voor de scheiding van het opslaan en opvragen van gegevens worden vaak de termen 
'projections' en 'queries' gebruikt.

## Projecties

Update & @EventHandler.

## Queries

# CQRS/Event sourcing: Optimalisatie

Projecties en queries worden in het framework gebruikt om optimalisatie toe te passen op de EventStore.

Tekortkomingen & oplossingen.

## Caching

## Snapshots

## Virtuele Archivering
