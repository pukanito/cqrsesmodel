/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.util.isDataId
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.filter
import java.util.logging.Logger
import javax.inject.Inject
import kotlin.reflect.KFunction

/**
 * An [AggregateRebuilder] that caches nothing, it always creates new [Aggregate]s.
 * It can replay [Event]s on [Aggregate]s.
 *
 * @param replayer an [AggregateMessageReplayer].
 * @param logger a [Logger].
 */
class NoMemoryAggregateRebuilder @Inject constructor(private val replayer: AggregateMessageReplayer,
                                                     private val logger: Logger): AggregateRebuilder {

    private fun KFunction<*>.hasExactlyOneDataIdParameter() = parameters.size == 1 && parameters.any { it.isDataId() }

    override suspend fun get(eventStorage: EventStorage, aggregateClass: AggregateClass, id: DataId): Aggregate {
        logger.fine { "$aggregateClass::$id" }
        return coroutineScope {
            val deferredAggregate = async {
                aggregateClass.constructors.first { it.hasExactlyOneDataIdParameter() }.call(id) }
            replayer.replay(deferredAggregate, eventStorage.get().filter { it.id == id }) }
    }

}
