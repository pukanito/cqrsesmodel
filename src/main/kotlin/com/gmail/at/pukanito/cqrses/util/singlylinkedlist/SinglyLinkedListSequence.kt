package com.gmail.at.pukanito.cqrses.util.singlylinkedlist

import com.gmail.at.pukanito.cqrses.Position

/**
 * @param position the starting [Position] in the list for the [Sequence].
 * @return a [Sequence] starting at the specified [Position] till the end of the list.
 */
fun <T> SinglyLinkedList<T>.asSequence(position: Position<*>? = null): Sequence<T> =
    Sequence { iterator(position) }.map { it.first }

/**
 * @param count the number of items in the [Sequence]. 0 <= everything.
 * @param position the starting [Position] in the list for the [Sequence].
 * @return a [Sequence] consisting of count elements starting at the specified [Position]. If the are not
 *         enough remaining items in the list only the remaining number of items are returned.
 */
fun <T> SinglyLinkedList<T>.asSequence(count: Int, position: Position<*>? = null): Sequence<Pair<T, Position<*>>> =
    Sequence { iterator(if (count <= 0) Int.MAX_VALUE else count, position) }
