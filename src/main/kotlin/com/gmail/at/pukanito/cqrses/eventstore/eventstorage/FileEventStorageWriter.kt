/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage

import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.Position
import java.io.Closeable
import java.util.function.Consumer

interface FileEventStorageWriter: Consumer<Event>, Closeable {

    /**
     * @return the current size of the Storage.
     */
    fun size(): Long

    /**
     * Return the current [Position] of the [FileEventStorageWriter].
     *
     * @return the current [Position] of the [FileEventStorageWriter].
     */
    fun getPosition(): Position<*>

}
