/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorageReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileWriter.EventFilePosition
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFileWriter.RotatingEventFilePosition
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFileWriter.RotatingEventFilePositionSpec
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flattenConcat
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.zip
import java.nio.file.Path
import javax.inject.Inject

/**
 * Reads [Event]s from rotating files.
 *
 * @param rotatingEventFile rotation helper.
 * @param dispatcher CoroutineDispatcher for providing the Event Flow.
 */
class RotatingEventFileReader @Inject constructor(private val rotatingEventFile: RotatingEventFile,
                                                  private val dispatcher: CoroutineDispatcher? = null) :
    FileEventStorageReader {

    @Suppress("EXPERIMENTAL_API_USAGE")
    private fun <T> Flow<T>.dispatch(): Flow<T> =
        if (dispatcher == null) this else this.flowOn(dispatcher)

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun get(): Flow<PositionedEvent> =
        rotatingEventFile.getEventFilePaths().asFlow().map { getReader(it.second).get() }
            .flattenConcat()
            .dispatch()

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun get(position: Position<*>): Flow<PositionedEvent> {
        if (position is RotatingEventFilePosition) return positionedGet(position.get()).dispatch()
        throw java.lang.IllegalStateException("Cannot handle Position: ${position.javaClass.canonicalName}")
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun get(count: Int, position: Position<*>?): Flow<PositionedEvent> {
        return when (position) {
            null -> positionedGetWithPosition(RotatingEventFilePositionSpec(0, EventFilePosition(0)))
            is RotatingEventFilePosition -> positionedGetWithPosition(position.get())
            else -> throw IllegalStateException("Cannot handle Position: ${position.javaClass.canonicalName}")
        }.let { if (count <= 0) it else it.take(count) }.dispatch()
    }

    private fun getReader(path: Path) = EventFileReader(path)

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun positionedGet(position: RotatingEventFilePositionSpec): Flow<PositionedEvent> =
        rotatingEventFile
            .getEventFilePaths(fileNumFilter = { it >= position.fileNum })
            .asFlow()
            .zip(positionFlow(position)) { pathSpec, pos: EventFilePosition -> getReader(pathSpec.second).get(pos) }
            .flattenConcat()

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun positionedGetWithPosition(position: RotatingEventFilePositionSpec): Flow<PositionedEvent> =
        rotatingEventFile
            .getEventFilePaths(fileNumFilter = { it >= position.fileNum })
            .asFlow()
            .zip(positionFlow(position)) { pathSpec, pos: EventFilePosition -> getReader(pathSpec.second)
                .get(0, pos)
                .map { PositionedEvent(it.event,
                    RotatingEventFilePosition(RotatingEventFilePositionSpec(
                        pathSpec.first, it.position as EventFilePosition))) } }
            .flattenConcat()

    private fun positionFlow(position: RotatingEventFilePositionSpec): Flow<EventFilePosition> =
        flow {
            emit(position.position)
            while (true) emit(EventFilePosition(0))
        }

}
