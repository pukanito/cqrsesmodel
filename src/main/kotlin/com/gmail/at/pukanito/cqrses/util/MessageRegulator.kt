/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util

import com.gmail.at.pukanito.cqrses.Message
import java.util.concurrent.atomic.AtomicBoolean
import java.util.function.Consumer

/**
 * A message consumer that regulates handling of accepted messages.
 *
 * The message regulator accepts incoming messages and calls a consumer for handling them.
 * It will have the consumer handle messages one by one. When the consumer is busy,
 * new incoming messages will be added to a message queue and be handled when the
 * consumer has finished handling the current message. Hence a consumer that adds
 * messages to the queue, will have those messages handled after the consumer has finished.
 *
 * @param handler the consumer for handling messages.
 * @param messageQueue the message queue to use.
 */
abstract class MessageRegulator<T>(private val handler: Consumer<T>,
                                   private val messageQueue: MessageQueue<T>
): Consumer<T> where T: Message {

    private val busy = AtomicBoolean(false)

    /**
     * Accept a new message for handling. Handling may not have been performed yet when the method returns.
     *
     * @param message the message to be handled.
     */
    override fun accept(message: T) {
        messageQueue.add(message)
        handleQueueIfNotBusy()
    }

    private fun handleQueueIfNotBusy() {
        if (busy.compareAndSet(false, true)) handleQueueAndResetBusy()
    }

    private fun handleQueueAndResetBusy() = try { handleQueue() } finally { busy.set(false) }

    private fun handleQueue() {
        while (messageQueue.poll()?.apply { handleMessage() } != null);
    }

    private fun T.handleMessage() {
        try {
            handler.accept(this)
        } finally {
            messageQueue.finish(this)
        }
    }

}
