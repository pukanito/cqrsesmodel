/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util

import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import java.util.function.Predicate
import kotlin.reflect.KParameter
import kotlin.reflect.full.createType
import kotlin.reflect.full.isSupertypeOf
import kotlin.reflect.jvm.jvmErasure

/**
 * Resolve parameters for function calls.
 *
 * For resolving parameter values when calling command handlers and event (sourcing) handlers by reflection.
 *
 * It can resolve the following parameter values:
 * - DataId: the id of the Event or Command,
 * - Context: the context,
 * - other: other types bound in the Guice initialization module.
 *
 * For internal workings it can also resolve:
 * - instance parameter of a KFunction: the companion object or a replayed Aggregate.
 */
open class ParameterResolver {

    companion object {
        /**
         * Base [ParameterResolver], for wrapping, see [ParameterResolverWrapper].
         */
        val BASE = ParameterResolver()
    }

    /**
     * Resolve a parameter value.
     *
     * This method is for event handlers since it can resolve an event value.
     *
     * @param parameter the parameter to resolve.
     * @param context the infrastructure for resolving values.
     * @param receiver the receiver for resolving values.
     * @param id the id for resolving values.
     */
    open fun resolve(parameter: KParameter, context: CqrsContext, receiver: Any?, id: DataId): Any? {
        return when {
            parameter.isDataId() -> id
            parameter.isContext() -> context
            parameter.isAggregate() -> parameter.createInstance(id, context)
            receiver != null && parameter.type.isSupertypeOf(receiver::class.createType()) -> receiver
            else -> context.getInstance(parameter.type.jvmErasure.java)
        }
    }

    @Suppress("UNCHECKED_CAST") // parameter.isAggregate()
    private fun KParameter.createInstance(id: DataId, context: CqrsContext) =
        context.eventStore.retrieve((type.classifier as AggregateClass), id)

    /**
     * Wrap the ParameterResolver inside a wrapper that contains a predicate and result that
     * can also be resolved.
     *
     * @param predicate the predicate that will cause the ParameterResolver to resolve to the result if true.
     * @param predicateResult the result that will be resolved when the predicate is true.
     * @return a new ParameterResolver that can resolve to the supplied result when the predicate is true.
     */
    fun wrap(predicate: Predicate<KParameter>, predicateResult: Any?) =
        ParameterResolverWrapper(predicate, predicateResult, this)

}

/**
 * Wrapper for [ParameterResolver] that adds an extra predicate and result value to an existing ParameterResolver
 * that will be checked before the predicates of the existing ParameterResolver.
 *
 * @param predicate the predicate that evaluates to true if the predicateResult should be returned.
 * @param predicateResult the value that is returned when the predicate is true.
 * @param baseParameterResolver the ParameterResolver to wrap.
 */
class ParameterResolverWrapper(private val predicate: Predicate<KParameter>,
                               private val predicateResult: Any?,
                               private val baseParameterResolver: ParameterResolver
): ParameterResolver() {
    override fun resolve(parameter: KParameter, context: CqrsContext, receiver: Any?, id: DataId): Any? =
        if (predicate.test(parameter)) predicateResult
        else baseParameterResolver.resolve(parameter, context, receiver, id)

}
