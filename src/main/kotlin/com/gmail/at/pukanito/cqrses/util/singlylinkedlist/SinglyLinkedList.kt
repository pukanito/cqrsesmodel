/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util.singlylinkedlist

import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.Position.StartPosition
import java.util.concurrent.atomic.AtomicLong

/**
 * A thread-safe singly linked list with minimal functionality.
 *
 * - add to tail,
 * - remove from head,
 * - get(), hasItem() from the head of the list,
 * - get(position), hasItem(position) from a specified position in the list.
 * - iterator(count, position)
 *
 * @param T the type of items in the list.
 */
open class SinglyLinkedList<T> {

    /**
     * An item in the list.
     */
    private class SinglyLinkedListItem<T>(val item: T) {
        companion object {
            var counter = AtomicLong(0)
        }
        val position = counter.getAndIncrement()
        var removed: Boolean = false
        var next: SinglyLinkedListItem<T>? = null
    }

    // Head and tail of the list.
    private var head: SinglyLinkedListItem<T>? = null
    private var tail: SinglyLinkedListItem<T>? = null

    private val newPositions: MutableList<SettablePosition> = mutableListOf()

    protected interface SettablePosition {
        fun set(position: Any)
    }

    private data class SinglyLinkedListPosition(private var position: SinglyLinkedListItem<*>? = null):
        Position<Any?>, SettablePosition {
        override fun get(): SinglyLinkedListItem<*>? = position
        override fun set(position: Any) { if (this.position == null)
            this.position = position as SinglyLinkedListItem<*> }
        override fun compareTo(other: Position<*>) = when (other) {
            is StartPosition -> if (position == null) 1 else itemPosition(position!!).compareTo(0)
            is SinglyLinkedListPosition -> if (position == null && other.position == null) 0
                else if (position == null) 1
                else if (other.position == null) -1
                else itemPosition(position!!).compareTo(itemPosition(other.position!!))
            else -> throw IllegalStateException("Cannot compare Position: ${other.javaClass.canonicalName}")
        }
        private fun itemPosition(position: SinglyLinkedListItem<*>) = position.position
    }

    /**
     * @return the current head [Position] of the list.
     */
    @Synchronized
    protected fun getHeadPosition(): Position<*> = SinglyLinkedListPosition(head).also { newPositions += it }

    /**
     * @return the current tail [Position] of the list.
     */
    @Synchronized
    fun getPosition(): Position<*> = SinglyLinkedListPosition().also { newPositions += it }

    /**
     * Add items to the tail of the list (thread-safe).
     *
     * @param items the items to be added.
     */
    @Synchronized
    fun add(vararg items: T) {
        items.forEach { item ->
            val listItem = SinglyLinkedListItem(item)
            if (tail == null)
                head = listItem
            else
                tail!!.next = listItem
            tail = listItem
            newPositions.run {
                forEach { it.set(tail!!) }
                clear() }
        }
    }

    /**
     * Remove from the head of the list while the predicate for the head item is true (thread-safe).
     *
     * @param predicate the predicate to test.
     */
    @Synchronized
    fun removeWhile(predicate: (T) -> Boolean) {
        while (head?.run { predicate(item) } == true) {
            head?.removed = true
            head = head?.next
        }
        if (head == null) tail = null
    }

    /**
     * Get the item and position of the next item at the specified position.
     *
     * @param position the position where to get the item, if null get from the head.
     * @return Pair<T, Position<*>> the item and the position of the next item in the list.
     */
    @Synchronized
    fun get(position: Position<*>? = null): Pair<T, Position<*>> {
        @Suppress("UNCHECKED_CAST") // SinglyLinkedListItem<T> is private type.
        return when (position) {
            is SinglyLinkedListPosition -> get((position.get() as? SinglyLinkedListItem<T>))
            null -> get(head)
            else -> throw IllegalStateException("Cannot handle Position: ${position.javaClass.canonicalName}") }
    }

    private fun get(item: SinglyLinkedListItem<T>?): Pair<T, Position<*>> {
        if (item != null) {
            if (item.removed) throw NoSuchElementException("Consuming a deleted SinglyLinkedListItem")
            return item.item to SinglyLinkedListPosition(item.next)
        }
        throw NoSuchElementException("No more items in SinglyLinkedList")
    }

    /**
     * Check if an item exists at the specified position.
     *
     * @param position the position to check, if null check at the head.
     */
    @Synchronized
    fun hasItem(position: Position<*>? = null): Boolean {
        @Suppress("UNCHECKED_CAST") // SinglyLinkedListItem<T> is private type.
        return when (position) {
            is SinglyLinkedListPosition -> hasItem((position.get() as? SinglyLinkedListItem<T>))
            null -> hasItem(head)
            else -> throw IllegalStateException("Cannot handle Position: ${position.javaClass.canonicalName}") }
    }

    private fun hasItem(item: SinglyLinkedListItem<T>?): Boolean {
        if (item != null && item.removed) throw NoSuchElementException("Consuming a deleted SinglyLinkedListItem")
        return item != null
    }

    /**
     * @param count the number of items to iterate maximally, count <= 0 iterates nothing.
     * @param position the starting [Position] in the list for the [Iterator],
     *        default is null which iterates the whole list.
     * @return an [Iterator] starting at the specified [Position] till the end of the list.
     */
    fun iterator(count: Int, position: Position<*>? = null): Iterator<Pair<T, Position<*>>> =
        SinglyLinkedListIterator(count, position ?: getHeadPosition())

    /**
     * @return an iterator for a lot of values: `iterator(Int.MAX_VALUE, position)`
     */
    fun iterator(position: Position<*>? = null): Iterator<Pair<T, Position<*>>> = iterator(Int.MAX_VALUE, position)

    /**
     *
     * Iterator implementation for a SinglyLinkedList.
     *
     * The iterator only snapshots the start item, not the complete list, hence items added after
     * iterator creation but not yet iterated are also included in the iteration.
     */
    private inner class SinglyLinkedListIterator(var count: Int, var position: Position<*>):
        Iterator<Pair<T, Position<*>>>, SettablePosition {
        @Synchronized
        override fun hasNext(): Boolean = count >= 1 && hasItem(position)
        @Synchronized
        override fun next(): Pair<T, Position<*>> = get(position).also {
            if (count < 1) throw NoSuchElementException("No more items in SinglyLinkedListIterator")
            count--
            position = it.second }
        @Synchronized
        override fun set(position: Any) {
            this.position = position as Position<*>
        }
    }

}
