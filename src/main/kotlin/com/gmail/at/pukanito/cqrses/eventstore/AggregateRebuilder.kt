/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event

/**
 * Supplier for [Aggregate]s, replay events on [Aggregate]s.
 *
 * Possible extra functionality: [Aggregate] caching/snapshotting.
 */
interface AggregateRebuilder {

    /**
     * Get a projected [Aggregate] of the specified class and id, or create a new initial one.
     *
     * @param eventStorage [EventStorage] for retrieving [Event]s that may need to be applied to
     *     the [Aggregate] to bring it up to date.
     * @param aggregateClass the class of the [Aggregate] to get.
     * @param id the id of the [Aggregate] to get.
     */
    suspend fun get(eventStorage: EventStorage, aggregateClass: AggregateClass, id: DataId): Aggregate

}
