/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.infrastructure

import com.gmail.at.pukanito.cqrses.Message
import com.gmail.at.pukanito.cqrses.SequenceId
import java.util.concurrent.CompletableFuture

/**
 * MessageState is only kept if someone is watching it, i.e. getState has been called.
 *
 * getState may be called by the creator of the Message to be able to track the result.
 *
 * TODO: cleanup, stored messages should no longer be inside the [MessageStateContext].
 *  if the state future was not finished, finish it with an exception: getState was probably called after finishState.
 */
class MessageStateContext {

    private class MessageState {
        val states = mutableListOf<Any>()
        val stateFuture = CompletableFuture<List<Any>>()
    }

    private val messageStates = mutableMapOf<SequenceId, MessageState>()

    fun getState(message: Message): CompletableFuture<List<Any>> =
        messageStates.getOrPut(message.sequenceId) { MessageState() }.stateFuture

    fun addState(message: Message, state: Any) {
        messageStates[message.sequenceId]?.run { states.add(state) }
    }

    fun finishState(message: Message) {
        messageStates[message.sequenceId]?.run { if (!stateFuture.isDone) stateFuture.complete(states) }
    }

}
