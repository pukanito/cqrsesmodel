/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util

import com.gmail.at.pukanito.cqrses.Message

/**
 * A queue for [Message]s.
 *
 * A persistent message queue can restore state after (unexpected) terminiation.
 * All messages that have been added but not yet finished will be restored in the queue.
 */
interface MessageQueue<T> where T: Message {

    /**
     * Add a new message to the queue.
     *
     * @param message the message to add to the queue.
     */
    fun add(message: T)

    /**
     * Return the next message from the queue or null if the queue is empty.
     *
     * @return the next message or null.
     */
    fun poll(): T?

    /**
     * Finish handling this message (only applicable for persistent implementation).
     *
     * @param message the message that has been handled by the queue.
     */
    fun finish(message: T)

}
