/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util

import com.gmail.at.pukanito.cqrses.Message
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.util.AnnotatedMethodsMapper.AnnotatedMethod
import java.lang.reflect.InvocationTargetException
import java.util.function.Consumer
import java.util.logging.Level.WARNING
import java.util.logging.Logger
import kotlin.reflect.KClass
import kotlin.reflect.KParameter

/**
 * Base for a message handler executor that consumes messages by executing
 * [AnnotatedMethod]s found by the [AnnotatedMethodsMapper].
 *
 * It keeps track of message state while consuming a message.
 *
 * @param context the infrastructure for resolving values.
 * @param isKeyParameter predicate that returns true when the parameter (sub)type is to be used as map key.
 * @param annotationsToFind the annotations to find.
 */
abstract class MessageHandlerExecutor<T>(private val context: CqrsContext,
                                         isKeyParameter: KParameter.() -> Boolean,
                                         vararg annotationsToFind: KClass<out Annotation>):
    Consumer<T> where T: Message {

    private val logger = Logger.getLogger(MessageHandlerExecutor::class.qualifiedName)

    protected val baseParameterResolver = ParameterResolver.BASE

    // Map of Event type to HandlerCallers for annotated handlers of all AggregateClasses.
    @Suppress("SpreadOperator") // No better way to do this yet.
    protected val annotatedMethods =
        AnnotatedMethodsMapper(context.aggregateClasses, isKeyParameter, *annotationsToFind)

    /**
     * Handle the message, keep track of message state and finish the message state.
     *
     * @param message the message to be handled.
     */
    override fun accept(message: T) {
        try {
            handleMessage(message)
        } catch (ex: Exception) {
            logger.log(WARNING, ex) { "Unhandled exception in message handler: ${ex.message}" }
            context.messageStateContext.addState(message, ex)
        } finally {
            context.messageStateContext.finishState(message)
        }
    }

    /**
     * Handle the specified message by executing all annotated message handlers that have the specified
     * message type as first [Message] parameter.
     *
     * If it uses the call() method for executing each annotated message handler than it need not
     * be concerned about messate state. If not, it has to keep track of message state itself.
     *
     * @param message the message to be handled.
     */
    abstract fun handleMessage(message: T)

    protected fun AnnotatedMethod.call(parameterResolver: ParameterResolver, message: Message) {
        try {
            method.callBy(method.parameters
                .associateWith { parameterResolver.resolve(it, context, this.instance, message.id) })
        } catch (ex: InvocationTargetException) {
            logger.log(WARNING, ex) { "Unhandled exception in message handler: ${ex.cause?.message}" }
            ex.cause?.let { context.messageStateContext.addState(message, it) }
        }
    }

}
