/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection.database

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY
import com.fasterxml.jackson.annotation.PropertyAccessor.FIELD
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.gmail.at.pukanito.cqrses.projection.ProjectionDataRepository
import com.gmail.at.pukanito.cqrses.projection.ProjectionDataRepository.ProjectionData
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.util.UUID
import javax.inject.Inject

/**
 * [ProjectionDataRepository] in a [Database].
 */
class DatabaseProjectionDataRepository @Inject constructor(private val database: Database): ProjectionDataRepository {

    companion object {
        private val DATA_UUID = UUID.fromString("bc8ee41a-15b5-402e-9dd6-47d5d9afeba2")
        private val MAPPER = jacksonObjectMapper()
            .setVisibility(FIELD, ANY)
            .enableDefaultTyping()
            .registerModule(KotlinModule())
            .registerModule(JavaTimeModule())
            .registerModule(Jdk8Module())!!
    }

    object ProjectionDataTable: Table() {
        val id = uuid("id")
        val data = text("data")
        override val primaryKey = PrimaryKey(id, name = "PkProjectionData") }

    // Cache the projection data.
    private var projectionData: ProjectionData? =
        transaction(database) { ProjectionDataTable.run { select { id eq DATA_UUID }.firstOrNull() } }
            ?.run { MAPPER.readValue<ProjectionData>(this[ProjectionDataTable.data]) }

    override fun save(projectionData: ProjectionData) = updateOrInsert(projectionData)

    override fun load(): ProjectionData = projectionData ?: MAPPER.readValue("{}")

    private fun updateOrInsert(projectionData: ProjectionData) {
        this.projectionData = projectionData
        val jsonData = MAPPER.writeValueAsString(projectionData)
        transaction(database) {
            ProjectionDataTable.run {
                if (update({ id eq DATA_UUID }) { it[data] = jsonData } == 0)
                    insert { it[id] = DATA_UUID; it[data] = jsonData }
            }
        }
    }

}
