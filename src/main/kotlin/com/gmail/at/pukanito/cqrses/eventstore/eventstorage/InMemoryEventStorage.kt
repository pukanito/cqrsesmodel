/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage

import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.util.singlylinkedlist.SinglyLinkedList
import com.gmail.at.pukanito.cqrses.util.singlylinkedlist.asFlow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.util.logging.Logger
import javax.inject.Inject

/**
 * An [EventStorage] that stores [Event]s in memory.
 *
 * This event storage is not persistent.
 *
 * @param logger a [Logger].
 */
class InMemoryEventStorage @Inject constructor(private val logger: Logger): EventStorage {

    private val allEvents = SinglyLinkedList<Event>()

    override fun accept(event: Event) {
        logger.finer { "$event" }
        allEvents.add(event)
    }

    override fun archive(eventPredicate: (Event) -> Boolean) = allEvents.removeWhile(eventPredicate)

    override fun get(position: Position<*>?): Flow<PositionedEvent> {
        logger.finer { "$position" }
        return allEvents.asFlow(position).map { PositionedEvent(it.first, it.second) }
    }

    override fun get(count: Int, position: Position<*>?): Flow<PositionedEvent> {
        logger.finer { "get: $count -> $position" }
        return allEvents.asFlow(count, position).map { PositionedEvent(it.first, it.second) }
    }

    override fun getPosition(): Position<*> {
        val position = allEvents.getPosition()
        logger.finer { "$position" }
        return position
    }

}
