/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses

import com.gmail.at.pukanito.cqrses.Position.Companion.START_POSITION
import java.time.Instant
import java.util.UUID
import java.util.concurrent.atomic.AtomicLong
import kotlin.reflect.KClass

/**
 * This file contains all small model definitions. Together with CommandBus, EventBus and
 * EventStore it makes up the complete CQRS model.
 */

// ===== Definitions =====

/**
 * An id for identifying object instances.
 */
typealias DataId = UUID

/**
 * Shortcut for covariant [Aggregate] class.
 */
typealias AggregateClass = KClass<out Aggregate>

// ===== Basics =====

/**
 * A data object that has an id.
 */
open class DataWithId(val id: DataId) {
    fun hasId(id: DataId): Boolean = this.id == id
    override fun toString(): String = "DataWithId(id=$id)"
}

/**
 * An unique id that can also be used for determining order in time.
 *
 * Every new instance created will have a larger value than earlier created instances according to Comparable,
 * even when the application restarts and nextId restarts at 0.
 */
class SequenceId private constructor(val instant: Instant,
                                     val sequenceNumber: Long): Comparable<SequenceId> {
    companion object {
        private val nextId = AtomicLong(0)
        private val comparator = Comparator.comparing(SequenceId::instant).thenComparing(SequenceId::sequenceNumber)
    }
    constructor(): this(Instant.now(), nextId.getAndIncrement())
    override fun compareTo(other: SequenceId): Int = comparator.compare(this, other)
    override fun toString(): String = "SequenceId(instant=$instant, sequenceNumber=$sequenceNumber)"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        return (other as? SequenceId)?.let { (instant == it.instant) && (sequenceNumber == it.sequenceNumber) } ?: false
    }
    override fun hashCode(): Int {
        var result = instant.hashCode()
        result = 31 * result + sequenceNumber.hashCode()
        return result
    }
}

/**
 * Class to specify a position in a message store for retrieving messages.
 *
 * @param <T> the position implementation type.
 */
interface Position<T>: Comparable<Position<*>> {
    fun get(): T
    /**
     * A [Position<*>] that is always less than all other positions and equal to itself.
     */
    class StartPosition: Position<Unit> {
        override fun get() = Unit
        override fun compareTo(other: Position<*>) = if (other is StartPosition) 0 else -1
    }
    companion object {
        val START_POSITION = StartPosition()
    }
}

// ===== Messaging =====

/**
 * Base class for messages.
 *
 * A message is something that will be sent over a message bus for distribution to
 * [MessageHandlingObject] instances.
 * A message has an id to identify the [MessageHandlingObject] instance to interact with.
 *
 * @param id the id of the object(s) with which the message interacts.
 */
abstract class Message(id: DataId): DataWithId(id) {
    /**
     * sequence id for identification of the [Message] and order of creation.
     */
    val sequenceId = SequenceId()
    override fun toString(): String = "Message(sequenceId=$sequenceId) ${super.toString()}"


}

/**
 * A command is a special [Message] signaling an intention to do something.
 *
 * @param id the id of the object(s) with which to do something.
 */
abstract class Command(id: DataId): Message(id) {
    override fun toString(): String = "Command() ${super.toString()}"
}

/**
 * An event is a special [Message] signaling something that has happened.
 *
 * @param id the id of the object(s) on which something has happened.
 */
abstract class Event(id: DataId): Message(id) {
    override fun toString(): String = "Event() ${super.toString()}"
}

// ===== Message handling =====

/**
 * Base class for message handling objects.
 *
 * Message handling objects may listen for [Message]s by annotating methods as a message handler:
 * [CommandHandler], [EventHandler] or [EventSourcingHandler].
 *
 * @param id the id of the message handling object instance.
 */
abstract class MessageHandlingObject(id: DataId): DataWithId(id) {
    // Id of the last applied message (kind of version of the MessageHandlingObject).
    @Transient
    var version: Position<*> = START_POSITION
    override fun toString(): String = "MessageHandlingObject(version=$version) ${super.toString()}"

}

/**
 * An aggregate is a [MessageHandlingObject] on which [Command]s operate and [Event]s have happened.
 *
 * An aggregate can rebuild its state by replaying [Event]s to its event sourcing handlers.
 * State is necessary for handling [Command]s (command handlers) and [Event]s (event handlers).
 * [Command]s and [Event]s may result in new [Command]s and [Event]s or other side-effects.
 *
 * @param id the id of the aggregate.
 */
abstract class Aggregate(id: DataId): MessageHandlingObject(id) {
    override fun toString(): String = "Aggregate() ${super.toString()}"
}

/**
 * A Saga is an object which orchestrates events on one or more Aggregates.
 */
//abstract class Saga(id: DataId, vararg val aggregateIds: DataId): MessageHandlingObject(id) {
//    companion object {
//        val sagas = mutableListOf<Saga>()
//    }
//    override fun toString() = "Saga: ${javaClass.simpleName} $id"
//}

// ===== Message Handling Annotations =====

/**
 * Annotation for marking a method as a CommandHandler.
 * A CommandHandler handles [Command]s that are published and may create [Command]s, [Event]s and other
 * side-effects.
 */
@Target(AnnotationTarget.FUNCTION)
annotation class CommandHandler

/**
 * Annotation for marking a method as an EventHandler.
 * An EventHandler handles [Event]s that are published, but not when replayed.
 *
 * Used for handlers that create [Command]s and/or [Event]s and/or have side-effects
 * that should not occur again when replaying.
 */
@Target(AnnotationTarget.FUNCTION)
annotation class EventHandler

/**
 * Annotation for marking a method as an EventSourcingHandler.
 * An EventSourcingHandler handles [Event]s that are replayed, but not when published.
 *
 * Used for handlers that have no side-effects and do not create [Command]s and/or [Event]s,
 * they only update [Aggregate] state.
 */
@Target(AnnotationTarget.FUNCTION)
annotation class EventSourcingHandler
