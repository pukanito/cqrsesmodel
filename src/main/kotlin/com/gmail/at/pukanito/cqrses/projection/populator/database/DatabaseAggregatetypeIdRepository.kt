/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection.populator.database

import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.projection.populator.AggregatetypeIdRepository
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.insertIgnore
import org.jetbrains.exposed.sql.transactions.transaction
import javax.inject.Inject

/**
 * [AggregatetypeIdRepository] in a [Database].
 *
 * @param database the database where to store projection results.
 */
class DatabaseAggregatetypeIdRepository @Inject constructor(private val database: Database): AggregatetypeIdRepository {

    object AggregatetypeAndIdTable: Table() {
        val id = uuid("id")
        @Suppress("MagicNumber") // Column size is fixed.
        val type = varchar("text", 256)
        override val primaryKey = PrimaryKey(id, type, name = "PkAggregatetypeAndId") }

    override fun save(aggregateClass: AggregateClass, id: DataId) {
        transaction(database) {
            AggregatetypeAndIdTable.insertIgnore {
                it[AggregatetypeAndIdTable.id] = id
                it[type] = aggregateClass.java.typeName } }
    }

}
