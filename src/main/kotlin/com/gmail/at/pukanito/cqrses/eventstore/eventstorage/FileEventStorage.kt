/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage

import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import kotlinx.coroutines.flow.Flow
import java.util.logging.Logger
import javax.inject.Inject

/**
 * An [EventStorage] that stores [Event]s in files on disk.
 *
 * Reading/writing is delegated to [FileEventStorageReader]/[FileEventStorageWriter].
 *
 * @param fileEventStorageReader the [FileEventStorageReader].
 * @param fileEventStorageWriter the [FileEventStorageWriter].
 * @param logger a [Logger].
 */
class FileEventStorage @Inject constructor(private val fileEventStorageReader: FileEventStorageReader,
                                           private val fileEventStorageWriter: FileEventStorageWriter,
                                           private val logger: Logger): EventStorage {

    override fun accept(event: Event) {
        logger.finer { "$event" }
        fileEventStorageWriter.accept(event)
    }

    override fun getPosition(): Position<*> {
        val position = fileEventStorageWriter.getPosition()
        logger.finer { "$position" }
        return position
    }

    override fun get(position: Position<*>?): Flow<PositionedEvent> {
        logger.finer { "$position" }
        return if (position == null) fileEventStorageReader.get() else fileEventStorageReader.get(position)
    }

    override fun get(count: Int, position: Position<*>?): Flow<PositionedEvent> {
        logger.finer { "get: $count -> $position" }
        return fileEventStorageReader.get(count, position)
    }

    override fun archive(eventPredicate: (Event) -> Boolean) {
        // TODO
    }

}
