/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.infrastructure

import com.charleskorn.kaml.Yaml
import kotlinx.serialization.Decoder
import kotlinx.serialization.Encoder
import kotlinx.serialization.KSerializer
import kotlinx.serialization.PrimitiveDescriptor
import kotlinx.serialization.PrimitiveKind
import kotlinx.serialization.SerialDescriptor
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.UUID

/**
 * Configuration manager.
 *
 * Contains all configurable settings.
 */
@Serializable
class CqrsConfiguration {

    companion object {
        const val EVENT_FILE_STORAGE_FILE = "out/cfg/test-eventfile.bin"
        const val ROTATION_EVENT_FILE_STORAGE_DIR = "out/cfg/test-rotating-eventfile-dir"
        const val COMMAND_QUEUE_FILE_STORAGE_FILE = "out/cfg/test-commandqueue.bin"
        const val ROTATION_COMMAND_QUEUE_FILE_STORAGE_DIR = "out/cfg/test-rotating-commandqueue-dir"
        const val EVENT_QUEUE_FILE_STORAGE_FILE = "out/cfg/test-eventqueue.bin"
        const val ROTATION_EVENT_QUEUE_FILE_STORAGE_DIR = "out/cfg/test-rotating-eventqueue-dir"

        /**
         * Load a configuration from YAML-file.
         */
        fun load(configFilePath: String): CqrsConfiguration =
            Yaml.default.parse(serializer(), String(Files.readAllBytes(Paths.get(configFilePath))))
    }

    @Serializable
    @Suppress("MagicNumber") // Just defaults.
    class AggregateCache {
        val maximumSize: Long = 1_000
        val expireAfterWriteSeconds: Long = 360
    }

    @Serializable
    class EventFileStorage {
        @Serializable(with = PathSerializer::class)
        val path: Path = Paths.get(EVENT_FILE_STORAGE_FILE)
        val rotation: RotationEventFileStorage = RotationEventFileStorage()
    }

    @Serializable
    @Suppress("MagicNumber") // Just defaults.
    class RotationEventFileStorage {
        @Serializable(with = PathSerializer::class)
        val directory: Path = Paths.get(ROTATION_EVENT_FILE_STORAGE_DIR)
        val size: Long = 10_000_000
    }

    @Suppress("unused")
    @Serializer(forClass = Path::class)
    object PathSerializer: KSerializer<Path> {
        override val descriptor: SerialDescriptor = PrimitiveDescriptor("Path", PrimitiveKind.STRING)
        override fun serialize(encoder: Encoder, value: Path) = encoder.encodeString(value.toString())
        override fun deserialize(decoder: Decoder): Path = Paths.get(decoder.decodeString())
    }

    @Suppress("MagicNumber") // Just defaults.
    @Serializable
    class Database {
        val jdbcUrl: String = "jdbc:h2:mem:${UUID.randomUUID()};MODE=MySQL;"
        val jdbcDriverClassName: String = "org.h2.Driver"
        val maximumIdle: Int = 4
        val maximumPoolSize: Int = 10
    }

    val aggregateCache = AggregateCache()
    val eventFileStorage = EventFileStorage()
    val commandQueueFileStorage = Yaml.default.parse(EventFileStorage.serializer(), """
          path: $COMMAND_QUEUE_FILE_STORAGE_FILE
          rotation:
              directory: $ROTATION_COMMAND_QUEUE_FILE_STORAGE_DIR
    """.trimIndent())
    val eventQueueFileStorage = Yaml.default.parse(EventFileStorage.serializer(), """
          path: $EVENT_QUEUE_FILE_STORAGE_FILE
          rotation:
              directory: $ROTATION_EVENT_QUEUE_FILE_STORAGE_DIR
    """.trimIndent())
    val database = Database()

}
