/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage.Companion.EMPTY_EVENT_STORAGE
import com.gmail.at.pukanito.cqrses.Position
import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.runBlocking
import java.util.concurrent.TimeUnit
import java.util.logging.Logger
import javax.inject.Inject

/**
 * An [AggregateRebuilder] that caches [Aggregate]s in memory.
 *
 * @param replayer an [AggregateMessageReplayer].
 * @param maximumCacheSize maximal number of items in cache.
 * @param expireCacheAfterWriteSeconds number of seconds after which cached items expire.
 * @param fallBackRebuilder [AggregateRebuilder] to use when the Aggregate is not yet cached.
 * @param logger a [Logger].
 */
class InMemoryAggregateRebuilder @Inject constructor(private val replayer: AggregateMessageReplayer,
                                                     maximumCacheSize: Long,
                                                     expireCacheAfterWriteSeconds: Long,
                                                     private val fallBackRebuilder: AggregateRebuilder,
                                                     private val logger: Logger): AggregateRebuilder {

    companion object {
        private val MAPPER = jacksonObjectMapper()
            .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
            .enableDefaultTyping()
            .registerModule(KotlinModule())
            .registerModule(JavaTimeModule())
            .registerModule(Jdk8Module())!!
    }

    @Suppress("unused") // Only used for (de)serialisation to add type info in json string.
    internal class SerialisedAggregate(val aggregate: Aggregate)

    private data class CacheId(val aggregateClass: AggregateClass, val id: DataId)

    private class AggregateInfo(val aggregateJson: String, var position: Position<*>?)

    private inner class AggregateCacheLoader: CacheLoader<CacheId, AggregateInfo>() {
        override fun load(id: CacheId): AggregateInfo {
            logger.fine { "${id.aggregateClass}::${id.id}" }
            return AggregateInfo(MAPPER.writeValueAsString(runBlocking {
                SerialisedAggregate(fallBackRebuilder.get(EMPTY_EVENT_STORAGE, id.aggregateClass, id.id))}), null)
        }
    }

    private val aggregateCache = CacheBuilder.newBuilder()
        .maximumSize(maximumCacheSize)
        .expireAfterWrite(expireCacheAfterWriteSeconds, TimeUnit.SECONDS)
        .build(AggregateCacheLoader())

    override suspend fun get(eventStorage: EventStorage, aggregateClass: AggregateClass, id: DataId): Aggregate {
        logger.fine { "$aggregateClass::$id" }
        val cacheId = CacheId(aggregateClass, id)
        return aggregateCache.get(cacheId).run { coroutineScope {
            val nextPosition = eventStorage.getPosition()
            val deferredAggregate = async { MAPPER.readValue<SerialisedAggregate>(aggregateJson).aggregate }
            replayer.replay(deferredAggregate, eventStorage.get(position).filter { it.id == id }).also {
                @Suppress("BlockingMethodInNonBlockingContext")
                aggregateCache.put(cacheId,
                    AggregateInfo(MAPPER.writeValueAsString(SerialisedAggregate(it)), nextPosition)) } } }
    }

}
