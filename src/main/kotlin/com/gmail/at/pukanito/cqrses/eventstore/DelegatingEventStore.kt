/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.runBlocking
import java.util.logging.Logger
import javax.inject.Inject
import kotlin.reflect.KClass

/**
 * [EventStore] that delegates storing/retrieving [Event]s to an [EventStorage] and
 * retrieving [Aggregate]s to an [AggregateRebuilder]. When retrieving an aggregate
 * and replaying the events, only the applicable events are applied.
 *
 * @param eventStorage [EventStorage] for storing/retrieving the events.
 * @param aggregateRebuilder [AggregateRebuilder] for retrieving [Aggregate] instances.
 * @param logger a [Logger].
 * @param archivePositionSupplier supplier of Position<*> until where the eventStorage has been archived. Earlier
 *     [Event]s need no longer be used.
 */
class DelegatingEventStore @Inject constructor(
    private val eventStorage: EventStorage,
    private val aggregateRebuilder: AggregateRebuilder,
    private val logger: Logger,
    private val archivePositionSupplier: () -> Position<*>? = { null }) : EventStore {

    private val eventStorageWrapper = EventStoragePositionWrapper(eventStorage)

    override fun accept(event: Event) {
        logger.finer { "$event" }
        return eventStorage.accept(event)
    }

    override fun getPosition(): Position<*> {
        val position = eventStorage.getPosition()
        logger.finer { "$position" }
        return position
    }

    override fun get(position: Position<*>?): Flow<PositionedEvent> {
        logger.finer { "get: $position" }
        return eventStorage.get(position)
    }

    override fun get(count: Int, position: Position<*>?): Flow<PositionedEvent> {
        logger.finer { "get: $count -> $position" }
        return eventStorage.get(count, position)
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T: Aggregate> retrieve(aggregateClass: KClass<out T>, id: DataId): T {
        logger.finer { "$aggregateClass::$id" }
        return runBlocking { aggregateRebuilder.get(eventStorageWrapper, aggregateClass, id) as T }
    }

    private inner class EventStoragePositionWrapper(eventStorage: EventStorage): EventStorage by eventStorage {
        override fun get(position: Position<*>?) = eventStorage.get(position ?: archivePositionSupplier.invoke())
        override fun get(count: Int, position: Position<*>?) =
            eventStorage.get(count, position ?: archivePositionSupplier.invoke())
    }

}
