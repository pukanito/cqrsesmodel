/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util.messagequeue

import com.fasterxml.jackson.annotation.JsonCreator
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.Message
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.util.MessageQueue
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import java.util.UUID
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * A thread safe persistent queue for [Message]s.
 *
 * @param queueEventStorage message storage for the message queue.
 */
class PersistentMessageQueue<T>(private val queueEventStorage: EventStorage):
    MessageQueue<T> where T: Message {

    private val queue: ConcurrentLinkedQueue<T> = ConcurrentLinkedQueue()

    internal class MessageAddedToQueue(val message: Message) : Event(message.id) {
        override fun toString(): String = "MessageAddedToQueue(message=$message) ${super.toString()}"
    }

    internal class MessagePolledFromQueue(message: Message) : Event(message.id) {
        @Suppress("unused") // Used by Jackson for deserialization.
        @JsonCreator constructor() : this(object: Message(UUID.randomUUID()){})
        override fun toString(): String = "MessagePolledFromQueue() ${super.toString()}"

    }

    internal class MessageFinished(message: Message) : Event(message.id) {
        @Suppress("unused") // Used by Jackson for deserialization.
        @JsonCreator constructor() : this(object: Message(UUID.randomUUID()){})
        override fun toString(): String = "MessageFinished() ${super.toString()}"

    }

    override fun add(message: T) = message.addToQueue()

    override fun poll(): T? = pollFromQueue()

    override fun finish(message: T) {
        queueEventStorage.accept(MessageFinished(message))
    }

    private fun T.addToQueue() {
        queueEventStorage.accept(MessageAddedToQueue(this))
        queue.add(this)
    }

    private fun pollFromQueue(): T? {
        return queue.poll()?.also { queueEventStorage.accept(MessagePolledFromQueue(it)) }
    }

    init {

        // Restore queue.
        val queueBuffer = mutableListOf<Message>()
        runBlocking {
            queueEventStorage.get().collect { positionedEvent ->
                when (positionedEvent.event) {
                    is MessageAddedToQueue -> queueBuffer.add(positionedEvent.event.message)
                    is MessagePolledFromQueue -> { }
                    is MessageFinished -> queueBuffer.removeIf { positionedEvent.id == it.id }
                    else -> { }
                } } }
        queueBuffer.forEach { @Suppress("UNCHECKED_CAST") queue.add(it as T) }

    }
}
