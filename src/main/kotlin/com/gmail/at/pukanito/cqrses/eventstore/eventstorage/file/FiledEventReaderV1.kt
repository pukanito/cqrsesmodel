/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import com.fasterxml.jackson.module.kotlin.readValue
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileWriter.EventFilePosition
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.FiledEventReader.FiledEvent
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import java.io.IOException
import java.nio.MappedByteBuffer

/**
 * Read [FiledEvent]s from event file version 1.
 */
internal class FiledEventReaderV1(private val mapToFiledEvent: (String) -> FiledEvent): FiledEventReader {

    companion object {

        const val VERSION = 1

        internal val mapToStringV1 = FiledEventReader.MAPPER::writeValueAsString
        internal val mapToFiledEventV1 = { s: String -> FiledEventReader.MAPPER.readValue<FiledEvent>(s) }
    }

    @Suppress("EXPERIMENTAL_API_USAGE")
    override fun generateFlow(buffer: MappedByteBuffer): Flow<Pair<FiledEvent, EventFilePosition>> {
        return channelFlow {
            while (buffer.hasRemaining()) {
                send(getFiledEvent(buffer) to EventFilePosition(buffer.position().toLong()))
            }
        }
    }

    private fun getFiledEvent(buffer: MappedByteBuffer): FiledEvent {
        // TODO: Retry on java.nio.BufferUnderflowexception? (partly written event?)
        val size = buffer.int
        val hash = buffer.int
        val json = ByteArray(size).let { bytes ->
            buffer.get(bytes, 0, size)
            String(bytes, Charsets.UTF_8)
        }
        if (hash != json.hashCode()) throw IOException("Read error in EventFileReader: checksum failure")
        return mapToFiledEvent(json)
    }

}
