/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.FiledEventReader.FiledEvent
import kotlinx.coroutines.flow.Flow
import java.nio.MappedByteBuffer

/**
 * Read [FiledEvent]s from a MappedByteBuffer and convert them to a [Flow] of [FiledEvent]s.
 */
internal interface FiledEventReader {

    /**
     * Internal representation of [Event]s when written to a file.
     */
    class FiledEvent @JsonCreator constructor(val event: Event)

    companion object {
        /**
         * Mapping from/to [FiledEvent].
         */
        val MAPPER = jacksonObjectMapper()
            .enableDefaultTyping()
            .registerModule(KotlinModule())
            .registerModule(JavaTimeModule())
            .registerModule(Jdk8Module())!!
    }

    /**
     * Generate a [Flow] from the [FiledEvent]s inside the buffer. When the end of the buffer is reached
     * execute onEndOfBuffer. When the buffer is empty, onEndOfBuffer is executed immediately.
     *
     * @param buffer [ByteBuffer] containing [FiledEvent]s for the [Flow].
     * @return a [Flow] of pairs of a [FiledEvent] and the [Position] of the next [FiledEvent].
     */
    fun generateFlow(buffer: MappedByteBuffer): Flow<Pair<FiledEvent, Position<*>>>

}
