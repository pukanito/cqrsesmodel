/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Helper methods for supporting the model.
 */
package com.gmail.at.pukanito.cqrses.util

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.Command
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import kotlin.reflect.KParameter
import kotlin.reflect.full.createType
import kotlin.reflect.full.isSubtypeOf

fun KParameter.isDataId() = type.isSubtypeOf(DataId::class.createType())
fun KParameter.isCommand() = type.isSubtypeOf(Command::class.createType())
fun KParameter.isEvent() = type.isSubtypeOf(Event::class.createType())
fun KParameter.isAggregate() = type.isSubtypeOf(Aggregate::class.createType())
fun KParameter.isContext() = type.isSubtypeOf(CqrsContext::class.createType())
