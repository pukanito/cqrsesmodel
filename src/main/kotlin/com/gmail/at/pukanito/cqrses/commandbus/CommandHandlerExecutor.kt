/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.commandbus

import com.gmail.at.pukanito.cqrses.Command
import com.gmail.at.pukanito.cqrses.CommandHandler
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.util.AnnotatedMethodsMapper.AnnotatedMethod
import com.gmail.at.pukanito.cqrses.util.MessageHandlerExecutor
import com.gmail.at.pukanito.cqrses.util.isCommand
import java.util.function.Predicate
import java.util.logging.Logger
import kotlin.reflect.full.createType

/**
 * A handler for commands that executes methods marked with @[CommandHandler].
 *
 * Commands that have no handler associated will result in an IllegalStateException.
 *
 * @param context the infrastructure [CqrsContext].
 * @param logger a [Logger].
 */
class CommandHandlerExecutor(private val context: CqrsContext,
                             private val logger: Logger):
    MessageHandlerExecutor<Command>(context, { isCommand() }, CommandHandler::class) {

    /**
     * Handle the specified command by executing all annotated command handlers that have the specified
     * command type as first [Command] parameter.
     *
     * @param message the command to be handled.
     */
    override fun handleMessage(message: Command) {
        logger.info { "$message" }
        val parameterResolver = baseParameterResolver.wrap(Predicate { p -> p.isCommand() }, message)
        annotatedMethods.kTypeMap
            .getOrElse(message::class.createType()) {
                context.messageStateContext.addState(message, IllegalStateException("No command handler for $message"))
                listOf<AnnotatedMethod>() }
            .forEach { it.call(parameterResolver, message) }
    }

}
