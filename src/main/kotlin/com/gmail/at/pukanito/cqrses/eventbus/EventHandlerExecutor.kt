/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventbus

import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventHandler
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.util.MessageHandlerExecutor
import com.gmail.at.pukanito.cqrses.util.isEvent
import java.util.function.Predicate
import java.util.logging.Logger
import kotlin.reflect.full.allSuperclasses

/**
 * A handler for events that executes methods marked with @[EventHandler].
 *
 * Events that have no handler associated will be silently ignored.
 *
 * @param context the infrastructure [CqrsContext].
 * @param logger a [Logger].
 */
class EventHandlerExecutor(private val context: CqrsContext,
                           private val logger: Logger):
    MessageHandlerExecutor<Event>(context, { isEvent() }, EventHandler::class) {

    /**
     * Handle the specified event by executing all annotated event handlers that have the specified
     * event type as first [Event] parameter.
     *
     * @param message the event to be handled.
     */
    override fun handleMessage(message: Event) {
        logger.fine { "$message" }
        try {
            val parameterResolver = baseParameterResolver.wrap(Predicate { p -> p.isEvent() }, message)
            listOf(message::class).union(message::class.allSuperclasses).forEach { c ->
                annotatedMethods[c]?.forEach { it.call(parameterResolver, message) } }
        } finally {
            context.eventStore.accept(message)
        }
    }

}
