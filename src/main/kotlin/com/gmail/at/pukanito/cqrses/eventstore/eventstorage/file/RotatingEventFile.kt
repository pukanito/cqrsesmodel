/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import java.nio.file.Files
import java.nio.file.Files.list
import java.nio.file.Path
import java.util.concurrent.atomic.AtomicInteger
import kotlin.streams.asSequence

/**
 * Helper class for rotating event files.
 *
 * It caches directory content once instantiated.
 *
 * @param dir path to the directory where to read/write event files (the directory must exist and be accessible)
 * @param rotationSize when reaching this size, the event file will rotate (optional, default = 10_000_000)
 * @param maxFileNumInitializor supplier for initializing the current max filenumber (optional,
 *   default from reading the directory contents)
 */
class RotatingEventFile(private val dir: Path,
                        val rotationSize: Long = 10_000_000,
                        maxFileNumInitializor: () -> Int = { getMaxFileNum(listFiles(dir)) }) {

    init {
        dir.toFile().mkdirs()
    }

    private var maxFileNum = AtomicInteger(maxFileNumInitializor())

    companion object {

        private const val baseName = "eventstore"
        private val nameRegex = Regex("""${baseName}_(\d+)\.bin""")
        private fun Path.isRegularFile() = Files.isRegularFile(this)
        private val pathIsRegularFile: (Path) -> Boolean = { p -> p.isRegularFile() }
        private fun listFiles(dir: Path): Sequence<Path> = list(dir).asSequence()

        /**
         * Get the value of the highest numbered event file.
         *
         * @param files Sequence of [Path]s to possible event files.
         * @param selector Predicate for selecting event files (selection on name is already included) (Optional,
         *     default = `{ p -> isRegularFile(p) }`
         * @return the value of the highest numbered event file.
         */
        fun getMaxFileNum(files: Sequence<Path>, selector: (Path) -> Boolean = pathIsRegularFile): Int =
            files.filter(selector)
                .map { nameRegex.find(it.fileName.toString())?.groupValues?.get(1) ?: "0" }
                .map(String::toInt)
                .max() ?: 0

    }

    /**
     * @return the current file number.
     */
    fun getCurrentFileNum() = maxFileNum.get()

    /**
     * @return the path of the currently active event file.
     */
    fun getCurrentEventFilePath() = getEventFilePath(maxFileNum.get())

    /**
     * Return the path of the next event file.
     *
     * The file will not be created!! It only returns a [Path].
     *
     * @return the path of the next event file in sequence.
     */
    fun getNextEventFilePath() = getEventFilePath(maxFileNum.incrementAndGet())

    /**
     * Return all [Path]s of the event files in order.
     *
     * @param files Sequence of [Path]s to possible event files (Optional),
     *     default = `listFiles(dir)` i.e. all files in the current RotatingEventFile directory.
     * @param fileNumFilter Predicator for selecting event file numbers based on the nameRegex (Optional),
     *     default = all files.
     * @param fileFilter Predicate for selecting event files (selection on name is already included) (Optional),
     *     default = `{ p -> isRegularFile(p) }`
     */
    fun getEventFilePaths(files: Sequence<Path> = listFiles(dir),
                          fileNumFilter: (Int) -> Boolean = { true },
                          fileFilter: (Path) -> Boolean = pathIsRegularFile): Sequence<Pair<Int, Path>> {
        return files.filter(fileFilter)
            .map { nameRegex.find(it.fileName.toString())?.groupValues?.get(1) ?: "0" }
            .map(String::toInt)
            .filter(fileNumFilter)
            .sorted()
            .asSequence()
            .map { num -> num to dir.resolve("${baseName}_$num.bin").toAbsolutePath() }
    }

    private fun getEventFilePath(num: Int): Path = dir.resolve("${baseName}_$num.bin")

}
