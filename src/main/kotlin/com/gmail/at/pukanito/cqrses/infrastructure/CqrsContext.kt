/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.infrastructure

import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.CommandBus
import com.gmail.at.pukanito.cqrses.EventBus
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.projection.AggregateSnapshotProjection
import com.gmail.at.pukanito.cqrses.projection.AggregatetypeIdProjection
import com.gmail.at.pukanito.cqrses.projection.populator.AggregateSnapshotPopulator
import com.gmail.at.pukanito.cqrses.projection.populator.AggregatetypeIdPopulator
import com.google.inject.Injector
import com.google.inject.Key
import javax.inject.Inject

/**
 * The [CqrsContext] is the glue for the CQRS model.
 *
 * The [CqrsContext] contains all infrastructure instances for handling Commands and Events.
 *
 * The [CqrsContext] depends on the injection framework, the rest of the code does NOT!!! The
 * only exception is that constructors may be marked with @[javax.inject.Inject] so that instances
 * may be created by Guice. The @Inject must be [javax.inject.Inject], not the Guice Inject,
 * so that there are NO dependencies on Guice by other components.
 */
class CqrsContext @Inject constructor(private val injector: Injector) {
    val commandBus by lazy { injector.getInstance(CommandBus::class.java)!! }
    val eventBus by lazy { injector.getInstance(EventBus::class.java)!! }
    val eventStore by lazy { injector.getInstance(EventStore::class.java)!! }
    val aggregateClasses by
            lazy { injector.getInstance(object : Key<Set<@JvmSuppressWildcards AggregateClass>>() {})!!
                .union(getSystemAggregateClasses()) }
    val messageStateContext = MessageStateContext()

    /**
     * Get an instance of the specified type.
     */
    fun <T> getInstance(type: Class<T>) = injector.getInstance(type)!!

    /**
     * @return all aggregate classes used by the framework by bound components.
     */
    private fun getSystemAggregateClasses(): Set<AggregateClass> =
        aggregatetypeIdProjectionSet().union(aggregateSnapshotProjectionSet())

    private fun aggregatetypeIdProjectionSet(): Set<AggregateClass> =
        if (injector.getExistingBinding(Key.get(AggregatetypeIdProjection::class.java)) != null)
            setOf(AggregatetypeIdPopulator.AggregatetypeIdProjectionHandler::class)
        else
            emptySet()

    private fun aggregateSnapshotProjectionSet(): Set<AggregateClass> =
        if (injector.getExistingBinding(Key.get(AggregateSnapshotProjection::class.java)) != null)
            setOf(AggregateSnapshotPopulator.AggregateSnapshotProjectionHandler::class)
        else
            emptySet()

}
