/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection.populator.database

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY
import com.fasterxml.jackson.annotation.PropertyAccessor.FIELD
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.projection.AggregateSnapshotProjection
import com.gmail.at.pukanito.cqrses.projection.populator.AggregateSnapshotRepository
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.util.logging.Logger
import javax.inject.Inject

/**
 * [AggregateSnapshotProjection] in a [Database].
 *
 * @param context the [CqrsContext].
 * @param database the database where to store the snapshots.
 */
class DatabaseAggregateSnapshotRepository @Inject constructor(private val context: CqrsContext,
                                                              private val database: Database,
                                                              private val logger: Logger): AggregateSnapshotRepository {

    companion object {
        private val MAPPER = jacksonObjectMapper()
            .setVisibility(FIELD, ANY)
            .enableDefaultTyping()
            .registerModule(KotlinModule())
            .registerModule(JavaTimeModule())
            .registerModule(Jdk8Module())!!

        /**
         * Get a copy of the mapper for use by queries.
         */
        fun mapper() = MAPPER.copy()!!
    }

    object AggregateSnapshotTable: Table() {
        val id = uuid("id")
        @Suppress("MagicNumber") // Column size is fixed.
        val type = varchar("text", 256)
        val position = text("position")
        val snapshot = text("snapshot")
        override val primaryKey = PrimaryKey(
            id,
            type, name = "PkAggregateSnapshot") }

    @Suppress("unused") // Only used for (de)serialisation to add type info in json string.
    internal class SerialisedPosition(val position: Position<*>)

    @Suppress("unused") // Only used for (de)serialisation to add type info in json string.
    internal class SerialisedAggregate(val aggregate: Aggregate)

    override fun save(aggregateClass: AggregateClass, id: DataId) {
        val position = MAPPER.writeValueAsString(SerialisedPosition(context.eventStore.getPosition()))
        val snapshot =
            MAPPER.writeValueAsString(SerialisedAggregate(context.eventStore.retrieve(aggregateClass, id)))
        val type = aggregateClass.java.typeName
        logger.fine { "$aggregateClass => $position => $snapshot" }
        transaction(database) {
            AggregateSnapshotTable.run {
                if (update({ AggregateSnapshotTable.id eq id and (AggregateSnapshotTable.type eq type) }) {
                        it[AggregateSnapshotTable.position] = position
                        it[AggregateSnapshotTable.snapshot] = snapshot } == 0)
                    insert {
                        it[AggregateSnapshotTable.id] = id
                        it[AggregateSnapshotTable.type] = type
                        it[AggregateSnapshotTable.position] = position
                        it[AggregateSnapshotTable.snapshot] = snapshot } } }
    }

}
