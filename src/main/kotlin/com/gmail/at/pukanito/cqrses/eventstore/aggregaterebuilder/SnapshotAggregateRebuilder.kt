/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.runBlocking
import java.util.logging.Logger
import javax.inject.Inject

/**
 * An [AggregateRebuilder] that retrieves [Aggregate]s from a snapshot repository.
 *
 * @param replayer an [AggregateMessageReplayer].
 * @param snapshotQuery a [AggregateSnapshotQuery] where to retrieve [Aggregate]s.
 * @param fallBackRebuilder [AggregateRebuilder] to use when the Aggregate is not in the snapshot repository.
 * @param logger a [Logger].
 */
class SnapshotAggregateRebuilder @Inject constructor(private val replayer: AggregateMessageReplayer,
                                                     private val snapshotQuery: AggregateSnapshotQuery,
                                                     private val fallBackRebuilder: AggregateRebuilder,
                                                     private val logger: Logger): AggregateRebuilder {

    override suspend fun get(eventStorage: EventStorage, aggregateClass: AggregateClass, id: DataId): Aggregate {
        logger.fine { "$aggregateClass::$id" }
        return snapshotQuery.get(aggregateClass, id).map { info -> runBlocking {
            replayer.replay(CompletableDeferred(info.aggregate),
                eventStorage.get(info.position).filter { it.id == id }) }
        }.orElseGet {
            runBlocking { fallBackRebuilder.get(eventStorage, aggregateClass, id) }
        }
    }

}
