/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.Position.StartPosition
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorageWriter
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileWriter.EventFilePosition
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileWriter.EventFilePosition.Companion.START_EVENT_FILE_POSITION
import java.nio.file.Path
import java.util.concurrent.locks.ReentrantLock
import javax.inject.Inject
import kotlin.concurrent.withLock

/**
 * Writes [Event]s to rotating files.
 *
 * @param rotatingEventFile rotation helper.
 */
class RotatingEventFileWriter @Inject constructor(private val rotatingEventFile: RotatingEventFile) :
    FileEventStorageWriter {

    private val rotationLock = ReentrantLock()

    private var eventFileWriter = getWriter(rotatingEventFile.getNextEventFilePath())

    override fun accept(event: Event) {
        eventFileWriter.accept(event)
        if (rotationRequired()) rotate()
    }

    override fun size() = rotatingEventFile.getEventFilePaths().map { it.second.toFile().length() }.sum()

    override fun close() = eventFileWriter.close()

    internal data class RotatingEventFilePositionSpec(val fileNum: Int, val position: EventFilePosition)

    internal data class RotatingEventFilePosition(private val position: RotatingEventFilePositionSpec) :
        Position<RotatingEventFilePositionSpec> {
        companion object {
            private val comparator = Comparator.comparing(RotatingEventFilePositionSpec::fileNum)
                .thenComparing(RotatingEventFilePositionSpec::position)
            internal val START_ROTATING_EVENT_FILE_POSITION_SPEC =
                RotatingEventFilePositionSpec(0, START_EVENT_FILE_POSITION)
        }
        override fun get(): RotatingEventFilePositionSpec = position
        override fun compareTo(other: Position<*>) = when (other) {
            is StartPosition -> comparator.compare(this.position, START_ROTATING_EVENT_FILE_POSITION_SPEC)
            is RotatingEventFilePosition -> comparator.compare(this.position, other.position)
            else -> throw IllegalStateException("Cannot compare Position: ${other.javaClass.canonicalName}")
        }
    }

    override fun getPosition(): Position<*> = RotatingEventFilePosition(
            RotatingEventFilePositionSpec(rotatingEventFile.getCurrentFileNum(),
                eventFileWriter.getPosition() as EventFilePosition))

    private fun rotationRequired() = eventFileWriter.size() > rotatingEventFile.rotationSize

    private fun rotate() {
        rotationLock.withLock {
            if (rotationRequired()) {
                eventFileWriter.use {
                    eventFileWriter = getWriter(rotatingEventFile.getNextEventFilePath())
                }
            }
        }
    }

    private fun getWriter(path: Path) = EventFileWriter(path)

}
