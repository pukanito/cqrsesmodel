/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util.messagequeue

import com.gmail.at.pukanito.cqrses.Message
import com.gmail.at.pukanito.cqrses.util.MessageQueue
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * A thread safe NON-persistent queue for [Message]s.
 */
class InMemoryMessageQueue<T>: MessageQueue<T> where T: Message {

    private val queue: ConcurrentLinkedQueue<T> = ConcurrentLinkedQueue()

    override fun add(message: T) { queue.add(message) }

    override fun poll(): T? = queue.poll()

    override fun finish(message: T) {
        // Nothing to do
    }

}
