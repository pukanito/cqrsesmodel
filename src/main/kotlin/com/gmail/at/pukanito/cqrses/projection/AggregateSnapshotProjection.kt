/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection

import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.Position

/**
 * Create snapshots of [AggregateClass] instances.
 */
interface AggregateSnapshotProjection {

    /**
     * Update the projection starting with [Event]s at the specified position.
     *
     * @param position position where to start processing [Event]s.
     */
    fun update(position: Position<*>?)

    /**
     * Save a snapshot of the specified [AggregateClass] and id.
     *
     * TODO: save multiple snapshots at once.
     *
     * @param aggregateClass aggregate class to make a snapshot of.
     * @param id id of the aggregate to make a snapshot of.
     */
    fun project(aggregateClass: AggregateClass, id: DataId)

    /**
     * Persist all outstanding snapshots of the projection.
     *
     * Projections may cache snapshots for performance improvement. This method forces
     * all cached snapshots to be persisted.
     */
    fun flush()

}
