/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore

import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.Position.Companion.START_POSITION
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import java.util.function.Consumer

/**
 * Event storage takes care of persisting and retrieving [Event]s somewhere.
 *
 * The consumer persists an [Event].
 *
 * The supplier returns a [Flow] of all stored (not archived) [Event]s in the order they were stored.
 */
interface EventStorage: Consumer<Event> {

    companion object {

        /**
         * A storage that is always empty. It does not store [Event]s and always returns an empty sequence of [Event]s.
         */
        val EMPTY_EVENT_STORAGE = object : EventStorage {
            override fun getPosition() = START_POSITION
            override fun get(position: Position<*>?): Flow<PositionedEvent> = emptyFlow()
            override fun get(count: Int, position: Position<*>?): Flow<PositionedEvent> = emptyFlow()
            override fun archive(eventPredicate: (Event) -> Boolean) {}
            override fun accept(event: Event) {}
        }
    }

    /**
     * Return the current [Position] of the [EventStorage].
     *
     * @return the current [Position] of the [EventStorage].
     */
    fun getPosition(): Position<*>

    /**
     * Get all [Event]s starting at the specified [Position].
     *
     * @param position the [Position] from where to start receiving [Event]s (default is from start of storage).
     * @return a [Flow] of all [Event]s with [Position] starting from the specified position.
     */
    fun get(position: Position<*>? = null): Flow<PositionedEvent>

    /**
     * Get the specified number of [Event]s and the [Position] of the next [Event] starting at the specified [Position].
     *
     * @param count the number of items in the [Sequence]. 0 = everything.
     * @param position the [Position] from where to start receiving [Event]s (default is from start of storage).
     * @return a [Flow] consisting of count elements starting at the specified [Position]. If the are not
     *         enough remaining items in the [EventStorage] only the remaining number of items are returned.
     */
    fun get(count: Int, position: Position<*>? = null): Flow<PositionedEvent>

    /**
     * Archive [Event]s. Start at the first [Event] and continue while the predicate is true.
     * Once the predicate is false all remaining [Event]s will be kept inside the storage.
     *
     * @param eventPredicate predicate indicating whether an event can be archived or not.
     */
    fun archive(eventPredicate: (Event) -> Boolean)

}
