/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Position
import java.util.Optional

/**
 * Repository for retrieving [Aggregate] snapshots.
 */
interface AggregateSnapshotQuery {

    class AggregateSnapshot(val aggregate: Aggregate, val position: Position<*>?)

    /**
     * Get a snapshot of the [Aggregate] with the specified class and id.
     *
     * @param aggregateClass the class of the [Aggregate] to get.
     * @param id the id of the [Aggregate] to get.
     * @return an [Optional] with the specified [Aggregate] info, or empty if no snapshot exists.
     */
    fun get(aggregateClass: AggregateClass, id: DataId): Optional<AggregateSnapshot>

}
