/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventSourcingHandler
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.util.AnnotatedMethodsMapper
import com.gmail.at.pukanito.cqrses.util.ParameterResolver
import com.gmail.at.pukanito.cqrses.util.isAggregate
import com.gmail.at.pukanito.cqrses.util.isEvent
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import java.util.function.Predicate
import java.util.logging.Logger
import javax.inject.Inject
import kotlin.reflect.full.allSuperclasses

/**
 * Replay [Event]s on [EventSourcingHandler]s on an [Aggregate].
 *
 * @param context the infrastructure [CqrsContext].
 * @param logger a [Logger].
 */
class AggregateMessageReplayer  @Inject constructor(private val context: CqrsContext,
                                                    private val logger: Logger) {

    suspend fun replay(deferred: Deferred<Aggregate>, events: Flow<PositionedEvent>): Aggregate = coroutineScope {
        val aggregateEventSourcingMethods = async {
            AnnotatedMethodsMapper(setOf(deferred.await()::class), { isEvent() }, EventSourcingHandler::class) }
        val aggregateParameterResolver = async {
            ParameterResolver.BASE.wrap(Predicate { p -> p.isAggregate() }, deferred.await()) }
        deferred.await().also { aggregate ->
            events.filter { it.position > aggregate.version }.collect { positionedEvent ->
                logger.finest { "$positionedEvent.event -> $aggregate" }
                replayEvent(positionedEvent.event, aggregateEventSourcingMethods.await(),
                    aggregateParameterResolver.await())
                aggregate.version = positionedEvent.position } }
    }

    private fun replayEvent(event: Event,
                            aggregateEventSourcingMethods: AnnotatedMethodsMapper,
                            aggregateParameterResolver: ParameterResolver) {
        val parameterResolver = aggregateParameterResolver.wrap(Predicate { p -> p.isEvent() }, event)
        listOf(event::class).union(event::class.allSuperclasses).forEach { c ->
            aggregateEventSourcingMethods[c]?.callEachMethod(parameterResolver, event.id) }
    }

    private fun List<AnnotatedMethodsMapper.AnnotatedMethod>.callEachMethod(p: ParameterResolver, id: DataId) =
        forEach { a -> a.method.callBy(a.method.parameters.associateWith { p.resolve(it, context, a.instance, id) }) }

}
