/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection

import com.gmail.at.pukanito.cqrses.Position

/**
 * Repository for storing/retrieving [ProjectionData].
 */
interface ProjectionDataRepository {

    /**
     * Projection data.
     *
     * @param aggregatetypeIdProjectionPosition AggregatetypeIdProjection position.
     * @param aggregateSnapshotProjectionPosition AggregateSnapshotProjection position.
     */
    data class ProjectionData(
        var aggregatetypeIdProjectionPosition: Position<*>?,
        var aggregateSnapshotProjectionPosition: Position<*>?
    )

    /**
     * Save the given [ProjectionData].
     *
     * Saving may be asynchronous.
     *
     * @param projectionData the [ProjectionData] to save.
     */
    fun save(projectionData: ProjectionData)

    /**
     * Load the stored [ProjectionData].
     *
     * Loading may retrieve an older version than was just saved due to asynchronous saving.
     * This should NEVER be a problem! Only probably result in some extra work.
     *
     * @return a [ProjectionData].
     */
    fun load(): ProjectionData

}
