/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.Position

/**
 * Projection that stores all unique id's of all [Aggregate]s that source [Event]s.
 * This can be used to identify all instances.
 */
interface AggregatetypeIdProjection {

    /**
     * Update the projection starting with [Event]s at the specified position.
     *
     * @param position position where to start processing [Event]s.
     */
    fun update(position: Position<*>?)

    /**
     * Project a new [Aggregate] type and its id.
     *
     * @param aggregateClass the class type of the aggregate.
     * @param id the id of the aggregate.
     */
    fun project(aggregateClass: AggregateClass, id: DataId)

}
