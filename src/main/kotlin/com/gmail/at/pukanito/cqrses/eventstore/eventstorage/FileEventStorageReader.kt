/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage

import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import kotlinx.coroutines.flow.Flow
import java.util.function.Supplier

interface FileEventStorageReader: Supplier<Flow<PositionedEvent>> {

    /**
     * Get all [Event]s starting at the specified [Position].
     *
     * @param position the [Position] from where to start receiving [Event]s.
     * @return a [Flow] of all [Event]s with [Position] starting from the specified position.
     */
    fun get(position: Position<*>): Flow<PositionedEvent>

    /**
     * Get the specified number of [Event]s and the [Position] of the next [Event] starting at the specified [Position].
     *
     * @param count the number of items in the [Sequence]. 0 = everything.
     * @param position the starting [Position] in the [EventStorage] for the [Sequence].
     * @return a [Flow] consisting of count elements starting at the specified [Position]. If the are not
     *         enough remaining items in the [EventStorage] only the remaining number of items are returned.
     */
    fun get(count: Int, position: Position<*>? = null): Flow<PositionedEvent>

}
