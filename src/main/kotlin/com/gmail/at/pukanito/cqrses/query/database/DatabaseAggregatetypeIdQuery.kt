/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.query.database

import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregatetypeIdRepository.AggregatetypeAndIdTable
import com.gmail.at.pukanito.cqrses.query.AggregatetypeIdQuery
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import javax.inject.Inject

/**
 * [AggregatetypeIdQuery] from database using exposed framework.
 *
 * The query retrieves all unique id/aggregate types. This can be used to identify all instances.
 *
 * @param database the database where to execute the query.
 */
class DatabaseAggregatetypeIdQuery @Inject constructor(private val database: Database): AggregatetypeIdQuery {

    override fun apply(consumer: (AggregateClass, DataId) -> Unit) =
        transaction(database) {
            AggregatetypeAndIdTable.run { selectAll().asSequence().forEach {
                @Suppress("UNCHECKED_CAST") // Projection only stores aggregate types.
                consumer.invoke(Class.forName(it[type].toString()).kotlin as AggregateClass, it[id]) } } }

}
