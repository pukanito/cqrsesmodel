/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorageReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileWriter.EventFilePosition
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.FiledEventReader.FiledEvent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.take
import java.io.IOException
import java.io.RandomAccessFile
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import java.nio.channels.FileChannel.MapMode.READ_ONLY
import java.nio.file.Path

/**
 * Reads [FiledEvent]s from a file. If the file does not exist an empty sequence is returned.
 *
 * Event files are versioned. The reader is aware of the file version and reads accordingly.
 * Currently known versions:
 *
 * - version 1
 *
 * If an unknown file version is encountered, an IOException is thrown.
 *
 * @param eventFilePath path to the file where to read from.
 * @param dispatcher CoroutineDispatcher for providing the Event Flow.
 */
class EventFileReader(eventFilePath: Path,
                      private val dispatcher: CoroutineDispatcher? = null): FileEventStorageReader {

    private val file = eventFilePath.toFile()

    @Suppress("EXPERIMENTAL_API_USAGE")
    private fun <T> Flow<T>.dispatch(): Flow<T> =
        if (dispatcher == null) this else this.flowOn(dispatcher)

    @ExperimentalCoroutinesApi
    override fun get(): Flow<PositionedEvent> =
        if (file.exists()) getFlowFromFile(EventFilePosition(0)).dispatch()
        else emptyFlow()

    @ExperimentalCoroutinesApi
    override fun get(position: Position<*>): Flow<PositionedEvent> {
        if (position is EventFilePosition)
            return if (file.exists()) getFlowFromFile(position).dispatch()
                   else emptyFlow()
        throw IllegalStateException("Cannot handle Position: ${position.javaClass.canonicalName}")
    }

    @ExperimentalCoroutinesApi
    override fun get(count: Int, position: Position<*>?): Flow<PositionedEvent> =
        if (file.exists()) {
            when (position) {
                null -> getFlowFromFile(EventFilePosition(0))
                is EventFilePosition -> getFlowFromFile(position)
                else -> throw IllegalStateException("Cannot handle Position: ${position.javaClass.canonicalName}")
            }.let { if (count <= 0) it else it.take(count) }.dispatch()
        } else emptyFlow()

    @Suppress("BlockingMethodInNonBlockingContext")
    @ExperimentalCoroutinesApi
    private fun getFlowFromFile(position: EventFilePosition): Flow<PositionedEvent> =
        RandomAccessFile(file, "r").let { r -> r.channel.let { c ->
            asFlow(c.asMappedByteBuffer(), position).onCompletion {
                c.close()
                r.close() } } }.map { PositionedEvent(it.first.event, it.second) }

    private fun FileChannel.asMappedByteBuffer() = map(READ_ONLY, 0, size())

    private fun asFlow(buffer: MappedByteBuffer, position: EventFilePosition): Flow<Pair<FiledEvent, Position<*>>> =
        buffer.readHeader(position).generateFlow(buffer)

    private fun MappedByteBuffer.readHeader(position: EventFilePosition): FiledEventReader =
        if (hasRemaining()) readHeaderVersion(position) else FiledEventReaderNoFile()

    private fun MappedByteBuffer.readHeaderVersion(position: EventFilePosition): FiledEventReader =
        when (val version = int) {
            // TODO: header checksum
            FiledEventReaderV1.VERSION -> readHeaderVersionV1(position)
            else -> throw IOException("Unknown event file version: $version")
        }

    private fun MappedByteBuffer.readHeaderVersionV1(position: EventFilePosition): FiledEventReader {
        if (position.get() > position()) position(position.get().toInt())
        return FiledEventReaderV1(FiledEventReaderV1.mapToFiledEventV1)
    }

}
