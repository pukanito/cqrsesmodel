/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.commandbus

import com.gmail.at.pukanito.cqrses.Command
import com.gmail.at.pukanito.cqrses.CommandBus
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.util.MessageQueue
import com.gmail.at.pukanito.cqrses.util.MessageRegulator
import com.google.inject.Inject
import java.util.logging.Logger

/**
 * A [CommandBus] that handles [Command]s given to it.
 *
 * @param context the infrastructure for resolving values.
 * @param messageQueue queue for regulating commands.
 * @param logger a [Logger].
 */
class LocalCommandBus @Inject constructor(context: CqrsContext,
                                          messageQueue: MessageQueue<Command>,
                                          logger: Logger) :
    CommandBus, MessageRegulator<Command>(CommandHandlerExecutor(context, logger), messageQueue){

    override fun handle(command: Command) = accept(command)

}
