package com.gmail.at.pukanito.cqrses.util.singlylinkedlist

import com.gmail.at.pukanito.cqrses.Position
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * @param position the starting [Position] in the list for the [Flow].
 * @return a [Flow] starting at the specified [Position] till the end of the list.
 */
fun <T> SinglyLinkedList<T>.asFlow(position: Position<*>? = null): Flow<Pair<T, Position<*>>> =
    flow { iterator(position).forEach { emit(it) } }

/**
 * @param count the number of items in the [Flow]. 0 <= everything.
 * @param position the starting [Position] in the list for the [Sequence].
 * @return a [Flow] consisting of count elements starting at the specified [Position]. If the are not
 *         enough remaining items in the list only the remaining number of items are returned.
 */
fun <T> SinglyLinkedList<T>.asFlow(count: Int, position: Position<*>? = null): Flow<Pair<T, Position<*>>> =
    flow { iterator(if (count <= 0) Int.MAX_VALUE else count, position).forEach { emit(it) } }
