/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection.populator

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventHandler
import com.gmail.at.pukanito.cqrses.EventSourcingHandler
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.projection.AggregateSnapshotProjection
import com.gmail.at.pukanito.cqrses.util.AnnotatedMethodsMapper
import com.gmail.at.pukanito.cqrses.util.AnnotatedMethodsMapper.AnnotatedMethod
import com.gmail.at.pukanito.cqrses.util.isEvent
import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.RemovalListener
import com.google.common.cache.RemovalNotification
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flattenConcat
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import javax.inject.Inject
import kotlin.reflect.KClass
import kotlin.reflect.full.allSuperclasses

/**
 * Populate aggregate snapshots.
 *
 * !!This populator does not make snapshots of all aggregates!!
 *
 * @param repository the [AggregateSnapshotRepository].
 */
class AggregateSnapshotPopulator @Inject constructor(private val context: CqrsContext,
                                                     private val repository: AggregateSnapshotRepository):
    AggregateSnapshotProjection {

    companion object {
        private const val SNAPSHOT_CACHE_SIZE: Long = 1000
    }

    private val eventSourcingMap =
        AnnotatedMethodsMapper(context.aggregateClasses, { isEvent() }, EventSourcingHandler::class)

    @FlowPreview
    override fun update(position: Position<*>?) {
        runBlocking { getFlowFromEventStore(position).collect { snapshotIfAppropriate(it.aggregateClass, it.id) } }
        flush()
    }

    override fun project(aggregateClass: AggregateClass, id: DataId) = repository.save(aggregateClass, id)

    override fun flush() = snapshotCache.invalidateAll()

    /**
     * Class that supplies an event handler for projecting [Aggregate] snapshots.
     * The [CqrsContext] will register this [AggregateClass] automatically when an [AggregateSnapshotProjection] has
     * been bound to a provider.
     */
    internal class AggregateSnapshotProjectionHandler(id: DataId): Aggregate(id) {
        companion object {
            @EventHandler
            fun handle(event: Event, projection: AggregateSnapshotProjection) {
                if (projection is AggregateSnapshotPopulator)
                    listOf(event::class).union(event::class.allSuperclasses).forEach { c ->
                        (projection.eventSourcingMap[c]?.toList() ?: listOf())
                            .asSequence().map {
                                @Suppress("UNCHECKED_CAST") // eventSourcingMap has aggregate classes as input.
                                it.kClass as AggregateClass to event.id }
                            .forEach { projection.snapshotIfAppropriate(it.first, it.second) } }
            }
        }
    }

    internal data class AggregatetypeAndId(val aggregateClass: AggregateClass, val id: DataId)

    /**
     * Get [Aggregate]-type/id combinations from the event store. Probably contains (many) duplicates since there
     * will be multiple events for the same [Aggregate]-type/id.
     *
     * @param position from which position in the event store to start the sequence.
     * @return a Flow of aggregate type and id (only for classes in the eventSourcingMap).
     */
    @FlowPreview
    private fun getFlowFromEventStore(position: Position<*>?): Flow<AggregatetypeAndId> =
        context.eventStore.get(position)
            .map { positionedEvent -> getAnnotatedMethods(positionedEvent.event::class)
                .map { positionedEvent.id to it.kClass } }
            .flattenConcat()
            .map {
                @Suppress("UNCHECKED_CAST") // eventSourcingMap has aggregate classes as input.
                (AggregatetypeAndId(it.second as AggregateClass, it.first))
            }

    private fun getAnnotatedMethods(kClass: KClass<*>): Flow<AnnotatedMethod> =
        listOf(kClass).union(kClass.allSuperclasses).flatMap { eventSourcingMap[it]?.toList() ?: emptyList() }.asFlow()

    private inner class SnapshotCacheLoader: CacheLoader<AggregatetypeAndId, Unit>() {
        override fun load(ai: AggregatetypeAndId) {}
    }

    private inner class SnapshotRemovalListener: RemovalListener<AggregatetypeAndId, Unit> {
            override fun onRemoval(removal: RemovalNotification<AggregatetypeAndId, Unit>) =
                project(removal.key.aggregateClass, removal.key.id)
        }

    private val snapshotCache = CacheBuilder.newBuilder()
        .maximumSize(SNAPSHOT_CACHE_SIZE)
        .removalListener(SnapshotRemovalListener())
        .build(SnapshotCacheLoader())

    /**
     * Possibly make a snapshot of this aggregate/id, the method decides.
     *
     * @param aggregateClass the type of the [Aggregate].
     * @param id the id of the [Aggregate].
     */
    private fun snapshotIfAppropriate(aggregateClass: AggregateClass, id: DataId) {
        snapshotCache.get(AggregatetypeAndId(aggregateClass, id))
    }

}
