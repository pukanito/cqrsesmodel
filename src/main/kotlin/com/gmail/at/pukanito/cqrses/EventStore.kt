/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses

import kotlinx.coroutines.flow.Flow
import java.util.function.Consumer
import kotlin.reflect.KClass

/**
 * The event store stores and retrieves [Event]s. [Event]s are retrieved in the order they were stored.
 *
 * [Aggregate]s are derived from [Event]s, hence the event store can also retrieve up-to-date [Aggregate]s.
 */
interface EventStore:
    // For storing events.
    Consumer<Event> {

    data class PositionedEvent(val event: Event, val position: Position<*>) {
        val id: DataId get() = event.id
    }

    /**
     * Return the current write [Position] of the [EventStore].
     *
     * @return the current write [Position] of the [EventStore].
     */
    fun getPosition(): Position<*>

    /**
     * Get all [Event]s added after querying the specified [Position].
     *
     * @param position the [Position] from where to start receiving (default is from start of store).
     * @return a [Flow] of all [Event]s with [Position] starting from the specified position.
     */
    fun get(position: Position<*>? = null): Flow<PositionedEvent>

    /**
     * Get the specified number of pairs of [Event] and the [Position] of the next [Event]
     * starting at the specified [Position].
     *
     * @param count the number of [Event]s to read.
     * @param position the [Position] from where to start receiving (default is from start of store).
     * @return a [Flow] of all [Event]s with [Position] starting from the specified position.
     */
    fun get(count: Int, position: Position<*>? = null): Flow<PositionedEvent>

    /**
     * Rebuild the specified [Aggregate] instance and replay all events to get its current state.
     *
     * @param aggregateClass the type of [Aggregate].
     * @param id the id of the [Aggregate].
     */
    fun <T: Aggregate> retrieve(aggregateClass: KClass<out T>, id: DataId): T

}
