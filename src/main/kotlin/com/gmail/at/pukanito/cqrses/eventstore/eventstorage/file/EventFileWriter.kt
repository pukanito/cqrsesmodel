/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.Position.StartPosition
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorageWriter
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.FiledEventReader.FiledEvent
import java.io.FileOutputStream
import java.io.IOException
import java.nio.Buffer
import java.nio.ByteBuffer
import java.nio.file.Path
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Writes [FiledEvent]s to a file. If the file already exists, it appends new [FiledEvent]s.
 * Current file version written is 1.
 *
 * If something goes wrong during writing, the file is rollbacked to its original size.
 *
 * @param eventFilePath path to the file where to write to.
 */
class EventFileWriter(eventFilePath: Path): FileEventStorageWriter {

    companion object {

        private const val FILE_HEADER_SIZE_BYTES = 32
        private const val FILE_VERSION = 1

    }

    init {
        eventFilePath.toFile().parentFile.mkdirs()
    }

    private val channel = FileOutputStream(eventFilePath.toFile(), true).channel
    private val headerLock = ReentrantLock()
    private val filePosition = AtomicLong(0)

    override fun accept(event: Event) {
        if (channel.position() == 0L) writeHeader()
        writeAllOrNothing { FiledEvent(event).writeToFile() }
    }

    override fun size() = channel.size()

    override fun close() {
        channel.close()
    }

    internal data class EventFilePosition(private val position: Long): Position<Long> {
        companion object {
            internal val START_EVENT_FILE_POSITION = EventFilePosition(0)
        }
        override fun get(): Long = position
        override fun compareTo(other: Position<*>) = when (other) {
            is StartPosition -> position.compareTo(START_EVENT_FILE_POSITION.position)
            is EventFilePosition -> position.compareTo(other.position)
            else -> throw IllegalStateException("Cannot compare Position: ${other.javaClass.canonicalName}")
        }
    }

    override fun getPosition(): Position<*> = EventFilePosition(filePosition.get())

    private fun writeHeader() {
        headerLock.withLock {
            if (channel.position() == 0L) {
                channel.write(ByteBuffer.allocate(FILE_HEADER_SIZE_BYTES).apply {
                    // TODO: header checksum
                    putInt(FILE_VERSION)
                    // Java 9+ compatibility fix. https://jira.mongodb.org/browse/JAVA-2559
                    (this as Buffer).flip() })
                filePosition.set(channel.position())
            }
        }
    }

    private fun writeAllOrNothing(writingRunnable: () -> Unit) {
        try {
            writingRunnable()
            filePosition.set(channel.position())
        } catch (ex: IOException) {
            channel.truncate(filePosition.get())
            throw ex
        }
    }

    private fun FiledEvent.writeToFile() = channel.write(serialize())

    private fun FiledEvent.serialize(): ByteBuffer {
        val json = FiledEventReaderV1.mapToStringV1(this)
        val bytes = json.toByteArray(Charsets.UTF_8)
        return ByteBuffer.allocate(Int.SIZE_BYTES + Int.SIZE_BYTES + bytes.size).apply {
            // Java 9+ compatibility fix. https://jira.mongodb.org/browse/JAVA-2559
            ((putInt(bytes.size).putInt(json.hashCode()).put(bytes)) as Buffer).flip()
        }
    }

}
