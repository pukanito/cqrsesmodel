/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.aggregatesnapshotrepository

import com.fasterxml.jackson.module.kotlin.readValue
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateSnapshotQuery
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateSnapshotQuery.AggregateSnapshot
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.AggregateSnapshotTable
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.Companion.mapper
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.SerialisedAggregate
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.SerialisedPosition
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.Optional
import javax.inject.Inject
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.AggregateSnapshotTable.id as dbid
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.AggregateSnapshotTable.type as dbtype

/**
 * An [AggregateSnapshotQuery] in database.
 */
class DatabaseAggregateSnapshotQuery @Inject constructor(private val database: Database):
    AggregateSnapshotQuery {

    companion object {
        private val MAPPER = mapper()
    }

    override fun get(aggregateClass: AggregateClass, id: DataId): Optional<AggregateSnapshot> =
        Optional
            .ofNullable(transaction(database) { AggregateSnapshotTable
                .select { dbid eq id and (dbtype eq aggregateClass.java.typeName) }.firstOrNull() } )
            .map { AggregateSnapshot(
                MAPPER.readValue<SerialisedAggregate>(it[AggregateSnapshotTable.snapshot]).aggregate,
                MAPPER.readValue<SerialisedPosition>(it[AggregateSnapshotTable.position]).position) }

}
