/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util

import com.gmail.at.pukanito.cqrses.util.AnnotatedMethodsMapper.AnnotatedMethod
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.companionObject
import kotlin.reflect.full.companionObjectInstance
import kotlin.reflect.full.createType
import kotlin.reflect.full.functions

/**
 * Find annotated methods in specified classes and store them in a map of 'KType' to 'list of [AnnotatedMethod]'.
 * This map can be used to find all [AnnotatedMethod]s that handle a parameter of a specific KType.
 * KType is the type of one of the parameters of the annotated method. Which parameter to use
 * is specified in the class constructor parameter: `isKeyParameter`.
 *
 * Usage:
 *
 *     val map = AnnotatedMethodsMapper(classes, { isEvent() }, EventHandler::class)
 *
 * will find all methods in `classes` annotated with `@EventHandler` and store them in a map with a list of
 * methods as values and the type of the first parameter of the method that returns true to `isEvent()` as the keys.
 *
 * Access to the methods via:
 *
 * - kTypeMap property: the complete map,
 * - operator []: an item of the map.
 *
 * @param classes the classes where to find annotated methods.
 * @param isKeyParameter predicate that returns true when the parameter (sub)type is to be used as map key.
 * @param annotationsToFind the annotations to find.
 */
class AnnotatedMethodsMapper(classes: Set<KClass<*>>,
                             isKeyParameter: KParameter.() -> Boolean,
                             private vararg val annotationsToFind: KClass<out Annotation>) {

    /**
     * Storage for annotated method and the instance it acts upon. Instance is the companion object instance
     * when not null.
     *
     * @param instance the companion object instance, or null when class method.
     * @param method the method.
     * @param kClass the class where the method is defined.
     */
    class AnnotatedMethod(val instance: Any?, val method: KFunction<*>, val kClass: KClass<*>)

    /**
     * Map of KType to list of [AnnotatedMethod].
     */
    val kTypeMap = classes
        .map { it.annotatedMethods() }.flatten()
        .groupingBy { it.method.parameters.first(isKeyParameter).type }
        .fold({ _, _ -> mutableListOf<AnnotatedMethod>() }) { _, result, item -> result.apply { add(item) } }

    // Return true if the method is annotated with one or more [annotationsToFind].
    private fun KFunction<*>.isAnnotated() = annotationsToFind.any(annotations.map { it.annotationClass }::contains)
    // Return a list of all class methods annotated with any of [annotationsToFind].
    private fun KClass<*>.annotatedClassMethods() = functions.filter { it.isAnnotated() }
    // Return a list of all class methods annotated with any of [annotationsToFind] as AnnotatedMethod.
    private fun KClass<*>.classAnnotatedMethods() =
        annotatedClassMethods().map { AnnotatedMethod(null, it, this) }
    // Return a list of all companion object methods annotated with any of [annotationsToFind].
    private fun KClass<*>.annotatedCompanionMethods() =
        (companionObject?.functions?.filter { it.isAnnotated() } ?: listOf())
    // Return a list of all companion object methods annotated with any of [annotationsToFind] as AnnotateMethod.
    private fun KClass<*>.companionAnnotatedMethods() =
        annotatedCompanionMethods().map { AnnotatedMethod(companionObjectInstance, it, this) }
    // Return a list of all class and companion object methods annotated with any of [annotationsToFind] as
    // AnnotatedMethod.
    private fun KClass<*>.annotatedMethods() =
        listOf(classAnnotatedMethods(), companionAnnotatedMethods()).flatten()

    /**
     * Convenience method for [], shorter access to annotatedMethods with key parameter of a specific KClass.
     *
     * @param kClass the parameter class to get the annotated methods for.
     */
    operator fun get(kClass: KClass<*>) = kTypeMap[kClass.createType()]

}
