/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.process

import com.gmail.at.pukanito.cqrses.Aggregate
import java.util.logging.Logger

/**
 * An [Operation] is something that has to be applied on an [Aggregate].
 * It can be a simple operation or a complex one consisting of many sub-operations.
 *
 * This class helps in modelling a process in data instead of code. Modelling a process in data
 * makes it more maintainable.
 *
 * [done] and [operation] can also be used for validation: let [done] return false if the [Aggregate] is
 * invalid and let [operation] throw an exception, instead of solving the invalid state.
 *
 * @param name a name for identifying an operation.
 * @param logger a [Logger].
 * @param requirements requirements (other [Operation]s) that must have been applied before applying this one.
 * @param done check if the [Operation] has been applied on the specified [Aggregate], should be read-only.
 * @param operation a consumer that performs the [Operation], may publish [Event]s.
 */
open class Operation<T: Aggregate>(private val name: String,
                                   private val logger: Logger,
                                   private val requirements: List<Operation<T>>,
                                   private val done: (T) -> Boolean,
                                   private val operation: (T) -> Unit) {

    /**
     * Apply the operation on the specified [Aggregate] if not yet done. If any requirements have not yet been
     * applied, apply them first.
     *
     * @param aggregate supplier for the [Aggregate] where to apply the operation if not yet applied.
     */
    fun execute(aggregate: () -> T) {
        logger.fine { "$name <" }
        requirements.forEach { it.execute(aggregate) }
        // Requirements may have published events hence the need to rebuild the Aggregate.
        val aggregateInstance = aggregate.invoke()
        if (done(aggregateInstance).not()) {
            logger.info { "$name =" }
            operation(aggregateInstance)
        }
        logger.fine { "$name >" }
    }

}
