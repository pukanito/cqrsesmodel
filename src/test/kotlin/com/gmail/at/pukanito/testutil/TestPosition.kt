package com.gmail.at.pukanito.testutil

import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.Position.StartPosition

/**
 * Position for testing.
 */
class TestPosition(private val value: Int): Position<Int> {

    override fun get() = value

    override fun compareTo(other: Position<*>) = when (other) {
        is StartPosition -> value.compareTo(0)
        is TestPosition -> value.compareTo(other.value)
        else -> throw IllegalStateException("Cannot compare Position: ${other.javaClass.canonicalName}")
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as TestPosition
        if (value != other.value) return false
        return true
    }

    override fun hashCode(): Int {
        return value
    }
}
