package com.gmail.at.pukanito.testutil

import java.io.PrintWriter
import java.io.StringWriter
import java.lang.management.ManagementFactory
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.logging.ConsoleHandler
import java.util.logging.Formatter
import java.util.logging.LogRecord
import java.util.logging.Logger

/**
 * java.util.Logging formatter that also shows the thread name (with coroutine name).
 */
class ThreadnameFormatter: Formatter() {

    companion object {

        /**
         * Install the formatter on a ConsoleHandler and replace the original ConsoleHandler.
         */
        fun init() {
            val globalLogger = Logger.getLogger("")
            globalLogger.handlers.forEach { globalLogger.removeHandler(it) }
            globalLogger.addHandler(ConsoleHandler().apply { formatter = ThreadnameFormatter() })
        }

        private fun LogRecord.stackTraceToString(): String =
            if (thrown != null) {
                StringWriter().let {
                    PrintWriter(it).use { p ->
                        p.println()
                        thrown.printStackTrace(p) }
                    it.toString() }
            } else ""

    }

    @Synchronized
    override fun format(logRecord: LogRecord): String = logRecord.run { String.format(
        "%1\$tF %1\$tT [%2\$s] %3$-7s %4\$s.%5\$s:%n%6\$s %n%7\$s",
        ZonedDateTime.ofInstant(instant, ZoneId.systemDefault()),
        ManagementFactory.getThreadMXBean().getThreadInfo(threadID.toLong()).threadName,
        level.name,
        sourceClassName,
        sourceMethodName,
        message,
        stackTraceToString()) }

}
