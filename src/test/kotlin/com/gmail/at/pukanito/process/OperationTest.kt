/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.process

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.DataId
import com.nhaarman.mockitokotlin2.mock
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsInAnyOrder
import org.hamcrest.collection.IsEmptyCollection.empty
import org.hamcrest.collection.IsIterableContainingInOrder.contains
import org.junit.jupiter.api.Test
import java.util.UUID

class OperationTest {

    class TestAggregate(id: DataId): Aggregate(id) {
        val data: MutableList<String> = mutableListOf()
    }

    @Test
    fun testApplyNotDoneOperationWithoutRequirementsIsApplied() {
        val operation = Operation<TestAggregate>("1", mock(), listOf(), { false }, { it.data += "1" })
        val aggregate = TestAggregate(UUID.randomUUID())
        operation.execute { aggregate }
        assertThat(aggregate.data, contains("1"))
        operation.execute { aggregate }
        assertThat(aggregate.data, contains("1", "1"))
    }

    @Test
    fun testApplyDoneOperationWithoutRequirementsIsNotApplied() {
        val operation = Operation<TestAggregate>("2", mock(), listOf(), { true }, {it.data += "2" })
        val aggregate = TestAggregate(UUID.randomUUID())
        operation.execute { aggregate }
        assertThat(aggregate.data, empty())
    }

    @Test
    fun testRequirementsAreAppliedWhenOpertionIsDone() {
        val requirement = Operation<TestAggregate>("3b", mock(), listOf(), { false }, { it.data += "3b" })
        val operation = Operation("3a", mock(), listOf(requirement), { true }, { it.data += "3a" })
        val aggregate = TestAggregate(UUID.randomUUID())
        operation.execute { aggregate }
        assertThat(aggregate.data, contains("3b"))
    }

    // Example of Operation subclass.
    class TestOperation: Operation<TestAggregate>("4a", mock(), requirements,  { false }, ::operation) {
        companion object {
            private fun operation(aggregate: TestAggregate) = testOperation("4a").invoke(aggregate)
            val testOperation = { x: String -> { a: TestAggregate -> a.data += x } }
            private val requirement1 = Operation("4b", mock(), listOf(), { true }, testOperation("4b"))
            private val requirement2 = Operation("4c", mock(), listOf(), { false },  testOperation("4c"))
            private val requirements = listOf(requirement1, requirement2)
        }
    }

    @Test
    fun testRequirementsAreAppliedWhenOpertionIsNotYetDone() {
        val operation = TestOperation()
        val aggregate = TestAggregate(UUID.randomUUID())
        operation.execute { aggregate }
        assertThat(aggregate.data, containsInAnyOrder("4a", "4c"))
    }

}
