package com.gmail.at.pukanito.playground

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Test
import java.util.logging.Logger

/**
 * Playground for flowOn other dispatcher and making test succeed.
 *
 * Also play with flow parameters: i.e. buffer(..).
 */
@ExperimentalCoroutinesApi
class FlowDispatcherTest {

    @Test
    fun test() = runBlockingTest {
        val logger = Logger.getLogger("Test")
        class Processor {
            suspend fun process(intFlow: Flow<Int>) {
                intFlow.collect {
                    logger.info("Process int") }
                logger.info("Collected") } }
        val eventCount = 30
        val processor = Processor()
        val intFlow = channelFlow {
            repeat(eventCount) {
                logger.info("Emit int")
                send(1) } }.buffer(2).flowOn(Dispatchers.IO)
        logger.info("Start")
        val job = launch { processor.process(intFlow) }
        @Suppress("BlockingMethodInNonBlockingContext")
        runBlocking { job.join() }
        logger.info("Finish")
    }

}
