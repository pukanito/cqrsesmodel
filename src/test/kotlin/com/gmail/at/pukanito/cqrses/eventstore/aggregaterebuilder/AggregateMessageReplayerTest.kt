/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventSourcingHandler
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.testutil.TestPosition
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.UUID

@ExperimentalCoroutinesApi
class AggregateMessageReplayerTest {

    class TestEvent1(id: DataId) : Event(id)

    class TestEvent2(id: DataId) : Event(id)

    class TestAggregate1(id: DataId) : Aggregate(id) {
        companion object { var count = 0 }
        @EventSourcingHandler
        @Suppress("unused", "UNUSED_PARAMETER")
        fun handle1E(event: TestEvent1, event2: Event) { count++ }
        @EventSourcingHandler
        @Suppress("unused", "UNUSED_PARAMETER")
        fun handle2(event: TestEvent2) { count += 10 }
        @EventSourcingHandler
        @Suppress("unused", "UNUSED_PARAMETER")
        fun handleE(event: Event) { count += 100 }
    }

    @Test
    fun testCorrectHandlersAreCalledByEventtypeAndSupertype() = runBlockingTest {
        val context = mock<CqrsContext>()
        val aggregateMessageReplayer = AggregateMessageReplayer(context, mock())
        val id = UUID.randomUUID()
        val deferredAggregate = CompletableDeferred(TestAggregate1(id))
        val event1 = PositionedEvent(TestEvent1(id), TestPosition(1))
        val event2 = PositionedEvent(TestEvent2(id), TestPosition(2))
        TestAggregate1.count  = 0
        aggregateMessageReplayer.replay(deferredAggregate, flowOf(event1, event2))
        assertThat(TestAggregate1.count, equalTo(211))
    }

    @Test
    fun testOnlyApplyUnhandledEvents() = runBlockingTest {
        val aggregateMessageReplayer = AggregateMessageReplayer(mock(), mock())
        val id = UUID.randomUUID()
        val deferredAggregate = CompletableDeferred(TestAggregate1(id))
        val event1 = PositionedEvent(TestEvent1(id), TestPosition(1))
        val event2 = PositionedEvent(TestEvent2(id), TestPosition(2))
        TestAggregate1.count  = 0
        aggregateMessageReplayer.replay(deferredAggregate, flowOf(event1))
        assertThat(TestAggregate1.count, equalTo(101))
        aggregateMessageReplayer.replay(deferredAggregate, flowOf(event1, event2))
        assertThat(TestAggregate1.count, equalTo(211))
    }

    @Test
    fun testApplyEventsCreatedInDifferentOrder() = runBlockingTest {
        val aggregateMessageReplayer = AggregateMessageReplayer(mock(), mock())
        val id = UUID.randomUUID()
        val deferredAggregate = CompletableDeferred(TestAggregate1(id))
        val event1Position = TestPosition(5)
        val event2Position = TestPosition(3)
        val event1 = PositionedEvent(TestEvent1(id), event1Position)
        val event2 = PositionedEvent(TestEvent2(id), event2Position)
        TestAggregate1.count  = 0
        aggregateMessageReplayer.replay(deferredAggregate, flowOf(event2, event1))
        assertThat(TestAggregate1.count, equalTo(211))
    }

    class TestAggregate2(id: DataId) : Aggregate(id) {
        // Most specific type should be first parameter, not last. Otherwise the method is also
        // called with TestEvent1 and that will not fit event2 parameter.
        @EventSourcingHandler
        @Suppress("unused", "UNUSED_PARAMETER")
        fun handleE2(event: Event, event2: TestEvent2) {}
    }

    @Test
    fun testIncorrectOrderOfSupertypeParametersInHandlerThrowsIllegalArgumentException() {
        val aggregateMessageReplayer = AggregateMessageReplayer(mock(), mock())
        val id = UUID.randomUUID()
        val deferredAggregate = CompletableDeferred(TestAggregate2(id))
        val event = PositionedEvent(TestEvent1(id), TestPosition(1))
        assertThrows<IllegalArgumentException> { runBlocking { aggregateMessageReplayer.replay(deferredAggregate, flowOf(event)) } }
    }

    @Test
    fun testIncorrectOrderOfSupertypeParametersInHandlerWithoutEvents() = runBlockingTest {
        val aggregateMessageReplayer = AggregateMessageReplayer(mock(), mock())
        val id = UUID.randomUUID()
        val deferredAggregate = CompletableDeferred(TestAggregate2(id))
        aggregateMessageReplayer.replay(deferredAggregate, emptyFlow())
    }

    @Suppress("unused")
    @Disabled
    fun testManyEvents() = runBlockingTest {
        val eventCount = 100_000
        val context = mock<CqrsContext>()
        val aggregateMessageReplayer = AggregateMessageReplayer(context, mock())
        val id = UUID.randomUUID()
        val event = PositionedEvent(TestEvent1(id), mock())
        val eventsFlow = channelFlow { repeat(eventCount) { send(event) } }.flowOn(Dispatchers.IO)
        val deferredAggregate = CompletableDeferred(TestAggregate1(id))
        TestAggregate1.count  = 0
        val job = launch {
            aggregateMessageReplayer.replay(deferredAggregate, eventsFlow)
            assertThat(TestAggregate1.count, equalTo(eventCount * 101)) }
        @Suppress("BlockingMethodInNonBlockingContext")
        runBlocking { job.join() }
    }

}
