/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.Command
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.google.inject.ConfigurationException
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.IsInstanceOf.instanceOf
import org.hamcrest.core.IsSame.sameInstance
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import java.util.UUID
import java.util.function.Predicate
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KType
import kotlin.reflect.full.companionObject
import kotlin.reflect.full.companionObjectInstance
import kotlin.reflect.full.createType
import kotlin.reflect.full.functions
import kotlin.reflect.full.isSubtypeOf

class ParameterResolverTest {

    class TestCommand(id: DataId) : Command(id)

    class TestEvent(id: DataId) : Event(id)

    class UnresolvableType

    @Suppress("unused", "UNUSED_PARAMETER")
    class TestAggregate(id: DataId): Aggregate(id) {
        companion object {
            fun handleWithCommandParameter(command: TestCommand) { }
            fun handleWithDataIdParameter(id: DataId) { }
            fun handleWithContextParameter(context: CqrsContext) { }
        }
        fun handleWithAggregateParameter0() { }
        fun handleWithGuiceBoundParameter(testInterface: TestInterface) { }
        fun handleWithEventParameter(event: TestEvent) { }
        fun handleWithUnresolvableParameter(p: UnresolvableType) { }
    }

    interface TestInterface

    private fun getCompanionObjectFunctionWithParameterType(type: KType): KFunction<*> =
        TestAggregate::class.companionObject!!.functions
            .first { it.parameters.size > 1 && it.parameters[1].type.isSubtypeOf(type) }

    private fun getFunctionWithParameterType(index: Int, type: KType): KFunction<*> =
        TestAggregate::class.functions
            .first { it.parameters.size > index && it.parameters[index].type.isSubtypeOf(type) }

    // For simple assignment of test data.
    private inner class TestCompanionObjectData(val type: KClass<*>) {
        operator fun component1() = TestAggregate::class.companionObjectInstance
        operator fun component2() = getCompanionObjectFunctionWithParameterType(type.createType())
        operator fun component3() = mock<CqrsContext>()
        operator fun component4() = UUID.randomUUID()!!
    }

    // For simple assignment of test data.
    private inner class TestData(val index: Int, val type: KClass<*>) {
        operator fun component1() = TestAggregate::class.companionObjectInstance
        operator fun component2() = getFunctionWithParameterType(index, type.createType())
        operator fun component3() = mock<CqrsContext>()
        operator fun component4() = UUID.randomUUID()!!
    }

    private val parameterResolver = ParameterResolver.BASE

    @Test
    fun testCompanionObjectParameterIsResolved() {
        val (receiver, function, context, uuid) = TestCompanionObjectData(Command::class)
        assertThat(function.name, equalTo("handleWithCommandParameter"))
        val value = parameterResolver.resolve(function.parameters[0], context, receiver, uuid)
        assertThat(value, sameInstance(receiver))
    }

    @Test
    fun testCommandParameterIsResolvedWhenWrapped() {
        val (_, function, context, uuid) = TestCompanionObjectData(Command::class)
        assertThat(function.name, equalTo("handleWithCommandParameter"))
        val command = TestCommand(uuid)
        val p = parameterResolver.wrap(Predicate { p -> p.isCommand() }, command)
        val value = p.resolve(function.parameters[1], context, null, uuid) as TestCommand
        assertThat(value, sameInstance(command))
    }

    @Test
    fun testEventParameterIsResolvedWhenWrapped() {
        val (_, function, context, uuid) = TestData(1, Event::class)
        assertThat(function.name, equalTo("handleWithEventParameter"))
        val event = TestEvent(uuid)
        val p = parameterResolver.wrap(Predicate { p -> p.isEvent() }, event)
        val value = p.resolve(function.parameters[1], context, null, uuid) as TestEvent
        assertThat(value, sameInstance(event))
    }

    @Test
    fun testDataIdParameterIsResolved() {
        val (_, function, context, uuid) = TestCompanionObjectData(DataId::class)
        assertThat(function.name, equalTo("handleWithDataIdParameter"))
        val value = parameterResolver.resolve(function.parameters[1], context, null, uuid) as DataId
        assertThat(value, sameInstance(uuid))
    }

    @Test
    fun testContextParameterIsResolved() {
        val (_, function, context, uuid) = TestCompanionObjectData(CqrsContext::class)
        assertThat(function.name, equalTo("handleWithContextParameter"))
        val value = parameterResolver.resolve(function.parameters[1], context, null, uuid) as CqrsContext
        assertThat(value, sameInstance(context))
    }

    @Test
    fun testGuiceBoundParameterIsResolved() {
        val (_, function, context, uuid) = TestData(1, TestInterface::class)
        assertThat(function.name, equalTo("handleWithGuiceBoundParameter"))
        parameterResolver.resolve(function.parameters[1], context, null, uuid)
        verify(context).getInstance(eq(TestInterface::class.java))
    }

    @Test
    fun testAggregateParameterIsResolved() {
        val (_, function, context, uuid) = TestData(0, Aggregate::class)
        assertThat(function.name, equalTo("handleWithAggregateParameter0"))
        val mockEventStore = mock<EventStore>()
        whenever(context.eventStore) doReturn mockEventStore
        whenever(context.eventStore.retrieve(eq(TestAggregate::class), any())) doReturn TestAggregate(uuid)
        val value = parameterResolver.resolve(function.parameters[0], context, null, uuid) as TestAggregate
        assertThat(value, instanceOf(TestAggregate::class.java))
        assertThat(value.id, sameInstance(uuid))
    }

    @Test
    fun testUnresolvableParameter() {
        val (_, function, context, uuid) = TestData(1, UnresolvableType::class)
        assertThat(function.name, equalTo("handleWithUnresolvableParameter"))
        whenever(context.getInstance(UnresolvableType::class.java)) doThrow ConfigurationException(listOf())
        assertThrows(ConfigurationException::class.java) {
            parameterResolver.resolve(function.parameters[1], context, null, uuid)
        }
    }

}
