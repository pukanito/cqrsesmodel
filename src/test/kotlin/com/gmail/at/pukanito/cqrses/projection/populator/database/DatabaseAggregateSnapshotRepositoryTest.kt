/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection.populator.database

import com.fasterxml.jackson.module.kotlin.readValue
import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.CommandBus
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventBus
import com.gmail.at.pukanito.cqrses.EventSourcingHandler
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventbus.LocalEventBus
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.DelegatingEventStore
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateMessageReplayer
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.NoMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.InMemoryEventStorage
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.projection.AggregateSnapshotProjection
import com.gmail.at.pukanito.cqrses.projection.populator.AggregateSnapshotPopulator
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.AggregateSnapshotTable
import com.gmail.at.pukanito.cqrses.util.MessageQueue
import com.gmail.at.pukanito.cqrses.util.messagequeue.InMemoryMessageQueue
import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.TypeLiteral
import com.nhaarman.mockitokotlin2.mock
import com.zaxxer.hikari.HikariDataSource
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.util.UUID
import java.util.logging.Logger
import javax.sql.DataSource

class DatabaseAggregateSnapshotRepositoryTest {

    private lateinit var configuration: CqrsConfiguration

    class TestEvent(id: DataId, val data: String): Event(id)

    class TestAggregate(id: DataId): Aggregate(id) {
        var data: String = ""; private set
        @Suppress("unused") @EventSourcingHandler
        fun handle(event: TestEvent) { data = event.data }
    }

    private fun inMemoryModule(configuration: CqrsConfiguration) = object : AbstractModule() {
        override fun configure() {
            val messageQueueEventBaseType = object : TypeLiteral<MessageQueue<Event>>() {}
            val messageQueueEventImplementationType = object : TypeLiteral<InMemoryMessageQueue<Event>>() {}
            bind(CqrsConfiguration::class.java).toInstance(configuration)
            bind(CqrsContext::class.java).`in`(Singleton::class.java)
            bind(CommandBus::class.java).toInstance(mock())
            bind(EventBus::class.java).to(LocalEventBus::class.java).`in`(Singleton::class.java)
            bind(messageQueueEventBaseType).to(messageQueueEventImplementationType).`in`(Singleton::class.java)
            bind(object: TypeLiteral<Function0<Position<*>?>>() {}).toInstance { null }
            bind(EventStore::class.java).to(DelegatingEventStore::class.java).`in`(Singleton::class.java)
            bind(EventStorage::class.java).to(InMemoryEventStorage::class.java).`in`(Singleton::class.java)
            bind(AggregateRebuilder::class.java).to(NoMemoryAggregateRebuilder::class.java).`in`(Singleton::class.java)
            bind(AggregateMessageReplayer::class.java).`in`(Singleton::class.java)
        }
        @Provides @Singleton
        fun provideAggregates(): Set<AggregateClass> = setOf(TestAggregate::class)
        @Provides @Singleton
        fun provideDatabase(dataSource: DataSource): Database = Database.connect(dataSource)
        @Provides @Singleton
        fun provideDatasource(config: CqrsConfiguration): DataSource = HikariDataSource().apply {
            jdbcUrl = config.database.jdbcUrl
            driverClassName = config.database.jdbcDriverClassName
            minimumIdle = config.database.maximumIdle
            maximumPoolSize = config.database.maximumPoolSize }
        @Provides @Singleton
        fun provideAggregateSnapshotProjection(context: CqrsContext, database: Database, logger: Logger): AggregateSnapshotProjection =
            AggregateSnapshotPopulator(context, DatabaseAggregateSnapshotRepository(context, database, logger))
    }

    @BeforeEach
    fun cleanDatabase(testInfo: TestInfo) {
        configuration = testConfig(testInfo)
        val database = Database.connect(HikariDataSource().apply {
            jdbcUrl = configuration.database.jdbcUrl
            driverClassName = configuration.database.jdbcDriverClassName
            minimumIdle = configuration.database.maximumIdle
            maximumPoolSize = configuration.database.maximumPoolSize })
        transaction(database) { SchemaUtils.create(AggregateSnapshotTable) }
        transaction(database) { AggregateSnapshotTable.deleteAll() }
    }

    @Test
    fun testSnapshotCreation() {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val context = Guice
            .createInjector(inMemoryModule(configuration))
            .getInstance(CqrsContext::class.java)
        val database = context.getInstance(Database::class.java)
        assertThat(selectById(database, id1).count(), equalTo(0))
        assertThat(selectById(database, id2).count(), equalTo(0))
        val snapshotProjection = context.getInstance(AggregateSnapshotProjection::class.java)
        snapshotProjection.project(TestAggregate::class, id1)
        assertThat(selectById(database, id1).count(), equalTo(1))
        assertThat(selectById(database, id2).count(), equalTo(0))
        snapshotProjection.project(TestAggregate::class, id2)
        assertThat(selectById(database, id1).count(), equalTo(1))
        assertThat(selectById(database, id2).count(), equalTo(1))
        // Test deserialization
        val mapper = DatabaseAggregateSnapshotRepository.mapper()
        assertThat(mapper.readValue<SerialisedAggregate>(snapshotById(database, id1)).aggregate.id, equalTo(id1))
        assertThat(mapper.readValue<SerialisedAggregate>(snapshotById(database, id2)).aggregate.id, equalTo(id2)) // Test save new snapshot.
        context.eventStore.accept(TestEvent(id1, "test"))
        snapshotProjection.project(TestAggregate::class, id1)
        assertThat((mapper.readValue<SerialisedAggregate>(snapshotById(database, id1)).aggregate as TestAggregate).data, equalTo("test"))
    }

    private class SerialisedAggregate(val aggregate: Aggregate)

    private fun snapshotById(database: Database, id1: UUID): String =
        selectById(database, id1).single()[AggregateSnapshotTable.snapshot]

    private fun selectById(database: Database, id1: UUID): List<ResultRow> =
        transaction(database) { AggregateSnapshotTable.run {
            select { id eq id1 and (type eq TestAggregate::class.java.typeName) }.toList() } }

}
