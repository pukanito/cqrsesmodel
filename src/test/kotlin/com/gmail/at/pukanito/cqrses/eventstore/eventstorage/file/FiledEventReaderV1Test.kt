/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import com.fasterxml.jackson.annotation.JsonCreator
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.FiledEventReader.FiledEvent
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.IOException
import java.nio.MappedByteBuffer
import java.util.UUID

@ExperimentalCoroutinesApi
class FiledEventReaderV1Test {

    class TestEvent @JsonCreator constructor(id: UUID): Event(id)

    @Test
    fun testEmptyBufferReturnsEmtpySequenceAndPerformsOnEndOfBuffer() = runBlockingTest {
        val filedEventReader = FiledEventReaderV1 { FiledEvent(TestEvent(UUID.randomUUID())) }
        val buffer = mock<MappedByteBuffer>()
        whenever(buffer.hasRemaining()) doReturn false
        val flow: Flow<FiledEvent> = filedEventReader.generateFlow(buffer).map { it.first }
        assertThat(flow.count(), equalTo(0))
    }

    @Test
    fun testFilledBufferReturnsCorrectNumberOfEventsAndPerformsOnEndOfBuffer() = runBlockingTest {
        val filedEventReader = FiledEventReaderV1 { FiledEvent(TestEvent(UUID.randomUUID())) }
        val buffer = mock<MappedByteBuffer>()
        whenever(buffer.hasRemaining()) doReturn true doReturn true doReturn false
        whenever(buffer.int) doReturn 2 doReturn 3938 doReturn 2 doReturn 3938
        whenever(buffer.get(any(), any(), any())) doAnswer { invocation ->
            val bytes = invocation.getArgument(0) as ByteArray
            bytes[0] = '{'.toByte()
            bytes[1] = '}'.toByte()
            buffer
        }
        val flow: Flow<FiledEvent> = filedEventReader.generateFlow(buffer).map { it.first }
        assertThat(flow.count(), equalTo(2))
    }

    @Test
    fun testFilledBufferWithChecksumErrorThrowsExceptionAndPerformsOnEndOfBuffer() {
        val filedEventReader = FiledEventReaderV1 { FiledEvent(TestEvent(UUID.randomUUID())) }
        val buffer = mock<MappedByteBuffer>()
        whenever(buffer.hasRemaining()) doReturn true doReturn true doReturn false
        whenever(buffer.int) doReturn 2 doReturn 393800 doReturn 2 doReturn 393800
        whenever(buffer.get(any(), any(), any())) doAnswer { invocation ->
            val bytes = invocation.getArgument(0) as ByteArray
            bytes[0] = '{'.toByte()
            bytes[1] = '}'.toByte()
            buffer
        }
        assertThrows<IOException> { runBlocking { filedEventReader.generateFlow(buffer).count() } }
    }

}
