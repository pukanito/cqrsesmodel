/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses

import com.gmail.at.pukanito.cqrses.CqrsNoMemoryRebuilderTest.Companion.checkEventsArriveAtCorrectAggregate
import com.gmail.at.pukanito.cqrses.CqrsNoMemoryRebuilderTest.TestAggregate
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.DelegatingEventStore
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateMessageReplayer
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.InMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.NoMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorage
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorageReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorageWriter
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileWriter
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.deleteEventFile
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.projection.AggregatetypeIdProjection
import com.gmail.at.pukanito.cqrses.projection.populator.AggregatetypeIdPopulator
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregatetypeIdRepository
import com.google.inject.AbstractModule
import com.google.inject.Provider
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.TypeLiteral
import com.nhaarman.mockitokotlin2.mock
import com.zaxxer.hikari.HikariDataSource
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.util.logging.Logger
import javax.inject.Inject
import javax.sql.DataSource

/**
 * Test:
 * - [Model]
 * - [Context]
 * - [DelegatingEventStore]
 * - [FileEventStorage]
 * - [EventFileReader]
 * - [EventFileWriter]
 * - [InMemoryAggregateRebuilder]
 * - [DatabaseAggregatetypeIdRepository] (initialization only)
 *
 * excluded:
 * - [CommandBus]
 * - [EventBus]
 *
 * Test handling of events by @[EventSourcingHandler]s
 */
class CqrsEventFileStoreTest {

    private lateinit var testModule: AbstractModule

    @BeforeEach
    fun deleteEventFileAndInitializaTestModule(testInfo: TestInfo) {
        val configuration = testConfig(testInfo)
        deleteEventFile(configuration.eventFileStorage.path)

        testModule = object : AbstractModule() {
            val mockCommandBus = mock<CommandBus>()
            val mockEventBus = mock<EventBus>()
            override fun configure() {
                bind(CqrsConfiguration::class.java).toInstance(configuration)
                bind(CqrsContext::class.java).`in`(Singleton::class.java)
                bind(CommandBus::class.java).toInstance(mockCommandBus)
                bind(EventBus::class.java).toInstance(mockEventBus)
                bind(object: TypeLiteral<Function0<Position<*>?>>() {}).toInstance { null }
                bind(EventStore::class.java).to(DelegatingEventStore::class.java).`in`(Singleton::class.java)
                bind(EventStorage::class.java).to(FileEventStorage::class.java).`in`(Singleton::class.java)
                bind(AggregateMessageReplayer::class.java).`in`(Singleton::class.java)
                bind(AggregatetypeIdProjection::class.java).toProvider(InMemoryDbAggregatetypeIdProjectionProvider::class.java).asEagerSingleton()
            }
            @Provides @Singleton
            fun provideAggregates(): Set<AggregateClass> = setOf(TestAggregate::class)
            @Provides @Singleton
            fun provideAggregateRebuilder(replayer: AggregateMessageReplayer,
                                          config: CqrsConfiguration,
                                          logger: Logger): AggregateRebuilder =
                InMemoryAggregateRebuilder(replayer,
                    config.aggregateCache.maximumSize,
                    config.aggregateCache.expireAfterWriteSeconds,
                    NoMemoryAggregateRebuilder(replayer, logger),
                    logger)
            @Provides @Singleton
            fun provideFileEventStorageReader(config: CqrsConfiguration): FileEventStorageReader =
                EventFileReader(config.eventFileStorage.path)
            @Provides @Singleton
            fun provideFileEventStorageWriter(config: CqrsConfiguration): FileEventStorageWriter =
                EventFileWriter(config.eventFileStorage.path)
            @Provides @Singleton
            fun provideDatabase(dataSource: DataSource): Database = Database.connect(dataSource)
            @Provides @Singleton
            fun provideDatasource(config: CqrsConfiguration): DataSource = HikariDataSource().apply {
                jdbcUrl = config.database.jdbcUrl
                driverClassName = config.database.jdbcDriverClassName
                minimumIdle = config.database.maximumIdle
                maximumPoolSize = config.database.maximumPoolSize }
        }
    }

    private class InMemoryDbAggregatetypeIdProjectionProvider @Inject constructor(
            val context: CqrsContext,
            val database: Database): Provider<AggregatetypeIdProjection> {
        override fun get(): AggregatetypeIdProjection =
            AggregatetypeIdPopulator(context, DatabaseAggregatetypeIdRepository(database))
    }

    @Test
    fun testEventsArriveAtCorrectAggregateWithLessEventHandlingDueToCaching() {
        checkEventsArriveAtCorrectAggregate(testModule)
        assertThat(TestAggregate.eventHandlingCount, equalTo(4))
    }

}
