/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses

import com.gmail.at.pukanito.cqrses.CqrsInMemoryBusNoMemoryRebuilderTest.Companion.checkCommandsArriveAtCorrectAggregate
import com.gmail.at.pukanito.cqrses.CqrsInMemoryBusNoMemoryRebuilderTest.TestAggregate
import com.gmail.at.pukanito.cqrses.CqrsInMemoryBusNoMemoryRebuilderTest.TestAggregate.Companion.resourceEventHandlingCount
import com.gmail.at.pukanito.cqrses.commandbus.LocalCommandBus
import com.gmail.at.pukanito.cqrses.eventbus.LocalEventBus
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.DelegatingEventStore
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateMessageReplayer
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.InMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.NoMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.InMemoryEventStorage
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.util.MessageQueue
import com.gmail.at.pukanito.cqrses.util.messagequeue.PersistentMessageQueue
import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.TypeLiteral
import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.util.logging.Logger

/**
 * Test:
 * - [Model]
 * - [Context]
 * - [DelegatingEventStore]
 * - [InMemoryEventStorage]
 * - [InMemoryAggregateRebuilder]
 * - [LocalCommandBus]
 * - [LocalEventBus]
 * - [PersistentMessageQueue] with [InMemoryEventStorage]
 *
 * Test handling of events by @[EventSourcingHandler]s and @[CommandHandler]s
 */
class CqrsPersistentBusInMemoryRebuilderTest {

    private lateinit var testModule: AbstractModule

    @BeforeEach
    fun initializeTestModule(testInfo: TestInfo) {
        testModule = object : AbstractModule() {
            override fun configure() {
                bind(CqrsConfiguration::class.java).toInstance(testConfig(testInfo))
                bind(CqrsContext::class.java).`in`(Singleton::class.java)
                bind(CommandBus::class.java).to(LocalCommandBus::class.java).`in`(Singleton::class.java)
                bind(EventBus::class.java).to(LocalEventBus::class.java).`in`(Singleton::class.java)
                bind(object: TypeLiteral<Function0<Position<*>?>>() {}).toInstance { null }
                bind(EventStore::class.java).to(DelegatingEventStore::class.java).`in`(Singleton::class.java)
                bind(EventStorage::class.java).to(InMemoryEventStorage::class.java).`in`(Singleton::class.java)
                bind(AggregateMessageReplayer::class.java).`in`(Singleton::class.java)
            }
            @Provides @Singleton
            fun provideAggregates(): Set<AggregateClass> = setOf(TestAggregate::class)
            @Provides @Singleton
            fun provideAggregateRebuilder(replayer: AggregateMessageReplayer,
                                          config: CqrsConfiguration,
                                          logger: Logger): AggregateRebuilder =
                InMemoryAggregateRebuilder(replayer,
                    config.aggregateCache.maximumSize,
                    config.aggregateCache.expireAfterWriteSeconds,
                    NoMemoryAggregateRebuilder(replayer, logger),
                    logger)
            @Provides @Singleton
            fun provideMessageQueueForCommands(logger: Logger): MessageQueue<Command> =
                PersistentMessageQueue(InMemoryEventStorage(logger))
            @Provides @Singleton
            fun provideMessageQueueForEvents(logger: Logger): MessageQueue<Event> =
                PersistentMessageQueue(InMemoryEventStorage(logger))
        }
    }

    @Test
    fun testEventsArriveAtCorrectAggregateWithLessEventHandlingDueToCaching() {
        checkCommandsArriveAtCorrectAggregate(testModule)
        MatcherAssert.assertThat(resourceEventHandlingCount, equalTo(4)
        )
    }

}
