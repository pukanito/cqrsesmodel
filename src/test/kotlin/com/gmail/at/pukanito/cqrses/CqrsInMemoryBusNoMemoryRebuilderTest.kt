/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses

import com.gmail.at.pukanito.cqrses.CqrsInMemoryBusNoMemoryRebuilderTest.TestAggregate.Companion.resourceEventHandlingCount
import com.gmail.at.pukanito.cqrses.commandbus.LocalCommandBus
import com.gmail.at.pukanito.cqrses.eventbus.LocalEventBus
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.DelegatingEventStore
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateMessageReplayer
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.NoMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.InMemoryEventStorage
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.util.MessageQueue
import com.gmail.at.pukanito.cqrses.util.messagequeue.InMemoryMessageQueue
import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.TypeLiteral
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsIterableContainingInOrder.contains
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import java.util.UUID

/**
 * Test:
 * - [Model]
 * - [Context]
 * - [DelegatingEventStore]
 * - [InMemoryEventStorage]
 * - [NoMemoryAggregateRebuilder]
 * - [LocalCommandBus]
 * - [LocalEventBus]
 * - [InMemoryMessageQueue]
 *
 * Test handling of events by @[EventSourcingHandler]s and @[CommandHandler]s
 */
class CqrsInMemoryBusNoMemoryRebuilderTest {

    private val testModule = object : AbstractModule() {
        override fun configure() {
            val messageQueueCommandBaseType = object : TypeLiteral<MessageQueue<Command>>() {}
            val messageQueueCommandImplementationType = object : TypeLiteral<InMemoryMessageQueue<Command>>() {}
            val messageQueueEventBaseType = object : TypeLiteral<MessageQueue<Event>>() {}
            val messageQueueEventImplementationType = object : TypeLiteral<InMemoryMessageQueue<Event>>() {}
            bind(CqrsContext::class.java).`in`(Singleton::class.java)
            bind(CommandBus::class.java).to(LocalCommandBus::class.java).`in`(Singleton::class.java)
            bind(messageQueueCommandBaseType).to(messageQueueCommandImplementationType).`in`(Singleton::class.java)
            bind(EventBus::class.java).to(LocalEventBus::class.java).`in`(Singleton::class.java)
            bind(messageQueueEventBaseType).to(messageQueueEventImplementationType).`in`(Singleton::class.java)
            bind(object: TypeLiteral<Function0<Position<*>?>>() {}).toInstance { null }
            bind(EventStore::class.java).to(DelegatingEventStore::class.java).`in`(Singleton::class.java)
            bind(EventStorage::class.java).to(InMemoryEventStorage::class.java).`in`(Singleton::class.java)
            bind(AggregateRebuilder::class.java).to(NoMemoryAggregateRebuilder::class.java).`in`(Singleton::class.java)
            bind(AggregateMessageReplayer::class.java).`in`(Singleton::class.java)
        }
        @Provides @Singleton
        fun provideAggregates(): Set<AggregateClass> = setOf(TestAggregate::class)
    }

    class TestAggregate(id: DataId): Aggregate(id) {
        companion object {
            var resourceEventHandlingCount = 0
        }
        val data: MutableList<String> = mutableListOf()
        @CommandHandler @Suppress("unused")
        fun handleCommand(command: TestCommand, context: CqrsContext) {
//            data += command.data
            context.eventBus.publish(TestEvent(id, command.data))
        }
        @EventSourcingHandler @Suppress("unused")
        fun handleSourcingEvent(event: TestEvent) {
            data += event.data
            resourceEventHandlingCount++
        }
    }

    class TestCommand(id: DataId, val data: String): Command(id)

    class TestEvent(id: DataId, val data: String): Event(id)

    @Test
    fun testCommandsArriveAtCorrectAggregate() {
        checkCommandsArriveAtCorrectAggregate(testModule)
        assertThat(resourceEventHandlingCount, equalTo(9))
    }

    companion object {
        fun checkCommandsArriveAtCorrectAggregate(module: AbstractModule) {
            resourceEventHandlingCount = 0
            Guice.createInjector(module).getInstance(CqrsContext::class.java).run {
                val id = UUID.randomUUID()
                sendCommand(TestCommand(id, "TEST1"))
                val id2 = UUID.randomUUID()
                sendCommand(TestCommand(id2, "TEST2"))
                assertThat(getTestAggregateData(id), contains("TEST1"))
                assertThat(getTestAggregateData(id2), contains("TEST2"))
                sendCommand(TestCommand(id, "TEST3"))
                assertThat(getTestAggregateData(id), contains("TEST1", "TEST3"))
                val id3 = UUID.randomUUID()
                sendCommand(TestCommand(id3, "TEST4"))
                assertThat(getTestAggregateData(id2), contains("TEST2"))
                assertThat(getTestAggregateData(id), contains("TEST1", "TEST3"))
                assertThat(getTestAggregateData(id3), contains("TEST4"))
            }
        }

        private fun CqrsContext.sendCommand(command: Command) {
            commandBus.handle(command)
        }

        private fun CqrsContext.getTestAggregateData(id: DataId): List<String> =
            eventStore.retrieve(TestAggregate::class, id).data

    }

}
