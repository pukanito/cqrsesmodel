/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection.populator

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.CommandBus
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventBus
import com.gmail.at.pukanito.cqrses.EventSourcingHandler
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventbus.LocalEventBus
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.DelegatingEventStore
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateMessageReplayer
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.NoMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.InMemoryEventStorage
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.projection.AggregatetypeIdProjection
import com.gmail.at.pukanito.cqrses.util.MessageQueue
import com.gmail.at.pukanito.cqrses.util.messagequeue.InMemoryMessageQueue
import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Provider
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.TypeLiteral
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.FlowPreview
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.util.UUID
import java.util.function.Consumer
import javax.inject.Inject

class AggregatetypeIdPopulatorTest {

    class TestEvent(id: DataId): Event(id)

    class TestAggregate(id: DataId): Aggregate(id) {
        @EventSourcingHandler
        @Suppress("unused", "UNUSED_PARAMETER")
        fun handleSourcingEventSupertype(event: Event) {}
    }

    private fun inMemoryModule(configuration: CqrsConfiguration, initializeEvents: Consumer<EventStore>) =
        object : AbstractModule() {
            override fun configure() {
                val messageQueueEventBaseType = object : TypeLiteral<MessageQueue<Event>>() {}
                val messageQueueEventImplementationType = object : TypeLiteral<InMemoryMessageQueue<Event>>() {}
                bind(CqrsConfiguration::class.java).toInstance(configuration)
                bind(CqrsContext::class.java).`in`(Singleton::class.java)
                bind(CommandBus::class.java).toInstance(mock())
                bind(EventBus::class.java).to(LocalEventBus::class.java).`in`(Singleton::class.java)
                bind(messageQueueEventBaseType).to(messageQueueEventImplementationType).`in`(Singleton::class.java)
                bind(object: TypeLiteral<Function0<Position<*>?>>() {}).toInstance { null }
                bind(EventStore::class.java).to(DelegatingEventStore::class.java).`in`(Singleton::class.java)
                bind(EventStorage::class.java).to(InMemoryEventStorage::class.java).`in`(Singleton::class.java)
                bind(AggregateRebuilder::class.java).to(NoMemoryAggregateRebuilder::class.java).`in`(Singleton::class.java)
                bind(AggregateMessageReplayer::class.java).`in`(Singleton::class.java)
                bind(AggregatetypeIdProjection::class.java).toProvider(
                    TestAggregatetypeIdProjectionProvider::class.java).asEagerSingleton()
                bind(AggregatetypeIdRepository::class.java).toInstance(mock())
                bind(object : TypeLiteral<Consumer<EventStore>>() {}).toInstance(initializeEvents)
            }
            @Provides @Singleton
            fun provideAggregates(): Set<AggregateClass> = setOf(TestAggregate::class)
        }

    // @Provides @Singleton cannot be an eager singleton.
    private class TestAggregatetypeIdProjectionProvider @Inject constructor(
        val context: CqrsContext,
        val repository: AggregatetypeIdRepository,
        val initializeEvents: Consumer<EventStore>): Provider<AggregatetypeIdProjection> {
        override fun get(): AggregatetypeIdProjection {
            initializeEvents.accept(context.eventStore)
            return AggregatetypeIdPopulator(context, repository)
        }
    }

    @FlowPreview
    @Test
    fun testProjectionAppliesSupertypeEventsAndDiscardsDuplicates(testInfo: TestInfo) {
        val id1 = UUID.randomUUID()
        val context = Guice
            .createInjector(inMemoryModule(testConfig(testInfo), Consumer {
                it.accept(TestEvent(id1))
                it.accept(TestEvent(id1)) }))
            .getInstance(CqrsContext::class.java)
        val projection = context.getInstance(AggregatetypeIdProjection::class.java)
        projection.update(null)
        val repository = context.getInstance(AggregatetypeIdRepository::class.java)
        verify(repository, times(1)).save(any(), any())
    }

    @FlowPreview
    @Test
    fun testProjectionAppliesUniqueExistingAndNewSourcedEvents(testInfo: TestInfo) {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val context = Guice
            .createInjector(inMemoryModule(testConfig(testInfo), Consumer {
                it.accept(TestEvent(id1))
                it.accept(TestEvent(id1))
                it.accept(TestEvent(id2)) }))
            .getInstance(CqrsContext::class.java)
        val projection = context.getInstance(AggregatetypeIdProjection::class.java)
        projection.update(null)
        val repository = context.getInstance(AggregatetypeIdRepository::class.java)
        verify(repository, times(2)).save(any(), any())
        val position = context.eventStore.getPosition()
        context.eventBus.publish(TestEvent(id2))
        context.eventBus.publish(TestEvent(UUID.randomUUID()))
        verify(repository, times(3)).save(any(), any())
        context.eventStore.accept(TestEvent(UUID.randomUUID()))
        projection.update(position)
        verify(repository, times(4)).save(any(), any())
    }

}
