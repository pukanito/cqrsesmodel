/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.query.database

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregatetypeIdRepository.AggregatetypeAndIdTable
import com.zaxxer.hikari.HikariDataSource
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.insertIgnore
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.util.UUID

class DatabaseAggregatetypeIdQueryTest {

    class TestAggregate(id: DataId): Aggregate(id)

    private lateinit var database: Database

    @BeforeEach
    fun createTables(testInfo: TestInfo) {
        database = testConfig(testInfo).let { config ->
            Database.connect(HikariDataSource().apply {
                jdbcUrl = config.database.jdbcUrl
                driverClassName = config.database.jdbcDriverClassName
                minimumIdle = config.database.maximumIdle
                maximumPoolSize = config.database.maximumPoolSize })}
        transaction(database) { SchemaUtils.create(AggregatetypeAndIdTable) }
        transaction(database) { AggregatetypeAndIdTable.deleteAll() }
        accept(UUID.randomUUID(), TestAggregate::class)
        accept(UUID.randomUUID(), TestAggregate::class)
        accept(UUID.randomUUID(), TestAggregate::class)
    }

    private fun accept(uuid: DataId, kClass: AggregateClass) {
        transaction(database) {
            AggregatetypeAndIdTable.insertIgnore {
                it[id] = uuid
                it[type] = kClass.java.typeName
            }
        }
    }

    @Test
    fun testApplyConsumesAllRecords() {
        val query = DatabaseAggregatetypeIdQuery(database)
        var count = 0
        query.apply { _, _ -> count++ }
        assertThat(count, equalTo(3))
    }

}
