/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage

import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import java.util.UUID

@ExperimentalCoroutinesApi
class InMemoryEventStorageTest {

    @Test
    fun testStoredEventsAreReplayable() = runBlockingTest {
        val eventStorage: EventStorage = InMemoryEventStorage(mock())
        val event: Event = object : Event(UUID.randomUUID()) {}
        eventStorage.accept(event)
        assertThat(eventStorage.get().count(), equalTo(1))
        assertThat(eventStorage.get().first().event, equalTo(event))
    }

    @Test
    fun testStoredEventsCanBeRemoved() = runBlockingTest {
        val eventStorage: EventStorage = InMemoryEventStorage(mock())
        val event1: Event = object : Event(UUID.randomUUID()) {}
        val event2: Event = object : Event(UUID.randomUUID()) {}
        eventStorage.accept(event1)
        eventStorage.accept(event2)
        assertThat(eventStorage.get().count(), equalTo(2))
        eventStorage.archive { e -> e.id == event1.id }
        assertThat(eventStorage.get().count(), equalTo(1))
    }

    @Test
    fun testGetReturnsLazySequence() = runBlockingTest {
        val eventStorage: EventStorage = InMemoryEventStorage(mock())
        val event1: Event = object : Event(UUID.randomUUID()) {}
        val event2: Event = object : Event(UUID.randomUUID()) {}
        eventStorage.accept(event1)
        eventStorage.accept(event2)
        val flow = eventStorage.get()
        assertThat(flow.count(), equalTo(2))
        eventStorage.archive { e -> e.id == event1.id }
        assertThat(flow.count(), equalTo(1))
        assertThat(flow.first().id, equalTo(event2.id))
    }

    @Test
    fun testPositionedGetReturnsEventsFromPosition() = runBlockingTest {
        val eventStorage: EventStorage = InMemoryEventStorage(mock())
        val event1: Event = object : Event(UUID.randomUUID()) {}
        val event2: Event = object : Event(UUID.randomUUID()) {}
        eventStorage.accept(event1)
        val position = eventStorage.getPosition()
        eventStorage.accept(event2)
        val sequence = eventStorage.get(position)
        assertThat(sequence.count(), equalTo(1))
        assertThat(sequence.first().id, equalTo(event2.id))
    }

}
