/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position.Companion.START_POSITION
import com.gmail.at.pukanito.cqrses.Position.StartPosition
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.InMemoryEventStorage
import com.gmail.at.pukanito.testutil.TestPosition
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.IsNot
import org.hamcrest.core.IsNull.notNullValue
import org.hamcrest.core.IsSame
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.util.UUID

@ExperimentalCoroutinesApi
class NoMemoryAggregateRebuilderTest {

    class TestAggregate(id: DataId) : Aggregate(id)

    @Test
    fun testReplayerIsCalledWithCorrectParameters() = runBlockingTest {
        val id = UUID.randomUUID()
        val replayer = mock<AggregateMessageReplayer>()
        whenever(replayer.replay(any(), any())) doAnswer { it.getArgument<Deferred<Aggregate>>(0).getCompleted() }
        val aggregateRebuilder: AggregateRebuilder = NoMemoryAggregateRebuilder(replayer, mock())
        val aggregate = aggregateRebuilder.get(InMemoryEventStorage(mock()), TestAggregate::class, id)
        assertThat(aggregate, notNullValue())
        val deferredAggregateCaptor = argumentCaptor<Deferred<Aggregate>>()
        verify(replayer).replay(deferredAggregateCaptor.capture(), any())
        assertThat(deferredAggregateCaptor.firstValue.getCompleted(), equalTo(aggregate))
    }

    @Test
    fun testAggregatesAreNotCached() = runBlockingTest {
        val id = UUID.randomUUID()
        val testAggregate = TestAggregate(id)
        val testPosition = TestPosition(1)
        testAggregate.version = testPosition
        val replayer = mock<AggregateMessageReplayer>()
        whenever(replayer.replay(any(), any()))
            .doAnswer { testAggregate }
            .doAnswer { it.getArgument<Deferred<Aggregate>>(0).getCompleted() }
        val aggregateRebuilder: AggregateRebuilder = NoMemoryAggregateRebuilder(replayer, mock())
        val inMemoryEventStorage = InMemoryEventStorage(mock())
        // testAggregate, has sequenceId.
        val aggregate1 = aggregateRebuilder.get(inMemoryEventStorage, TestAggregate::class, id) as TestAggregate
        assertThat(aggregate1, notNullValue())
        assertThat(aggregate1.version as TestPosition, equalTo(testPosition))
        // it.getArgument(0): a new created Aggregate, has minimal sequenceId.
        val aggregate2 = aggregateRebuilder.get(inMemoryEventStorage, TestAggregate::class, id) as TestAggregate
        assertThat(aggregate2, notNullValue())
        assertThat(aggregate2.version as StartPosition, equalTo(START_POSITION))
    }

    @Test
    fun testNoMemoryAggregateRebuilderReturnsDifferentInstances() = runBlockingTest {
        val id = UUID.randomUUID()
        val testAggregate = TestAggregate(id)
        val testPosition = TestPosition(1)
        testAggregate.version = testPosition
        val replayer = mock<AggregateMessageReplayer>()
        whenever(replayer.replay(any(), any())).doAnswer { it.getArgument<Deferred<Aggregate>>(0).getCompleted() }
        val aggregateRebuilder: AggregateRebuilder = NoMemoryAggregateRebuilder(replayer, mock())
        val inMemoryEventStorage = InMemoryEventStorage(mock())
        val aggregate1 = aggregateRebuilder.get(inMemoryEventStorage, TestAggregate::class, id) as TestAggregate
        val aggregate1a = aggregateRebuilder.get(inMemoryEventStorage, TestAggregate::class, id) as TestAggregate
        assertThat(aggregate1, IsNot.not(IsSame.sameInstance(aggregate1a)))
    }

    @Test
    fun testGetOnlyAppliesAssociatedEvents() = runBlockingTest {
        val id = UUID.randomUUID()
        val storage = mock<EventStorage>()
        val event1 = PositionedEvent(object : Event(id) {}, mock())
        val event2 = PositionedEvent(object : Event(UUID.randomUUID()) {}, mock())
        val event3 = PositionedEvent(object : Event(id) {}, mock())
        val event4 = PositionedEvent(object : Event(UUID.randomUUID()) {}, mock())
        whenever(storage.get()) doReturn flowOf(event1, event2, event3, event4)
        val replayer = mock<AggregateMessageReplayer>()
        val testAggregate = TestAggregate(id)
        whenever(replayer.replay(any(), any())) doReturn testAggregate
        val aggregateRebuilder: AggregateRebuilder = NoMemoryAggregateRebuilder(replayer, mock())
        aggregateRebuilder.get(storage, TestAggregate::class, id) as TestAggregate
        val captor = argumentCaptor<Flow<PositionedEvent>>()
        Mockito.verify(replayer).replay(any(), captor.capture())
        val sequence = captor.firstValue
        assertThat(sequence.count(), equalTo(2))
        assertThat(sequence.first(), IsSame.sameInstance(event1))
        assertThat(sequence.drop(1).first(), IsSame.sameInstance(event3))
    }

}
