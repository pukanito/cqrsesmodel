package com.gmail.at.pukanito.cqrses.util.singlylinkedlist

import com.gmail.at.pukanito.testutil.TestPosition
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toCollection
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsIterableContainingInOrder.contains
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@Suppress("EXPERIMENTAL_API_USAGE")
class SinglyLinkedListFlowTest {

    @Test
    fun testConsumingUnknownPositionTypeThrowsException() {
        SinglyLinkedList<Int>().run {
            val position = TestPosition(1)
            val flow = asFlow(position)
            assertThrows<IllegalStateException> { runBlocking { flow.count() } } }
    }

    @Test
    fun testSinglyLinkedListIsInitiallyEmpty() = runBlockingTest {
        SinglyLinkedList<Int>().run {
            assertThat(asFlow().count(), equalTo(0)) }
    }

    @Test
    fun testSinglyLinkedListFlowCanBeCollectedManyTimes() = runBlockingTest {
        SinglyLinkedList<Int>().run {
            val flow = asFlow().map { it.first }
            add(1, 2)
            assertThat(flow.toCollection(MutableList(0) { 0 }), contains(1, 2))
            assertThat(flow.toCollection(MutableList(0) { 0 }), contains(1, 2)) }
    }

    @Test
    fun testSinglyLinkedListFlowIsConsumedLazy() = runBlockingTest {
        SinglyLinkedList<Int>().run {
            val flow0 = asFlow()
            add(1)
            val flow1 = asFlow()
            add(2)
            val flow2 = asFlow()
            removeWhile { true }
            val flow3 = asFlow()
            assertThat(flow0.count(), equalTo(0))
            assertThat(flow1.count(), equalTo(0))
            assertThat(flow2.count(), equalTo(0))
            assertThat(flow3.count(), equalTo(0)) }
    }

    @Test
    fun testSinglyLinkedListPositionedFlowIsConsumedLazy() = runBlockingTest {
        SinglyLinkedList<Int>().run {
            val flow0 = asFlow(getPosition())
            add(1)
            val flow1 = asFlow(getPosition())
            add(2)
            val flow2 = asFlow(getPosition())
            removeWhile { true }
            val flow3 = asFlow(getPosition())
            assertThrows<NoSuchElementException> { runBlocking { flow0.count() } }
            assertThrows<NoSuchElementException> { runBlocking { flow1.count() } }
            assertThat(flow2.count(), equalTo(0))
            assertThat(flow3.count(), equalTo(0))
            add(3)
            assertThat(flow2.count(), equalTo(1))
            assertThat(flow3.count(), equalTo(1)) }
    }

    @Test
    fun testCountZeroFlowReturnsAllItems() = runBlockingTest {
        SinglyLinkedList<Int>().run {
            val flow0 = asFlow(0)
            val flow1 = asFlow(0, getPosition())
            add(1)
            val flow2 = asFlow(0, getPosition())
            add(2)
            val flow3 = asFlow(0, getPosition())
            add(3)
            assertThat(flow0.count(), equalTo(3))
            assertThat(flow1.count(), equalTo(3))
            assertThat(flow2.count(), equalTo(2))
            assertThat(flow3.count(), equalTo(1)) }
    }

    @Test
    fun testCountedSequenceFlowReturnsRequestedItems() = runBlockingTest {
        SinglyLinkedList<Int>().run {
            val flow0 = asFlow(2)
            val flow1 = asFlow(2, getPosition())
            add(1)
            val flow2 = asFlow(2, getPosition())
            add(2)
            val flow3 = asFlow(2, getPosition())
            add(3)
            assertThat(flow0.count(), equalTo(2))
            assertThat(flow1.count(), equalTo(2))
            assertThat(flow2.count(), equalTo(2))
            assertThat(flow3.count(), equalTo(1)) }
    }

    @Test
    fun testCountPositionedSequenceReturnsPositionOfNextItem() = runBlockingTest {
        SinglyLinkedList<Int>().run {
            val position = getPosition()
            add(1, 2, 3)
            assertThat(asFlow(10, position).count(), equalTo(3))
            val position1 = asFlow(10, position).first().second
            assertThat(asFlow(10, position1).count(), equalTo(2))
            val position2 = asFlow(10, position1).first().second
            assertThat(asFlow(10, position2).count(), equalTo(1))
            assertThat(asFlow(1, position2).first().first, equalTo(3))
            val position3 = asFlow(10, position2).first().second
            assertThat(asFlow(10, position3).count(), equalTo(0)) }
    }

}
