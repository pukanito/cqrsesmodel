/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder
import org.hamcrest.collection.IsIterableContainingInOrder.contains
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.IsNull.notNullValue
import org.hamcrest.core.IsNull.nullValue
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.NoSuchElementException
import kotlin.reflect.KParameter
import kotlin.reflect.full.companionObjectInstance
import kotlin.reflect.full.createType
import kotlin.reflect.full.isSubtypeOf


class AnnotatedMethodsMapperTest {

    @Target(AnnotationTarget.FUNCTION)
    annotation class TestAnnotation1

    @Target(AnnotationTarget.FUNCTION)
    annotation class TestAnnotation2

    @Target(AnnotationTarget.FUNCTION)
    annotation class TestAnnotation3

    open class Parameter

    class Parameter1 : Parameter()

    class Parameter2 : Parameter()

    @Suppress("unused", "UNUSED_PARAMETER")
    class TestClass1 {
        companion object {
            @TestAnnotation1 fun fun1(p: Parameter1) {}
        }
        @TestAnnotation2 fun fun2(p: Parameter2) {}
        @TestAnnotation3 fun fun3() {}
    }

    private fun KParameter.isParameter() = type.isSubtypeOf(Parameter::class.createType())
    private fun KParameter.isParameter1() = type.isSubtypeOf(Parameter1::class.createType())

    @Test
    fun testAnnotatedCompanionObjectMethodIsFound() {
        val annotatedMethods = AnnotatedMethodsMapper(setOf(TestClass1::class), { isParameter() }, TestAnnotation1::class)
        assertThat(annotatedMethods.kTypeMap.size, equalTo(1))
        assertThat(annotatedMethods[Parameter1::class], notNullValue())
        assertThat(annotatedMethods[Parameter1::class]!![0].instance, equalTo(TestClass1::class.companionObjectInstance))
        assertThat(annotatedMethods[Parameter1::class]!![0].method.name, equalTo("fun1"))
    }

    @Test
    fun testAnnotatedInstanceMethodIsFound() {
        val annotatedMethods = AnnotatedMethodsMapper(setOf(TestClass1::class), { isParameter() }, TestAnnotation2::class)
        assertThat(annotatedMethods.kTypeMap.size, equalTo(1))
        assertThat(annotatedMethods[Parameter2::class], notNullValue())
        assertThat(annotatedMethods[Parameter2::class]!![0].instance, nullValue())
        assertThat(annotatedMethods[Parameter2::class]!![0].method.name, equalTo("fun2"))
    }

    @Suppress("unused", "UNUSED_PARAMETER")
    class TestClass2 {
        companion object {
            @TestAnnotation2 fun fun4(p: Parameter1, p2: Parameter2) {}
            fun fun5(p: Parameter1) {}
        }
        @TestAnnotation1 fun fun6(p: Parameter2) {}
        fun fun7(p: Parameter2) {}
    }

    @Test
    fun testAnnotatedMethodsAreFoundWithCorrectKey() {
        val annotatedMethods = AnnotatedMethodsMapper(setOf(TestClass1::class, TestClass2::class),
            { isParameter() }, TestAnnotation1::class, TestAnnotation2::class)
        assertThat(annotatedMethods.kTypeMap.size, equalTo(2))
        assertThat(annotatedMethods[Parameter1::class], notNullValue())
        assertThat(annotatedMethods[Parameter1::class]!!.map { it.method.name }, containsInAnyOrder("fun1", "fun4"))
        assertThat(annotatedMethods[Parameter2::class], notNullValue())
        assertThat(annotatedMethods[Parameter2::class]!!.map { it.method.name }, containsInAnyOrder("fun2", "fun6"))
    }

    @Test
    fun testAnnotatedMethodsAreFoundWithoutCorrectKeyThrowsNoSuchElementException() {
        assertThrows<NoSuchElementException> { AnnotatedMethodsMapper(setOf(TestClass2::class),
            { isParameter1() }, TestAnnotation1::class, TestAnnotation2::class) }
    }

    @Test
    fun testAnnotatedMethodWithoutKeyParameterThrowsNoSuchElementException() {
        assertThrows(NoSuchElementException::class.java) {
            AnnotatedMethodsMapper(setOf(TestClass1::class), { isParameter() }, TestAnnotation3::class)
        }
    }

    @Suppress("unused", "UNUSED_PARAMETER")
    class TestClass3 {
        @TestAnnotation1 fun fun8(p: Parameter, p2: Parameter2) {}
    }

    @Test
    fun testTypeOfFirstMatchingParameterIsMapped() {
        val annotatedMethods = AnnotatedMethodsMapper(setOf(TestClass3::class),
            { isParameter() }, TestAnnotation1::class)
        assertThat(annotatedMethods.kTypeMap.size, equalTo(1))
        assertThat(annotatedMethods[Parameter::class], notNullValue())
        assertThat(annotatedMethods[Parameter::class]!!.map { it.method.name }, contains("fun8"))
    }

}
