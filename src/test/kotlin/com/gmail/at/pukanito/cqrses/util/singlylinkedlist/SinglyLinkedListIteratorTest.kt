package com.gmail.at.pukanito.cqrses.util.singlylinkedlist

import com.gmail.at.pukanito.testutil.TestPosition
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class SinglyLinkedListIteratorTest {

    @Test
    fun testConsumingUnknownPositionTypeThrowsException() {
        SinglyLinkedList<Int>().run {
            val position = TestPosition(1)
            val iterator = iterator(position)
            assertThrows<IllegalStateException> { iterator.hasNext() }
            assertThrows<IllegalStateException> { iterator.next() } }
    }

    @Test
    fun testSinglyLinkedListIteratorIsInitiallyEmpty() {
        SinglyLinkedList<Int>().run {
            assertThat(iterator().hasNext(), equalTo(false))
            val position = getPosition()
            assertThat(iterator(position).hasNext(), equalTo(false)) }
    }

    @Test
    fun testEmptyListIteratorIteratesAddedItems() {
        SinglyLinkedList<Int>().run {
            val iterator = iterator()
            add(1)
            assertThat(iterator.hasNext(), equalTo(true))
            assertThat(iterator.next().first, equalTo(1)) }
    }

    @Test
    fun testEmptyListPositionedIteratorIteratesAddedItems() {
        SinglyLinkedList<Int>().run {
            val iterator = iterator(getPosition())
            add(1)
            assertThat(iterator.hasNext(), equalTo(true))
            assertThat(iterator.next().first, equalTo(1)) }
    }

    @Test
    fun testEmptyListIteratorIteratingAfterAddAndRemoveThrowsException() {
        SinglyLinkedList<Int>().run {
            val iterator = iterator()
            add(1)
            removeWhile { true }
            assertThrows<NoSuchElementException> { iterator.hasNext() }
            assertThrows<NoSuchElementException> { iterator.next() } }
    }

    @Test
    fun testEmptyListPositionedIteratorIteratingAfterAddAndRemoveThrowsException() {
        SinglyLinkedList<Int>().run {
            val iterator = iterator(getPosition())
            add(1)
            removeWhile { true }
            assertThrows<NoSuchElementException> { iterator.hasNext() }
            assertThrows<NoSuchElementException> { iterator.next() } }
    }

    @Test
    fun testFilledListPositionedIteratorIteratingAfterRemovingOldItemsIterates() {
        SinglyLinkedList<Int>().run {
            add(1)
            val iterator = iterator(getPosition())
            assertThat(iterator.hasNext(), equalTo(false))
            assertThrows<NoSuchElementException> { iterator.next() }
            add(2)
            removeWhile { it <= 1 }
            assertThat(iterator.hasNext(), equalTo(true))
            assertThat(iterator.next().first, equalTo(2)) }
    }

    @Test
    fun testFilledListPositionedIteratorIteratingAfterRemovingAllItemsThrowsException() {
        SinglyLinkedList<Int>().run {
            add(1)
            val iterator = iterator(getPosition())
            add(2)
            removeWhile { true }
            assertThrows<NoSuchElementException> { iterator.hasNext() }
            assertThrows<NoSuchElementException> { iterator.next() } }
    }

    @Test
    fun testCountedIteratorIteratesCountItemsMaximally() {
        SinglyLinkedList<Int>().run {
            val iterator = iterator(1)
            add(1, 2)
            assertThat(iterator.hasNext(), equalTo(true))
            iterator.next()
            assertThat(iterator.hasNext(), equalTo(false))
            assertThrows<NoSuchElementException> { iterator.next() }
            val fullIterator = iterator(5)
            repeat(2) {
                assertThat(fullIterator.hasNext(), equalTo(true))
                fullIterator.next()
            }
            assertThat(fullIterator.hasNext(), equalTo(false))
            assertThrows<NoSuchElementException> { fullIterator.next() }
        }
    }

    @Test
    fun testCountedPositionedIteratorIteratesCountItemsMaximally() {
        SinglyLinkedList<Int>().run {
            add(1, 2)
            val position = getPosition()
            val iterator = iterator(2, position)
            add(3, 4, 5)
            repeat(2) {
                assertThat(iterator.hasNext(), equalTo(true))
                iterator.next() }
            assertThat(iterator.hasNext(), equalTo(false))
            assertThrows<NoSuchElementException> { iterator.next() }
            val fullPositionedIterator = iterator(10, position)
            repeat(3) {
                assertThat(fullPositionedIterator.hasNext(), equalTo(true))
                fullPositionedIterator.next() }
            assertThat(fullPositionedIterator.hasNext(), equalTo(false))
            assertThrows<NoSuchElementException> { fullPositionedIterator.next() }
            val fullIterator = iterator()
            repeat(5) {
                assertThat(fullIterator.hasNext(), equalTo(true))
                fullIterator.next() }
            assertThat(fullIterator.hasNext(), equalTo(false))
            assertThrows<NoSuchElementException> { fullIterator.next() }
        }
    }

    @Test
    fun testCountPositionedIteratorReturnsPositionOfNextItem() {
        SinglyLinkedList<Int>().run {
            val position1 = getPosition()
            add(1, 2, 3)
            val position2 = iterator(1, position1).next().second
            val position3 = iterator(1, position2).next().second
            assertThat(iterator(1, position3).next().first, equalTo(3)) }
    }

}
