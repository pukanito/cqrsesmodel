/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util.messagequeue

import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Message
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorage
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.InMemoryEventStorage
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileWriter
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.FiledEventReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.FiledEventReaderV1
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFile
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFileReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFileWriter
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.deleteEventFile
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.deleteRotatingEventFile
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.cqrses.util.messagequeue.PersistentMessageQueue.MessagePolledFromQueue
import com.nhaarman.mockitokotlin2.mock
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.IsInstanceOf.instanceOf
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.util.UUID

class PersistentMessageQueueTest {

    class TestMessage(id: DataId, val order: Int): Message(id)

    @Test
    fun testMessagesArriveInOrder() {
        val eventStorage = InMemoryEventStorage(mock())
        val queue = PersistentMessageQueue<TestMessage>(eventStorage)
        val range = (1..10)
        range.forEach { queue.add(TestMessage(UUID.randomUUID(), it)) }
        range.forEach { assertThat(queue.poll()!!.order, equalTo(it)) }
    }

    @Test
    fun testMessagesArePersisted() {
        val eventStorage = InMemoryEventStorage(mock())
        val queue1 = PersistentMessageQueue<TestMessage>(eventStorage)
        val range = (1..10)
        range.forEach { queue1.add(TestMessage(UUID.randomUUID(), it)) }
        val queue2 = PersistentMessageQueue<TestMessage>(eventStorage)
        range.forEach { assertThat(queue2.poll()!!.order, equalTo(it)) }
    }

    @Test
    fun testMessagesPolledButNotFinishedAreRestoredToQueue() {
        val eventStorage = InMemoryEventStorage(mock())
        val queue1 = PersistentMessageQueue<TestMessage>(eventStorage)
        val range1 = (1..5)
        val range2 = (6..10)
        (range1 + range2).forEach { queue1.add(TestMessage(UUID.randomUUID(), it)) }
        range1.forEach { assertThat(queue1.poll()!!.order, equalTo(it)) }
        val queue2 = PersistentMessageQueue<TestMessage>(eventStorage)
        (range1 + range2).forEach { assertThat(queue2.poll()!!.order, equalTo(it)) }
    }

    @Test
    fun testMessagesFinishedAreNotRestoredToQueueInMemoryEventStorage() {
        testMessagesFinishedAreNotRestoredToQueue(InMemoryEventStorage(mock()))
    }

    @Test
    fun testFiledEventReaderV1Serialization() {
        class TestMessage(id: DataId): Message(id)
        val message = TestMessage(UUID.randomUUID())
        val messagePolledEvent = MessagePolledFromQueue(message)
        val serialized = FiledEventReaderV1.mapToStringV1(FiledEventReader.FiledEvent(messagePolledEvent))
        val deserialized = FiledEventReaderV1.mapToFiledEventV1(serialized)
        assertThat(deserialized.event, instanceOf(MessagePolledFromQueue::class.java))
        assertThat(deserialized.event.id, equalTo(message.id))
    }

    @Test
    fun testMessagesFinishedAreNotRestoredToQueueFileEventStorage(testInfo: TestInfo) {
        val config = testConfig(testInfo)
        deleteEventFile(config.eventFileStorage.path)
        testMessagesFinishedAreNotRestoredToQueue(
            FileEventStorage(
                EventFileReader(config.commandQueueFileStorage.path),
                EventFileWriter(config.commandQueueFileStorage.path),
                mock()))
        testMessagesFinishedAreNotRestoredToQueue(
            FileEventStorage(
                EventFileReader(config.commandQueueFileStorage.path),
                EventFileWriter(config.commandQueueFileStorage.path),
                mock()))
    }

    @Test
    fun testMessagesFinishedAreNotRestoredToQueueRotatingFileEventStorage(testInfo: TestInfo) {
        val config = testConfig(testInfo)
        val eventFilePath = config.commandQueueFileStorage.rotation.directory
        deleteRotatingEventFile(eventFilePath)
        testMessagesFinishedAreNotRestoredToQueue(
            FileEventStorage(
                RotatingEventFileReader(RotatingEventFile(config.commandQueueFileStorage.rotation.directory,
                        config.commandQueueFileStorage.rotation.size)),
                RotatingEventFileWriter(RotatingEventFile(config.commandQueueFileStorage.rotation.directory,
                        config.commandQueueFileStorage.rotation.size)), mock()))
        testMessagesFinishedAreNotRestoredToQueue(
            FileEventStorage(
                RotatingEventFileReader(RotatingEventFile(config.commandQueueFileStorage.rotation.directory,
                        config.commandQueueFileStorage.rotation.size)),
                RotatingEventFileWriter(RotatingEventFile(config.commandQueueFileStorage.rotation.directory,
                        config.commandQueueFileStorage.rotation.size)), mock()))
    }

    private fun testMessagesFinishedAreNotRestoredToQueue(eventStorage: EventStorage) {
        val queue1 = PersistentMessageQueue<TestMessage>(eventStorage)
        val range1 = (1..5)
        val range2 = (6..10)
        (range1 + range2).forEach { queue1.add(TestMessage(UUID.randomUUID(), it)) }
        range1.forEach {
            val message = queue1.poll()!!
            assertThat(message.order, equalTo(it))
            queue1.finish(message)
        }
        val queue2 = PersistentMessageQueue<TestMessage>(eventStorage)
        range2.forEach {
            val message = queue2.poll()!!
            assertThat(message.order, equalTo(it))
            queue2.finish(message)
        }
    }

}
