/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventbus

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventHandler
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.infrastructure.MessageStateContext
import com.gmail.at.pukanito.cqrses.util.messagequeue.InMemoryMessageQueue
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.hamcrest.core.StringContains.containsString
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.never
import java.io.IOException
import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger
import kotlin.reflect.KClass

class LocalEventBusTest {

    class TestEvent(id: DataId) : Event(id) {
        val check = AtomicInteger(0)
    }

    @Test
    fun testSendEventToAggregateWithInstanceEventHandlerExecutesThatHandler() {
        @Suppress("unused")
        class TestAggregate(id: DataId): Aggregate(id) {
            @EventHandler
            fun handle(event: TestEvent) { event.check.getAndIncrement() }
        }
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate::class)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(any<KClass<TestAggregate>>(), any())) doReturn TestAggregate(uuid)
        val testEvent = TestEvent(UUID.randomUUID())
        LocalEventBus(context, InMemoryMessageQueue(), mock()).publish(testEvent)
        MatcherAssert.assertThat(testEvent.check.get(), IsEqual.equalTo(1))
        Mockito.verify(context.messageStateContext).finishState(any())
        Mockito.verify(context.messageStateContext, never()).addState(any(), any())
        Mockito.verify(context.eventStore).accept(any())
    }

    @Test
    fun testSendEventToAggregateWithoutEventHandlerIsIgnored() {
        @Suppress("unused")
        class TestAggregate(id: DataId) : Aggregate(id) {
            fun handle(event: TestEvent) { event.check.getAndIncrement() }
        }
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate::class)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(any<KClass<TestAggregate>>(), any())) doReturn TestAggregate(uuid)
        val testEvent = TestEvent(uuid)
        LocalEventBus(context, InMemoryMessageQueue(), mock()).publish(testEvent)
        MatcherAssert.assertThat(testEvent.check.get(), IsEqual.equalTo(0))
        Mockito.verify(context.messageStateContext).finishState(any())
        Mockito.verify(context.messageStateContext, never()).addState(any(), any())
        Mockito.verify(context.eventStore).accept(any())
    }

    @Test
    fun testExceptionInsideAggregateInstanceMessageHandlerAddsExceptionToMessageStateAndStoresEvent() {
        @Suppress("unused")
        class TestAggregate(id: DataId): Aggregate(id) {
            @Suppress("UNUSED_PARAMETER")
            @EventHandler
            fun handle(event: TestEvent) { throw IOException("IOFOUT") }
        }
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate::class)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(any<KClass<TestAggregate>>(), any())) doReturn TestAggregate(uuid)
        val testEvent = TestEvent(uuid)
        LocalEventBus(context, InMemoryMessageQueue(), mock()).publish(testEvent)
        Mockito.verify(context.messageStateContext).finishState(any())
        val captor = argumentCaptor<Any>()
        Mockito.verify(context.messageStateContext).addState(any(), captor.capture())
        val exception = captor.firstValue as IOException
        MatcherAssert.assertThat(exception.message, containsString("IOFOUT"))
        Mockito.verify(context.eventStore).accept(any())
    }

    private fun mockContext(): CqrsContext {
        val context = mock<CqrsContext>()
        val messageStateContext = mock<MessageStateContext>()
        whenever(context.messageStateContext) doReturn messageStateContext
        val eventStore = mock<EventStore>()
        whenever(context.eventStore) doReturn eventStore
        return context
    }

}
