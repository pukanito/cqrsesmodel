/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util

import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Message
import com.gmail.at.pukanito.cqrses.util.messagequeue.InMemoryMessageQueue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsIterableContainingInOrder
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.IOException
import java.util.UUID
import java.util.function.Consumer

class MessageRegulatorTest {

    class RecursiveMessage(id: DataId,
                           val regulator: MessageRegulator<RecursiveMessage>,
                           val count: Int): Message(id)

    @Test
    fun testRecursiveMessagesAreCompletelyHandledInOrder() {
        val result = mutableListOf<Int>()
        val messageRegulator = object : MessageRegulator<RecursiveMessage>(Consumer {
            if (it.count < 3) it.regulator.accept(RecursiveMessage(it.id, it.regulator,it.count + 1))
            result.add(it.count)
        }, InMemoryMessageQueue()) {}
        val uuid = UUID.randomUUID()
        messageRegulator.accept(RecursiveMessage(uuid, messageRegulator,0))
        assertThat(result, IsIterableContainingInOrder.contains(0, 1, 2, 3))
    }

    @Test
    fun testExceptionWhileMessageHandlingDoesNotLockTheCommandBus() {
        val messageRegulator = object : MessageRegulator<RecursiveMessage>(Consumer {
            if (it.count == 0) throw IOException("IOFOUT")
        }, InMemoryMessageQueue()) {}
        val uuid = UUID.randomUUID()
        assertThrows<IOException> { messageRegulator.accept(RecursiveMessage(uuid, messageRegulator, 0)) }
        messageRegulator.accept(RecursiveMessage(uuid, messageRegulator, 1))
    }

}
