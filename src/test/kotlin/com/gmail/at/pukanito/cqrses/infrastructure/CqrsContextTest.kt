/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.infrastructure

import com.gmail.at.pukanito.cqrses.CommandBus
import com.gmail.at.pukanito.cqrses.EventBus
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.testfixtures.MocksModule.Companion.mocksModule
import com.gmail.at.pukanito.cqrses.testfixtures.TestAggregate
import com.google.inject.Guice
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argThat
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.IsNull.notNullValue
import org.hamcrest.core.IsSame.sameInstance
import org.junit.jupiter.api.Test
import java.util.UUID
import kotlin.reflect.KClass

class CqrsContextTest {

    @Test
    fun testContextConfigurationIsCorrect() {
        val context = Guice.createInjector(mocksModule).getInstance(CqrsContext::class.java)
        assertThat(context, notNullValue())
        assertThat(context.commandBus::class.java, sameInstance(mocksModule.mockCommandBus::class.java))
        assertThat(context.eventBus::class.java, sameInstance(mocksModule.mockEventBus::class.java))
        assertThat(context.eventStore::class.java, sameInstance(mocksModule.mockEventStore::class.java))
        assertThat(context.aggregateClasses, notNullValue())
    }

    @Test
    fun testRetrieveTheAggregate() {
        whenever(mocksModule.mockEventStore.retrieve(argThat<KClass<TestAggregate>> { this == TestAggregate::class }, any()))
            .doAnswer { TestAggregate(it.getArgument(1)) }
        val id = UUID.randomUUID()
        val context = Guice.createInjector(mocksModule).getInstance(CqrsContext::class.java)
        val aggregate = context.eventStore.retrieve(TestAggregate::class, id)
        assertThat(aggregate.hasId(id), equalTo(true))
    }

    @Test
    fun testGetInstance() {
        val context = Guice.createInjector(mocksModule).getInstance(CqrsContext::class.java)
        assertThat(context.getInstance(CommandBus::class.java), sameInstance(mocksModule.mockCommandBus))
        assertThat(context.getInstance(EventBus::class.java), sameInstance(mocksModule.mockEventBus))
        assertThat(context.getInstance(EventStore::class.java), sameInstance(mocksModule.mockEventStore))
    }

}
