/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses

import com.fasterxml.jackson.module.kotlin.readValue
import com.gmail.at.pukanito.cqrses.commandbus.LocalCommandBus
import com.gmail.at.pukanito.cqrses.eventbus.LocalEventBus
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.DelegatingEventStore
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateMessageReplayer
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateSnapshotQuery
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateSnapshotQuery.AggregateSnapshot
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.NoMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.SnapshotAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorage
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorageReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorageWriter
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFile
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFileReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFileWriter
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.deleteRotatingEventFile
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.projection.AggregateSnapshotProjection
import com.gmail.at.pukanito.cqrses.projection.AggregatetypeIdProjection
import com.gmail.at.pukanito.cqrses.projection.ProjectionDataRepository
import com.gmail.at.pukanito.cqrses.projection.database.DatabaseProjectionDataRepository
import com.gmail.at.pukanito.cqrses.projection.database.DatabaseProjectionDataRepository.ProjectionDataTable
import com.gmail.at.pukanito.cqrses.projection.populator.AggregateSnapshotPopulator
import com.gmail.at.pukanito.cqrses.projection.populator.AggregateSnapshotRepository
import com.gmail.at.pukanito.cqrses.projection.populator.AggregatetypeIdPopulator
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.AggregateSnapshotTable
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.SerialisedAggregate
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregatetypeIdRepository
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregatetypeIdRepository.AggregatetypeAndIdTable
import com.gmail.at.pukanito.cqrses.query.AggregatetypeIdQuery
import com.gmail.at.pukanito.cqrses.query.database.DatabaseAggregatetypeIdQuery
import com.gmail.at.pukanito.cqrses.util.MessageQueue
import com.gmail.at.pukanito.cqrses.util.messagequeue.PersistentMessageQueue
import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Provider
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.TypeLiteral
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.zaxxer.hikari.HikariDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsCollectionWithSize.hasSize
import org.hamcrest.collection.IsEmptyCollection.empty
import org.hamcrest.collection.IsIterableContainingInOrder.contains
import org.hamcrest.core.IsEqual.equalTo
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.util.Optional
import java.util.UUID
import java.util.function.Consumer
import java.util.logging.Logger
import javax.inject.Inject
import javax.sql.DataSource

/**
 * Test:
 * - [Model]
 * - [Context]
 * - [DelegatingEventStore]
 * - [FileEventStorage]
 * - [RotatingEventFileReader]
 * - [RotatingEventFileWriter]
 * - [SnapshotAggregateRebuilder]
 * - [LocalCommandBus]
 * - [LocalEventBus]
 * - [PersistentMessageQueue] with [RotatingEventFileReader]/[RotatingEventFileWriter]
 * - [AggregatetypeIdProjection]
 * - [AggregateSnapshotProjection]
 * - [ProjectionDataRepository]
 *
 * Test handling of events by @[EventSourcingHandler]s and @[CommandHandler]s
 */
class CqrsSnapshotTest {

    private lateinit var configuration: CqrsConfiguration

    private fun testModule(config: CqrsConfiguration, initializeEvents: Consumer<EventStore>) = object : AbstractModule() {
        override fun configure() {
            bind(CqrsConfiguration::class.java).toInstance(config)
            bind(CqrsContext::class.java).`in`(Singleton::class.java)
            bind(CommandBus::class.java).to(LocalCommandBus::class.java).`in`(Singleton::class.java)
            bind(EventBus::class.java).to(LocalEventBus::class.java).`in`(Singleton::class.java)
            bind(object: TypeLiteral<Function0<Position<*>?>>() {}).toInstance { null }
            bind(EventStore::class.java).to(DelegatingEventStore::class.java).`in`(Singleton::class.java)
            bind(EventStorage::class.java).to(FileEventStorage::class.java).`in`(Singleton::class.java)
            bind(AggregateMessageReplayer::class.java).`in`(Singleton::class.java)
            bind(object : TypeLiteral<Consumer<EventStore>>() {}).toInstance(initializeEvents)
            bind(AggregatetypeIdProjection::class.java).toProvider(InMemoryDbAggregatetypeIdProjectionProvider::class.java).asEagerSingleton()
            bind(ProjectionDataRepository::class.java).to(DatabaseProjectionDataRepository::class.java).`in`(Singleton::class.java)
            bind(AggregatetypeIdQuery::class.java).to(DatabaseAggregatetypeIdQuery::class.java).`in`(Singleton::class.java)
            bind(AggregateSnapshotProjection::class.java).to(AggregateSnapshotPopulator::class.java).`in`(Singleton::class.java)
            bind(AggregateSnapshotRepository::class.java).to(DatabaseAggregateSnapshotRepository::class.java).`in`(Singleton::class.java)
        }
        @Provides @Singleton
        fun provideSnapshotRepository(): AggregateSnapshotQuery = mock()
        @Provides @Singleton
        fun provideAggregates(): Set<AggregateClass> = setOf(TestAggregate1::class, TestAggregate2::class)
        @Provides @Singleton
        fun provideAggregateRebuilder(replayer: AggregateMessageReplayer,
                                      aggregateSnapshotQuery: AggregateSnapshotQuery,
                                      logger: Logger): AggregateRebuilder =
            SnapshotAggregateRebuilder(replayer, aggregateSnapshotQuery,
                NoMemoryAggregateRebuilder(replayer, logger), logger)
        @Provides @Singleton
        fun provideRotatingEventFile(config: CqrsConfiguration): RotatingEventFile =
            RotatingEventFile(config.eventFileStorage.rotation.directory, config.eventFileStorage.rotation.size)
        @Provides @Singleton
        fun provideFileEventStorageReader(rotatingEventFile: RotatingEventFile): FileEventStorageReader =
            RotatingEventFileReader(rotatingEventFile, Dispatchers.IO)
        @Provides @Singleton
        fun provideFileEventStorageWriter(rotatingEventFile: RotatingEventFile): FileEventStorageWriter =
            RotatingEventFileWriter(rotatingEventFile)
        @Provides @Singleton
        fun provideMessageQueueForCommands(config: CqrsConfiguration,
                                           logger: Logger): MessageQueue<Command> =
            RotatingEventFile(config.commandQueueFileStorage.rotation.directory,
                config.commandQueueFileStorage.rotation.size).let {
                    PersistentMessageQueue(FileEventStorage(RotatingEventFileReader(it), RotatingEventFileWriter(it), logger)) }
        @Provides @Singleton
        fun provideMessageQueueForEvents(config: CqrsConfiguration,
                                         logger: Logger): MessageQueue<Event> =
            RotatingEventFile(config.eventQueueFileStorage.rotation.directory,
                config.eventQueueFileStorage.rotation.size).let {
                    PersistentMessageQueue(FileEventStorage(RotatingEventFileReader(it), RotatingEventFileWriter(it), logger)) }
        @Provides @Singleton
        fun provideDatabase(dataSource: DataSource): Database = Database.connect(dataSource)
        @Provides @Singleton
        fun provideDatasource(config: CqrsConfiguration): DataSource = HikariDataSource().apply {
            jdbcUrl = config.database.jdbcUrl
            driverClassName = config.database.jdbcDriverClassName
            minimumIdle = config.database.maximumIdle
            maximumPoolSize = config.database.maximumPoolSize }
    }

    private class InMemoryDbAggregatetypeIdProjectionProvider @Inject constructor(
        val context: CqrsContext,
        val database: Database,
        val initializeEvents: Consumer<EventStore>): Provider<AggregatetypeIdProjection> {
        override fun get(): AggregatetypeIdProjection {
            initializeEvents.accept(context.eventStore)
            return AggregatetypeIdPopulator(context, DatabaseAggregatetypeIdRepository(database))
        }
    }

    @BeforeEach
    fun allTestsStartWithoutPreviousEventsInStorageOrQueuesAndWithEmptyDatabase(testInfo: TestInfo) {
        configuration = testConfig(testInfo)
        // Clean event and queue files.
        deleteRotatingEventFile(configuration.eventFileStorage.rotation.directory)
        deleteRotatingEventFile(configuration.commandQueueFileStorage.rotation.directory)
        deleteRotatingEventFile(configuration.eventQueueFileStorage.rotation.directory)
        // Clean database.
        val database = Database.connect(HikariDataSource().apply {
            jdbcUrl = configuration.database.jdbcUrl
            driverClassName = configuration.database.jdbcDriverClassName
            minimumIdle = configuration.database.maximumIdle
            maximumPoolSize = configuration.database.maximumPoolSize })
        transaction(database) { SchemaUtils.createMissingTablesAndColumns(
            ProjectionDataTable,
            AggregatetypeAndIdTable,
            AggregateSnapshotTable) }
        transaction(database) { ProjectionDataTable.deleteAll() }
        transaction(database) { AggregatetypeAndIdTable.deleteAll() }
        transaction(database) { AggregateSnapshotTable.deleteAll() }
    }

    class TestEvent1(id: DataId, val data: String): Event(id)
    class TestEvent2(id: DataId, val data: String): Event(id)

    class TestAggregate1(id: DataId): Aggregate(id) {
        val data: MutableList<String> = mutableListOf()
        @EventSourcingHandler @Suppress("unused", "UNUSED_PARAMETER")
        fun handleSourcingEvent(event: TestEvent1) { data += event.data}
    }

    class TestAggregate2(id: DataId): Aggregate(id) {
        val data: MutableList<String> = mutableListOf()
        @EventSourcingHandler @Suppress("unused", "UNUSED_PARAMETER")
        fun handleSourcingEvent(event: TestEvent2) { data += event.data }
    }

    @FlowPreview
    @Test
    fun test() {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        // EventStore with initial events.
        val context = Guice.createInjector(testModule(configuration, Consumer {
                it.accept(TestEvent1(id1, "1"))
                it.accept(TestEvent2(id1, "2"))
                it.accept(TestEvent1(id2, "3"))
                it.accept(TestEvent2(id1, "4"))
        })).getInstance(CqrsContext::class.java)
        // Check AggregatetypeIdProjection update updates from the given position.
        val projection = context.getInstance(AggregatetypeIdProjection::class.java)
        projection.update(context.eventStore.getPosition())
        val database = context.getInstance(Database::class.java)
        assertThat(transaction(database) { AggregatetypeAndIdTable.selectAll().count() }, equalTo(0))
        projection.update(null)
        assertThat(transaction(database) { AggregatetypeAndIdTable.selectAll().count() }, equalTo(3))
        // Create snapshots.
        assertThat(selectById(database, TestAggregate1::class, id1), empty())
        val query = context.getInstance(AggregatetypeIdQuery::class.java)
        val snapshotProjection = context.getInstance(AggregateSnapshotProjection::class.java)
        query.apply { type, id -> snapshotProjection.project(type, id) }
        // Check created snapshots.
        assertThat(selectById(database, TestAggregate1::class, id1), hasSize(1))
        val mapper = DatabaseAggregateSnapshotRepository.mapper()
        assertThat((mapper.readValue<SerialisedAggregate>(
            snapshotById(database, TestAggregate1::class, id1)).aggregate as TestAggregate1).data, contains("1"))
        assertThat((mapper.readValue<SerialisedAggregate>(
            snapshotById(database, TestAggregate2::class, id1)).aggregate as TestAggregate2).data, contains("2", "4"))
        assertThat((mapper.readValue<SerialisedAggregate>(
            snapshotById(database, TestAggregate1::class, id2)).aggregate as TestAggregate1).data, contains("3"))
        // Check retrieve without snapshot.
        val repository = context.getInstance(AggregateSnapshotQuery::class.java)
        whenever(repository.get(any(), any())) doReturn Optional.empty()
        assertThat(context.eventStore.retrieve(TestAggregate1::class, id1).data, contains("1"))
        assertThat(context.eventStore.retrieve(TestAggregate2::class, id1).data, contains("2", "4"))
        assertThat(context.eventStore.retrieve(TestAggregate1::class, id2).data, contains("3"))
        // Check retrieve with snapshot from position null: does replay events on snapshot value.
        whenever(repository.get(eq(TestAggregate1::class), any())) doReturn Optional.of(AggregateSnapshot(TestAggregate1(id1), null))
        assertThat(context.eventStore.retrieve(TestAggregate1::class, id1).data, contains("1"))
        // Check retrieve with snapshot from end of events position: does not replay events on snapshot value
        val position = context.eventStore.getPosition()
        val testAggregate1 = TestAggregate1(id1).apply { data += "X" }
        whenever(repository.get(eq(TestAggregate1::class), any())) doReturn Optional.of(AggregateSnapshot(testAggregate1, position))
        assertThat(context.eventStore.retrieve(TestAggregate1::class, id1).data, contains("X"))
    }

    private fun snapshotById(database: Database, kClass: AggregateClass, id: UUID): String =
        selectById(database, kClass, id).single()[AggregateSnapshotTable.snapshot]

    private fun selectById(database: Database, kClass: AggregateClass, id: UUID): List<ResultRow> =
        transaction(database) { AggregateSnapshotTable.run {
            select { this@run.id eq id and (type eq kClass.java.typeName) }.toList() } }

}
