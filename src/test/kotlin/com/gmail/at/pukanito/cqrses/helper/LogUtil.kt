/*
 * Copyright (C) 2018 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.helper

import com.nhaarman.mockitokotlin2.atLeast
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.mockito.ArgumentCaptor
import java.util.logging.Handler
import java.util.logging.LogRecord
import java.util.logging.Logger

/**
 * Capture java.util.logging and return a list of logged messages.
 */
fun captureLog(test: () -> Unit): List<LogRecord> {
    val mockHandler: Handler = mock()
    val rootLogger = Logger.getLogger("")
    rootLogger.addHandler(mockHandler)
    try {
        test.invoke()
        val argumentCaptor = ArgumentCaptor.forClass(LogRecord::class.java)
        verify(mockHandler, atLeast(0)).publish(argumentCaptor.capture())
        return argumentCaptor.allValues
    } finally {
        rootLogger.removeHandler(mockHandler)
    }
}
