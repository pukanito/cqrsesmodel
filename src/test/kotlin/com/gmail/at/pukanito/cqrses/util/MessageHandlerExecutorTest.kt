/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.Message
import com.gmail.at.pukanito.cqrses.helper.captureLog
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.infrastructure.MessageStateContext
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.beans.HasPropertyWithValue.hasProperty
import org.hamcrest.core.AllOf.allOf
import org.hamcrest.core.IsCollectionContaining.hasItem
import org.hamcrest.core.IsCollectionContaining.hasItems
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.StringContains.containsString
import org.junit.jupiter.api.Test
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import java.io.IOException
import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger
import java.util.function.Predicate
import java.util.logging.Level.WARNING
import java.util.logging.LogRecord
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.full.createType
import kotlin.reflect.full.isSubtypeOf

class MessageHandlerExecutorTest {

    @Target(AnnotationTarget.FUNCTION)
    annotation class TestAnnotation

    class TestMessage(id: DataId, val check: AtomicInteger): Message(id)

    fun KParameter.isTestMessage() = type.isSubtypeOf(TestMessage::class.createType())

    inner class TestMessageHandlerExecutor constructor(context: CqrsContext):
        MessageHandlerExecutor<TestMessage>(context, { isTestMessage() }, TestAnnotation::class) {
        override fun handleMessage(message: TestMessage) {
            val parameterResolver = baseParameterResolver.wrap(Predicate { p -> p.isTestMessage() }, message)
            annotatedMethods[message::class]?.forEach { it.call(parameterResolver, message) }
        }

    }

    class TestAggregate1(id: DataId): Aggregate(id) {
        @Suppress("unused")
        companion object {
            @TestAnnotation
            fun handle(message: TestMessage) { message.check.getAndIncrement() }
        }
    }

    @Test
    fun testSendMessageToAggregateWithCompanionObjectMessageHandlerExecutesThatHandler() {
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate1::class)
        val resultCheck = AtomicInteger(0)
        TestMessageHandlerExecutor(context).accept(TestMessage(UUID.randomUUID(), resultCheck))
        assertThat(resultCheck.get(), equalTo(1))
        verify(context.messageStateContext).finishState(any())
        verify(context.messageStateContext, never()).addState(any(), any())
    }

    class TestAggregate2(id: DataId): Aggregate(id) {
        @Suppress("unused")
        companion object {
            @TestAnnotation
            fun handle1(message: TestMessage) { message.check.getAndIncrement() }
            @TestAnnotation
            fun handle2(message: TestMessage) { message.check.getAndIncrement() }
        }
    }

    @Test
    fun testSendMessageToAggregateWithTwoCompanionObjectMessageHandlersExecutesAllHandlers() {
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate2::class)
        val resultCheck = AtomicInteger(0)
        TestMessageHandlerExecutor(context).accept(TestMessage(UUID.randomUUID(), resultCheck))
        assertThat(resultCheck.get(), equalTo(2))
        verify(context.messageStateContext).finishState(any())
        verify(context.messageStateContext, never()).addState(any(), any())
    }

    @Test
    fun testSendMessageToAggregateWithInstanceMessageHandlerExecutesThatHandler() {
        @Suppress("unused")
        class TestAggregate(id: DataId): Aggregate(id) {
            @TestAnnotation
            fun handle(message: TestMessage) { message.check.getAndIncrement() }
        }
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate::class)
        val resultCheck = AtomicInteger(0)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(any<KClass<TestAggregate>>(), any())) doReturn TestAggregate(uuid)
        TestMessageHandlerExecutor(context).accept(TestMessage(uuid, resultCheck))
        assertThat(resultCheck.get(), equalTo(1))
        verify(context.messageStateContext).finishState(any())
        verify(context.messageStateContext, never()).addState(any(), any())
    }

    @Test
    fun testSendMessageToAggregateWithTwoInstanceMessageHandlersExecutesAllHandlers() {
        @Suppress("unused", "UNUSED_PARAMETER")
        class TestAggregate(id: DataId): Aggregate(id) {
            @TestAnnotation
            fun handle(message: TestMessage) { message.check.getAndIncrement() }
            @TestAnnotation
            fun handle(id: DataId, message: TestMessage) { message.check.getAndIncrement() }
        }
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate::class)
        val resultCheck = AtomicInteger(0)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(any<KClass<TestAggregate>>(), any())) doReturn TestAggregate(uuid)
        TestMessageHandlerExecutor(context).accept(TestMessage(uuid, resultCheck))
        assertThat(resultCheck.get(), equalTo(2))
        verify(context.messageStateContext).finishState(any())
        verify(context.messageStateContext, never()).addState(any(), any())
    }

    @Suppress("unused")
    class TestAggregate3(id: DataId): Aggregate(id) {
        companion object {
            @TestAnnotation
            fun handle(message: TestMessage) { message.check.getAndIncrement() }
        }
        @TestAnnotation
        fun handle(message: TestMessage) { message.check.getAndIncrement() }
    }

    @Test
    fun testSendMessageToAggregateWithInstanceAndCompanionObjectMessageHandlersExecutesAllHandlers() {
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate3::class)
        val resultCheck = AtomicInteger(0)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(any<KClass<TestAggregate3>>(), any())) doReturn TestAggregate3(uuid)
        TestMessageHandlerExecutor(context).accept(TestMessage(uuid, resultCheck))
        assertThat(resultCheck.get(), equalTo(2))
        verify(context.messageStateContext).finishState(any())
        verify(context.messageStateContext, never()).addState(any(), any())
    }

    @Test
    fun testSendMessageToDifferentAggregatesExecutesAllHandlers() {
        @Suppress("unused")
        class TestAggregate1(id: DataId): Aggregate(id) {
            @TestAnnotation
            fun handle(message: TestMessage) { message.check.getAndIncrement() }
        }
        @Suppress("unused")
        class TestAggregate2(id: DataId): Aggregate(id) {
            @TestAnnotation
            fun handle(message: TestMessage) { message.check.getAndIncrement(); message.check.getAndIncrement() }
        }
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate1::class, TestAggregate2::class)
        val resultCheck = AtomicInteger(0)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(eq(TestAggregate1::class), any())) doReturn TestAggregate1(uuid)
        whenever(context.eventStore.retrieve(eq(TestAggregate2::class), any())) doReturn TestAggregate2(uuid)
        TestMessageHandlerExecutor(context).accept(TestMessage(uuid, resultCheck))
        assertThat(resultCheck.get(), equalTo(3))
        verify(context.messageStateContext).finishState(any())
        verify(context.messageStateContext, never()).addState(any(), any())
    }

    @Test
    fun testExceptionInsideAggregateInstanceMessageHandlerAddsExceptionToMessageState() {
        @Suppress("unused")
        class TestAggregate(id: DataId): Aggregate(id) {
            @Suppress("UNUSED_PARAMETER")
            @TestAnnotation
            fun handle(message: TestMessage) { throw IOException("IOFOUT") }
        }
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate::class)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(any<KClass<TestAggregate>>(), any())) doReturn TestAggregate(uuid)
        val logRecords = captureLog { TestMessageHandlerExecutor(context).accept(TestMessage(uuid, mock())) }
        assertThat(logRecords, hasItem<LogRecord>(allOf(
            hasProperty("level", equalTo(WARNING)),
            hasProperty("message", containsString("IOFOUT")))))
        verify(context.messageStateContext).finishState(any())
        val captor = argumentCaptor<Any>()
        verify(context.messageStateContext).addState(any(), captor.capture())
        val exception = captor.firstValue as IOException
        assertThat(exception.message, containsString("IOFOUT"))
    }

    @Test
    fun testExceptionWhileMessageHandlingExecutesOtherMessageHandlersAddsMessageStateAndFinishesMessageState() {
        fun TestMessage.inc(times: Int) { repeat(times) { check.getAndIncrement() } }
        @Suppress("unused", "UNUSED_PARAMETER")
        class TestAggregate(id: DataId): Aggregate(id) {
            @TestAnnotation
            fun handle1(message: TestMessage) = message.inc(1)
            @TestAnnotation
            fun handle2(message: TestMessage) { message.inc(2); throw IllegalStateException("Test2") }
            @TestAnnotation
            fun handle3(message: TestMessage) = message.inc(4)
            @TestAnnotation
            fun handle4(message: TestMessage) { message.inc(8); throw IllegalStateException("Test4") }
        }
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate::class)
        val resultCheck = AtomicInteger(0)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(any<KClass<TestAggregate>>(), any())) doReturn TestAggregate(uuid)
        val logRecords = captureLog { TestMessageHandlerExecutor(context).accept(TestMessage(uuid, resultCheck)) }
        assertThat(logRecords, hasItems(
            allOf(hasProperty("level", equalTo(WARNING)),
                  hasProperty("message", containsString("Test2"))),
            allOf(hasProperty("level", equalTo(WARNING)),
                  hasProperty("message", containsString("Test4")))))
        assertThat(resultCheck.get(), equalTo(15))
        verify(context.messageStateContext).finishState(any())
        verify(context.messageStateContext, times(2)).addState(any(), any())
    }

    inner class TestMessageHandlerExecutor2 constructor(context: CqrsContext):
        MessageHandlerExecutor<TestMessage>(context, { isTestMessage() }, TestAnnotation::class) {
        override fun handleMessage(message: TestMessage) {
            throw IOException("IOFOUT")
        }
    }

    @Test
    fun testExceptionInsideMessageHandlerExecutorAddsExceptionToMessageState() {
        val context = mockContext()
        val uuid = UUID.randomUUID()
        val logRecords = captureLog { TestMessageHandlerExecutor2(context).accept(TestMessage(uuid, mock())) }
        assertThat(logRecords, hasItem<LogRecord>(allOf(
            hasProperty("level", equalTo(WARNING)),
            hasProperty("message", containsString("IOFOUT")))))
        verify(context.messageStateContext).finishState(any())
        val captor = argumentCaptor<Any>()
        verify(context.messageStateContext).addState(any(), captor.capture())
        val exception = captor.firstValue as IOException
        assertThat(exception.message, containsString("IOFOUT"))
    }

    private fun mockContext(): CqrsContext {
        val context = mock<CqrsContext>()
        val messageStateContext = mock<MessageStateContext>()
        whenever(context.messageStateContext) doReturn messageStateContext
        val eventStore = mock<EventStore>()
        whenever(context.eventStore) doReturn eventStore
        return context
    }

}
