/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.infrastructure

import com.charleskorn.kaml.Yaml
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration.Companion.COMMAND_QUEUE_FILE_STORAGE_FILE
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration.Companion.EVENT_FILE_STORAGE_FILE
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration.Companion.EVENT_QUEUE_FILE_STORAGE_FILE
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration.Companion.ROTATION_COMMAND_QUEUE_FILE_STORAGE_DIR
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration.Companion.ROTATION_EVENT_FILE_STORAGE_DIR
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration.Companion.ROTATION_EVENT_QUEUE_FILE_STORAGE_DIR
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Comparator


class CqrsConfigurationTest {

    companion object {

        fun testConfig(testInfo: TestInfo): CqrsConfiguration {
            val subdir1 = testInfo.testClass.get().simpleName
            val subdir2 = testInfo.testMethod.get().name
            fun String.setSubdir() = this.replace("out/cfg", "out/cfg/$subdir1/$subdir2")
            val yamlConfig = """
                eventFileStorage:
                  path: ${EVENT_FILE_STORAGE_FILE.setSubdir()}
                  rotation:
                    directory: ${ROTATION_EVENT_FILE_STORAGE_DIR.setSubdir()}
                commandQueueFileStorage:
                  path: ${COMMAND_QUEUE_FILE_STORAGE_FILE.setSubdir()}
                  rotation:
                    directory: ${ROTATION_COMMAND_QUEUE_FILE_STORAGE_DIR.setSubdir()}
                eventQueueFileStorage:
                  path: ${EVENT_QUEUE_FILE_STORAGE_FILE.setSubdir()}
                  rotation:
                    directory: ${ROTATION_EVENT_QUEUE_FILE_STORAGE_DIR.setSubdir()}
            """.trimIndent()
            return Yaml.default.parse(CqrsConfiguration.serializer(), yamlConfig)
        }

        fun deleteEventFile(path: Path) {
            Files.deleteIfExists(path)
        }

        fun deleteRotatingEventFile(path: Path) {
            if (path.toFile().exists())
                Files.walk(path)
                    .sorted(Comparator.reverseOrder())
                    .forEach { f -> Files.delete(f) }
        }

    }

    @Test
    fun testJsonSerialisationDeserializationWithSmallChange(testInfo: TestInfo) {
        val json = Json(JsonConfiguration.Stable)
        val jsonData = json.stringify(CqrsConfiguration.serializer(), testConfig(testInfo))
        val dirPart = "cfg/CqrsConfigurationTest/testJsonSerialisationDeserializationWithSmallChange"
        val osDirPart = json.stringify(CqrsConfiguration.PathSerializer,
            Paths.get("$dirPart/test-rotating-eventfile-dir")).trim { it == '"' }
        val config = json.parse(CqrsConfiguration.serializer(), jsonData.replace(osDirPart, "ers"))
        assertThat(config.eventFileStorage.rotation.directory.toString(), equalTo(osPath("out/ers")))
        assertThat(config.eventFileStorage.path.toString(), equalTo(osPath("out/$dirPart/test-eventfile.bin")))
    }

    @Test
    fun testParseConfigJson() {
        val jsonConfig = """
            {
                "aggregateCache": {
                    "maximumSize": 99,
                    "expireAfterWriteSeconds": 5
                },
                "eventFileStorage": {
                    "path": "/file/path/events.evt",
                    "rotation": {
                        "directory": "/file/dir/eventrotatingstorage",
                        "size": 1000000000
                    }
                }
            }
        """.trimIndent()
        val config = Json(JsonConfiguration.Stable).parse(CqrsConfiguration.serializer(), jsonConfig)
        assertThat(config.aggregateCache.maximumSize, equalTo(99L))
        assertThat(config.aggregateCache.expireAfterWriteSeconds, equalTo(5L))
        assertThat(config.eventFileStorage.path.toString(), equalTo(osPath("/file/path/events.evt")))
        assertThat(config.eventFileStorage.rotation.directory.toString(), equalTo(osPath("/file/dir/eventrotatingstorage")))
        assertThat(config.eventFileStorage.rotation.size, equalTo(1_000_000_000L))
    }

    @Test
    fun testParsePartialConfigJson() {
        val jsonConfig = """
            {
                "aggregateCache": {
                    "maximumSize": 99
                },
                "eventFileStorage": {
                    "path": "/file/path/events.evt"
                }
            }
        """.trimIndent()
        val config = Json(JsonConfiguration.Stable).parse(CqrsConfiguration.serializer(), jsonConfig)
        assertThat(config.aggregateCache.maximumSize, equalTo(99L))
        assertThat(config.aggregateCache.expireAfterWriteSeconds, equalTo(360L))
        assertThat(config.eventFileStorage.path.toString(), equalTo(osPath("/file/path/events.evt")))
        assertThat(config.eventFileStorage.rotation.directory.toString(), equalTo(osPath("out/cfg/test-rotating-eventfile-dir")))
        assertThat(config.eventFileStorage.rotation.size, equalTo(10_000_000L))
    }

    @Test
    fun testYamlSerialisationDeserializationWithSmallChange(testInfo: TestInfo) {
        val yaml = Yaml.default
        val yamlData = yaml.stringify(CqrsConfiguration.serializer(), testConfig(testInfo))
        val dirPart = "testYamlSerialisationDeserializationWithSmallChange"
        val config = yaml.parse(CqrsConfiguration.serializer(), yamlData.replace(dirPart, "tysdwsc"))
        assertThat(config.eventFileStorage.rotation.directory.toString(),
            equalTo(osPath("out/cfg/CqrsConfigurationTest/tysdwsc/test-rotating-eventfile-dir")))
        assertThat(config.commandQueueFileStorage.rotation.directory.toString(),
            equalTo(osPath("out/cfg/CqrsConfigurationTest/tysdwsc/test-rotating-commandqueue-dir")))
        assertThat(config.eventFileStorage.path.toString(),
            equalTo(osPath("out/cfg/CqrsConfigurationTest/tysdwsc/test-eventfile.bin")))
    }

    @Test
    fun testParseConfigYaml() {
        val yamlConfig = """
            aggregateCache:
              maximumSize: 99
              expireAfterWriteSeconds: 5
            eventFileStorage:
              path: "/file/path/events.evt"
              rotation:
                  directory: "/file/dir/eventrotatingstorage"
                  size: 1000000000
        """.trimIndent()
        val config = Yaml.default.parse(CqrsConfiguration.serializer(), yamlConfig)
        assertThat(config.aggregateCache.maximumSize, equalTo(99L))
        assertThat(config.aggregateCache.expireAfterWriteSeconds, equalTo(5L))
        assertThat(config.eventFileStorage.path.toString(), equalTo(osPath("/file/path/events.evt")))
        assertThat(config.eventFileStorage.rotation.directory.toString(), equalTo(osPath("/file/dir/eventrotatingstorage")))
        assertThat(config.eventFileStorage.rotation.size, equalTo(1_000_000_000L))
    }

    @Test
    fun testParsePartialConfigYaml() {
        val yamlConfig = """
            eventFileStorage:
              path: "/file/path/events.evt"
              rotation:
                  size: 1000000000
        """.trimIndent()
        val config = Yaml.default.parse(CqrsConfiguration.serializer(), yamlConfig)
        assertThat(config.aggregateCache.maximumSize, equalTo(1_000L))
        assertThat(config.aggregateCache.expireAfterWriteSeconds, equalTo(360L))
        assertThat(config.eventFileStorage.path.toString(), equalTo(osPath("/file/path/events.evt")))
        assertThat(config.eventFileStorage.rotation.directory.toString(), equalTo(osPath("out/cfg/test-rotating-eventfile-dir")))
        assertThat(config.eventFileStorage.rotation.size, equalTo(1_000_000_000L))
    }

    @Test
    fun testLoadYamlFile() {
        val resource = CqrsConfigurationTest::class.java.classLoader.getResource("cqrses-config.yaml")!!
        val configFilePath = Paths.get(resource.toURI()).toAbsolutePath().toString()
        val config = CqrsConfiguration.load(configFilePath)
        assertThat(config.database.maximumPoolSize, equalTo(11))
    }

    private fun osPath(path: String): String = Paths.get(path).toString()

}
