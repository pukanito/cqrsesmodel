/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateSnapshotQuery.AggregateSnapshot
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.hamcrest.core.IsSame
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.util.Optional
import java.util.UUID

@ExperimentalCoroutinesApi
class SnapshotAggregateRebuilderTest {

    class TestAggregate(id: DataId) : Aggregate(id)

    @Test
    fun testGetCallsFallbackRebuilderWhenSnapshotIsNotPresent() = runBlockingTest {
        val id = UUID.randomUUID()
        val storage = mock<EventStorage>()
        val replayer = mock<AggregateMessageReplayer>()
        val testAggregate = TestAggregate(id)
        whenever(replayer.replay(any(), any())).doAnswer { it.getArgument(0) }
        val repository = mock<AggregateSnapshotQuery>()
        whenever(repository.get(any(), any())).doReturn(Optional.empty())
        val fallbackRebuilder = mock<AggregateRebuilder>()
        whenever(fallbackRebuilder.get(any(), any(), any())).doReturn(testAggregate)
        val aggregateRebuilder: AggregateRebuilder = SnapshotAggregateRebuilder(replayer, repository, fallbackRebuilder, mock())
        aggregateRebuilder.get(storage, TestAggregate::class, id) as TestAggregate
        verify(replayer, never()).replay(any(), any())
        verify(storage, never()).get()
        verify(storage, never()).get(any<Position<*>>())
        verify(fallbackRebuilder).get(any(), any(), any())
    }

    @Test
    fun testGetAppliesAllAssociatedEventsToSnapshot() = runBlockingTest {
        val id = UUID.randomUUID()
        val storage = mock<EventStorage>()
        val event1 = PositionedEvent(object : Event(id) {}, mock())
        val event2 = PositionedEvent(object : Event(UUID.randomUUID()) {}, mock())
        val event3 = PositionedEvent(object : Event(id) {}, mock())
        val event4 = PositionedEvent(object : Event(UUID.randomUUID()) {}, mock())
        whenever(storage.get()) doReturn flowOf(event1, event2, event3, event4)
        val replayer = mock<AggregateMessageReplayer>()
        val testAggregate = TestAggregate(id)
        whenever(replayer.replay(any(), any())).doAnswer { it.getArgument<Deferred<Aggregate>>(0).getCompleted() }
        val repository = mock<AggregateSnapshotQuery>()
        whenever(repository.get(any(), any())).doReturn(Optional.of(AggregateSnapshot(testAggregate, null)))
        val fallbackRebuilder = mock<AggregateRebuilder>()
        val aggregateRebuilder: AggregateRebuilder = SnapshotAggregateRebuilder(replayer, repository, fallbackRebuilder, mock())
        aggregateRebuilder.get(storage, TestAggregate::class, id) as TestAggregate
        val captor = argumentCaptor<Flow<PositionedEvent>>()
        Mockito.verify(replayer).replay(any(), captor.capture())
        val sequence = captor.firstValue
        MatcherAssert.assertThat(sequence.count(), IsEqual.equalTo(2))
        MatcherAssert.assertThat(sequence.first(), IsSame.sameInstance(event1))
        MatcherAssert.assertThat(sequence.drop(1).first(), IsSame.sameInstance(event3))
        verify(storage).get()
        verify(storage, never()).get(any<Position<*>>())
        verify(fallbackRebuilder, never()).get(any(), any(), any())
    }

    @Test
    fun testGetAppliesPositionedAssociatedEventsToSnapshot() = runBlockingTest {
        val id = UUID.randomUUID()
        val storage = mock<EventStorage>()
        val event1 = PositionedEvent(object : Event(id) {}, mock())
        val event2 = PositionedEvent(object : Event(UUID.randomUUID()) {}, mock())
        val event3 = PositionedEvent(object : Event(id) {}, mock())
        val event4 = PositionedEvent(object : Event(UUID.randomUUID()) {}, mock())
        whenever(storage.get(any<Position<*>>())) doReturn flowOf(event1, event2, event3, event4)
        val replayer = mock<AggregateMessageReplayer>()
        val testAggregate = TestAggregate(id)
        whenever(replayer.replay(any(), any())).doAnswer { it.getArgument<Deferred<Aggregate>>(0).getCompleted() }
        val repository = mock<AggregateSnapshotQuery>()
        whenever(repository.get(any(), any())).doReturn(Optional.of(AggregateSnapshot(testAggregate, mock())))
        val fallbackRebuilder = mock<AggregateRebuilder>()
        val aggregateRebuilder: AggregateRebuilder = SnapshotAggregateRebuilder(replayer, repository, fallbackRebuilder, mock())
        aggregateRebuilder.get(storage, TestAggregate::class, id) as TestAggregate
        val captor = argumentCaptor<Flow<PositionedEvent>>()
        Mockito.verify(replayer).replay(any(), captor.capture())
        val sequence = captor.firstValue
        MatcherAssert.assertThat(sequence.count(), IsEqual.equalTo(2))
        MatcherAssert.assertThat(sequence.first(), IsSame.sameInstance(event1))
        MatcherAssert.assertThat(sequence.drop(1).first(), IsSame.sameInstance(event3))
        verify(storage, never()).get()
        verify(storage).get(any<Position<*>>())
        verify(fallbackRebuilder, never()).get(any(), any(), any())
    }

}
