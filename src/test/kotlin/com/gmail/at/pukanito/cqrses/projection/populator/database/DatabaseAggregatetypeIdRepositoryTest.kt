/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection.populator.database

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.CommandBus
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventBus
import com.gmail.at.pukanito.cqrses.EventSourcingHandler
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventbus.LocalEventBus
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.DelegatingEventStore
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateMessageReplayer
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.NoMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.InMemoryEventStorage
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.projection.AggregatetypeIdProjection
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregatetypeIdRepository.AggregatetypeAndIdTable
import com.gmail.at.pukanito.cqrses.projection.populator.AggregatetypeIdPopulator
import com.gmail.at.pukanito.cqrses.util.MessageQueue
import com.gmail.at.pukanito.cqrses.util.messagequeue.InMemoryMessageQueue
import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Provider
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.TypeLiteral
import com.nhaarman.mockitokotlin2.mock
import com.zaxxer.hikari.HikariDataSource
import kotlinx.coroutines.FlowPreview
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.util.UUID
import java.util.function.Consumer
import javax.inject.Inject
import javax.sql.DataSource

class DatabaseAggregatetypeIdRepositoryTest {

    private lateinit var configuration: CqrsConfiguration

    class TestEvent1(id: DataId): Event(id)

    class TestEvent2(id: DataId): Event(id)

    class TestAggregate1(id: DataId): Aggregate(id) {
        @EventSourcingHandler @Suppress("unused", "UNUSED_PARAMETER")
        fun handleSourcingEvent(event: TestEvent1) {}
    }

    class TestAggregate2(id: DataId): Aggregate(id) {
        @EventSourcingHandler @Suppress("unused", "UNUSED_PARAMETER")
        fun handleSourcingEvent(event: TestEvent2) {}
    }

    private fun inMemoryModule(configuration: CqrsConfiguration, initializeEvents: Consumer<EventStore>) =
        object : AbstractModule() {
            override fun configure() {
                val messageQueueEventBaseType = object : TypeLiteral<MessageQueue<Event>>() {}
                val messageQueueEventImplementationType = object : TypeLiteral<InMemoryMessageQueue<Event>>() {}
                bind(CqrsConfiguration::class.java).toInstance(configuration)
                bind(CqrsContext::class.java).`in`(Singleton::class.java)
                bind(CommandBus::class.java).toInstance(mock())
                bind(EventBus::class.java).to(LocalEventBus::class.java).`in`(Singleton::class.java)
                bind(messageQueueEventBaseType).to(messageQueueEventImplementationType).`in`(Singleton::class.java)
                bind(object: TypeLiteral<Function0<Position<*>?>>() {}).toInstance { null }
                bind(EventStore::class.java).to(DelegatingEventStore::class.java).`in`(Singleton::class.java)
                bind(EventStorage::class.java).to(InMemoryEventStorage::class.java).`in`(Singleton::class.java)
                bind(AggregateRebuilder::class.java).to(NoMemoryAggregateRebuilder::class.java).`in`(Singleton::class.java)
                bind(AggregateMessageReplayer::class.java).`in`(Singleton::class.java)
                bind(object : TypeLiteral<Consumer<EventStore>>() {}).toInstance(initializeEvents)
                bind(AggregatetypeIdProjection::class.java).toProvider(InMemoryDbAggregatetypeIdProjectionProvider::class.java).asEagerSingleton()
            }
            @Provides @Singleton
            fun provideAggregates(): Set<AggregateClass> = setOf(TestAggregate1::class, TestAggregate2::class)
            @Provides @Singleton
            fun provideDatabase(dataSource: DataSource): Database = Database.connect(dataSource)
            @Provides @Singleton
            fun provideDatasource(config: CqrsConfiguration): DataSource = HikariDataSource().apply {
                jdbcUrl = config.database.jdbcUrl
                driverClassName = config.database.jdbcDriverClassName
                minimumIdle = config.database.maximumIdle
                maximumPoolSize = config.database.maximumPoolSize }
        }

    // @Provides @Singleton cannot be an eager singleton.
    private class InMemoryDbAggregatetypeIdProjectionProvider @Inject constructor(
            val context: CqrsContext,
            val database: Database,
            val initializeEvents: Consumer<EventStore>): Provider<AggregatetypeIdProjection> {
        override fun get(): AggregatetypeIdProjection {
            initializeEvents.accept(context.eventStore)
            return AggregatetypeIdPopulator(context, DatabaseAggregatetypeIdRepository(database))
        }
    }

    @BeforeEach
    fun cleanDatabase(testInfo: TestInfo) {
        configuration = testConfig(testInfo)
        val dataSource = HikariDataSource().apply {
            jdbcUrl = configuration.database.jdbcUrl
            driverClassName = configuration.database.jdbcDriverClassName
            minimumIdle = configuration.database.maximumIdle
            maximumPoolSize = configuration.database.maximumPoolSize }
        val database = Database.connect(dataSource)
        transaction(database) { SchemaUtils.create(AggregatetypeAndIdTable) }
        transaction(database) { AggregatetypeAndIdTable.deleteAll() }
    }

    @FlowPreview
    @Test
    fun testProjectionAppliesExistingAndNewSourcedEventsUniquelyByIdAndTypeAndUpdatesFromTheGivenPosition() {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val context = Guice
            .createInjector(inMemoryModule(configuration, Consumer {
                it.accept(TestEvent1(id1))
                it.accept(TestEvent2(id1))
                it.accept(TestEvent1(id1))
                it.accept(TestEvent2(id2)) }))
            .getInstance(CqrsContext::class.java)
        // Check update updates from the given position.
        val database = context.getInstance(Database::class.java)
        assertThat(transaction(database) { AggregatetypeAndIdTable.selectAll().count() }, equalTo(0))
        val projection = context.getInstance(AggregatetypeIdProjection::class.java)
        projection.update(context.eventStore.getPosition())
        assertThat(transaction(database) { AggregatetypeAndIdTable.selectAll().count() }, equalTo(0))
        // Check update records unique type/id.
        projection.update(null)
        assertThat(transaction(database) { AggregatetypeAndIdTable.selectAll().count() }, equalTo(3))
        // Check new published events are also projected.
        val id3 = UUID.randomUUID()
        context.eventBus.publish(TestEvent1(id3))
        context.eventBus.publish(TestEvent1(id3))
        val existingPlusPublishedCount = transaction(database) { AggregatetypeAndIdTable.selectAll().count() }
        assertThat(existingPlusPublishedCount, equalTo(4))
    }

    @FlowPreview
    @Test
    fun testManyEventsProjection() {
        val context = Guice
            .createInjector(inMemoryModule(configuration, Consumer { eventStore ->
                repeat((1..1_000).count()) { eventStore.accept(
                    TestEvent1(UUID.randomUUID())) } }))
            .getInstance(CqrsContext::class.java)
        context.getInstance(AggregatetypeIdProjection::class.java).update(null)
        val database = context.getInstance(Database::class.java)
        assertThat(transaction(database) { AggregatetypeAndIdTable.selectAll().count() }, equalTo(1_000))
    }

}
