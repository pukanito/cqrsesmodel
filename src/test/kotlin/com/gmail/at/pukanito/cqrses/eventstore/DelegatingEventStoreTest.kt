/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.IsSame.sameInstance
import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import java.util.UUID

@ExperimentalCoroutinesApi
class DelegatingEventStoreTest {

    @Test
    fun testAcceptStoresEvent() {
        val storage = mock<EventStorage>()
        val event = mock<Event>()
        val eventStore: EventStore = DelegatingEventStore(storage, mock(), mock())
        eventStore.accept(event)
        val captor = argumentCaptor<Event>()
        verify(storage).accept(captor.capture())
        val acceptedEvent = captor.firstValue
        assertThat(acceptedEvent, sameInstance(event))
    }

    @Test
    fun testGetReturnsStoredEvents() = runBlockingTest {
        val storage = mock<EventStorage>()
        val positionedEvent = mock<PositionedEvent>()
        whenever(storage.get()) doReturn flowOf(positionedEvent)
        val eventStore: EventStore = DelegatingEventStore(storage, mock(), mock())
        val getEvents = eventStore.get()
        assertThat(getEvents.count(), equalTo(1))
        assertThat(getEvents.first(), sameInstance(positionedEvent))
    }

    @Test
    fun testPositionedGetReturnsStoredEvents() = runBlockingTest {
        val storage = mock<EventStorage>()
        val positionedEvent = mock<PositionedEvent>()
        whenever(storage.get(any<Position<*>>())) doReturn flowOf(positionedEvent)
        val eventStore: EventStore = DelegatingEventStore(storage, mock(), mock())
        val getEvents = eventStore.get(mock<Position<*>>())
        assertThat(getEvents.count(), equalTo(1))
        assertThat(getEvents.first(), sameInstance(positionedEvent))
    }

    @Test
    fun testLimitiedPositionedGetReturnsStoredEvents() = runBlockingTest {
        val storage = mock<EventStorage>()
        val positionedEvent = mock<PositionedEvent>()
        whenever(storage.get(any(), any())) doReturn flowOf(positionedEvent)
        val eventStore: EventStore = DelegatingEventStore(storage, mock(), mock())
        val getEvents = eventStore.get(1, mock())
        assertThat(getEvents.count(), equalTo(1))
        assertThat(getEvents.first(), sameInstance(positionedEvent))
    }

    @Test
    fun testRetrieveReturnsRebuilderAggregate() = runBlockingTest {
        val rebuilder = mock<AggregateRebuilder>()
        val aggregate = mock<Aggregate>()
        whenever(rebuilder.get(any(), any(), any())) doReturn aggregate
        val eventStorage = mock<EventStorage>()
        val eventStore: EventStore = DelegatingEventStore(eventStorage, rebuilder, mock())
        val id = UUID.randomUUID()
        val getAggregate = eventStore.retrieve(Aggregate::class, id)
        assertThat(getAggregate, sameInstance(aggregate))
        verify(rebuilder).get(any(), eq(Aggregate::class), eq(id))
    }

}
