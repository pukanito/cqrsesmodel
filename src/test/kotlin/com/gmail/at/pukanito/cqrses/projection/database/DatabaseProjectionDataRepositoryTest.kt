/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.projection.database

import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileWriter.EventFilePosition
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFileWriter
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFileWriter.RotatingEventFilePositionSpec
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.cqrses.projection.ProjectionDataRepository.ProjectionData
import com.gmail.at.pukanito.cqrses.projection.database.DatabaseProjectionDataRepository.ProjectionDataTable
import com.gmail.at.pukanito.cqrses.util.singlylinkedlist.SinglyLinkedList
import com.zaxxer.hikari.HikariDataSource
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.IsNull.nullValue
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo

class DatabaseProjectionDataRepositoryTest {

    private lateinit var database: Database

    @BeforeEach
    fun createTables(testInfo: TestInfo) {
        database = testConfig(testInfo).let { config ->
            Database.connect(HikariDataSource().apply {
                jdbcUrl = config.database.jdbcUrl
                driverClassName = config.database.jdbcDriverClassName
                minimumIdle = config.database.maximumIdle
                maximumPoolSize = config.database.maximumPoolSize })}
        transaction(database) { SchemaUtils.create(ProjectionDataTable) }
        transaction(database) { ProjectionDataTable.deleteAll() }
    }

    @Test
    fun testLoadWithoutSaveReturnsEmptyProjectionData() {
        val repository = DatabaseProjectionDataRepository(database)
        val parsedData = repository.load()
        assertThat(parsedData.aggregatetypeIdProjectionPosition, nullValue())
    }

    @Test
    fun testSaveDifferentProjectionData() {
        val repository = DatabaseProjectionDataRepository(database)
        repository.save(ProjectionData(null, null))
        repository.save(ProjectionData(EventFilePosition(1), null))
        assertThat(repository.load(), equalTo(ProjectionData(EventFilePosition(1), null)))
        assertThat(DatabaseProjectionDataRepository(database).load(),
            equalTo(ProjectionData(EventFilePosition(1), null)))
    }

    @Test
    fun testNullAggregatetypeIdProjectionPositionSerialization() {
        val repository = DatabaseProjectionDataRepository(database)
        val data = ProjectionData(null, null)
        repository.save(data)
        val parsedData = repository.load()
        assertThat(parsedData, equalTo(data))
        assertThat(DatabaseProjectionDataRepository(database).load(), equalTo(data))
    }

    @Test
    fun testEventFileAggregatetypeIdProjectionPositionSerialization() {
        val repository = DatabaseProjectionDataRepository(database)
        val data = ProjectionData(EventFilePosition(1), null)
        repository.save(data)
        val parsedData = repository.load()
        assertThat(parsedData, equalTo(data))
        assertThat(DatabaseProjectionDataRepository(database).load(), equalTo(data))
    }

    @Test
    fun testRotatingEventFileAggregatetypeIdProjectionPositionSerialization() {
        val repository = DatabaseProjectionDataRepository(database)
        val data = ProjectionData(RotatingEventFileWriter.RotatingEventFilePosition(
            RotatingEventFilePositionSpec(2, EventFilePosition(1))), null)
        repository.save(data)
        val parsedData = repository.load()
        assertThat(parsedData, equalTo(data))
        assertThat(DatabaseProjectionDataRepository(database).load(), equalTo(data))
    }

    @Test
    fun testSinglyLinkedListAggregatetypeIdProjectionPositionSerialization() {
        val repository = DatabaseProjectionDataRepository(database)
        val data = ProjectionData(SinglyLinkedList<Int>().getPosition(), null)
        repository.save(data)
        val parsedData = repository.load()
        assertThat(parsedData, equalTo(data))
        assertThat(DatabaseProjectionDataRepository(database).load(), equalTo(data))
    }

    @Test
    fun testSaveDifferentFields() {
        val repository = DatabaseProjectionDataRepository(database)
        val data = ProjectionData(null, null)
        repository.save(data)
        val dataset1 = repository.load()
        dataset1.aggregatetypeIdProjectionPosition = EventFilePosition(5)
        val dataset2 = repository.load()
        dataset2.aggregateSnapshotProjectionPosition = EventFilePosition(7)
        repository.save(dataset1)
        repository.save(dataset2)
        val resultData = repository.load()
        val expectedData = ProjectionData(EventFilePosition(5), EventFilePosition(7))
        assertThat(resultData, equalTo(expectedData))
    }

}
