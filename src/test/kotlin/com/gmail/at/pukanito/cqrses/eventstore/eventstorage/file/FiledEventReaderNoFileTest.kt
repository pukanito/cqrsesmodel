/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.FiledEventReader.FiledEvent
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import java.nio.MappedByteBuffer

@ExperimentalCoroutinesApi
class FiledEventReaderNoFileTest {

    @Test
    fun testReturnsEmtpySequenceAndPerformsOnEndOfBuffer() = runBlockingTest {
        val filedEventReader = FiledEventReaderNoFile()
        val buffer = mock<MappedByteBuffer>()
        whenever(buffer.hasRemaining()) doReturn false
        val flow: Flow<Pair<FiledEvent, Position<*>>> = filedEventReader.generateFlow(buffer)
        assertThat(flow.count(), equalTo(0))
    }

}
