/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.testfixtures

import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.CommandBus
import com.gmail.at.pukanito.cqrses.EventBus
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Singleton
import com.nhaarman.mockitokotlin2.mock

class MocksModule : AbstractModule() {

    companion object {
        val mocksModule = MocksModule()
    }

    val mockCommandBus = mock<CommandBus>()
    val mockEventBus = mock<EventBus>()
    val mockEventStore = mock<EventStore>()

    override fun configure() {
        bind(CqrsContext::class.java).`in`(Singleton::class.java)
        bind(CommandBus::class.java).toInstance(mockCommandBus)
        bind(EventBus::class.java).toInstance(mockEventBus)
        bind(EventStore::class.java).toInstance(mockEventStore)
    }

    @Provides @Singleton
    fun provideAggregates(): Set<AggregateClass> = setOf(TestAggregate::class)

}
