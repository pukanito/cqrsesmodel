/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses

import com.gmail.at.pukanito.cqrses.CqrsNoMemoryRebuilderTest.TestAggregate.Companion.eventHandlingCount
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.DelegatingEventStore
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateMessageReplayer
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.NoMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.InMemoryEventStorage
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Singleton
import com.google.inject.TypeLiteral
import com.nhaarman.mockitokotlin2.mock
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsIterableContainingInOrder.contains
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import java.util.UUID.randomUUID

/**
 * Test:
 * - [Model]
 * - [Context]
 * - [DelegatingEventStore]
 * - [InMemoryEventStorage]
 * - [NoMemoryAggregateRebuilder]
 *
 * excluded:
 * - [CommandBus]
 * - [EventBus]
 *
 * Test handling of events by @[EventSourcingHandler]s
 */
class CqrsNoMemoryRebuilderTest {

    private val testModule = object : AbstractModule() {
        val mockCommandBus = mock<CommandBus>()
        val mockEventBus = mock<EventBus>()
        override fun configure() {
            bind(CqrsContext::class.java).`in`(Singleton::class.java)
            bind(CommandBus::class.java).toInstance(mockCommandBus)
            bind(EventBus::class.java).toInstance(mockEventBus)
            bind(object: TypeLiteral<Function0<Position<*>?>>() {}).toInstance { null }
            bind(EventStore::class.java).to(DelegatingEventStore::class.java).`in`(Singleton::class.java)
            bind(EventStorage::class.java).to(InMemoryEventStorage::class.java).`in`(Singleton::class.java)
            bind(AggregateRebuilder::class.java).to(NoMemoryAggregateRebuilder::class.java).`in`(Singleton::class.java)
            bind(AggregateMessageReplayer::class.java).`in`(Singleton::class.java)
        }
    }

    class TestAggregate(id: DataId): Aggregate(id) {
        companion object {
            var eventHandlingCount = 0
        }
        val data: MutableList<String> = mutableListOf()
        @EventSourcingHandler @Suppress("unused")
        fun handle(event: TestEvent) {
            eventHandlingCount++
            data += event.data
        }
    }

    class TestEvent(id: DataId, val data: String): Event(id)

    @Test
    fun testEventsArriveAtCorrectAggregate() {
        checkEventsArriveAtCorrectAggregate(testModule)
        assertThat(eventHandlingCount, equalTo(8))
    }

    companion object {
        fun checkEventsArriveAtCorrectAggregate(module: AbstractModule) {
            eventHandlingCount = 0
            Guice.createInjector(module).getInstance(CqrsContext::class.java).run {
                val id = randomUUID()
                storeEvent(TestEvent(id, "TEST1"))
                val id2 = randomUUID()
                storeEvent(TestEvent(id2, "TEST2"))
                assertThat(getTestAggregateData(id), contains("TEST1"))
                assertThat(getTestAggregateData(id2), contains("TEST2"))
                storeEvent(TestEvent(id, "TEST3"))
                assertThat(getTestAggregateData(id), contains("TEST1", "TEST3"))
                val id3 = randomUUID()
                storeEvent(TestEvent(id3, "TEST4"))
                assertThat(getTestAggregateData(id2), contains("TEST2"))
                assertThat(getTestAggregateData(id), contains("TEST1", "TEST3"))
                assertThat(getTestAggregateData(id3), contains("TEST4"))
            }
        }

        private fun CqrsContext.storeEvent(event: Event) {
            eventStore.accept(event)
        }

        private fun CqrsContext.getTestAggregateData(id: DataId): List<String> =
            eventStore.retrieve(TestAggregate::class, id).data

    }

}
