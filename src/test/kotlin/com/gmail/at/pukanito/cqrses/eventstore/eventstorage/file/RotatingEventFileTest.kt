/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import java.nio.file.Paths

class RotatingEventFileTest {

    @Test
    fun testGetMaxFileNumReturnsCorrectValueWhenEventFilesArePresent() {
        val path1 = Paths.get("eventstore_1.bin")
        val path2 = Paths.get("eventstore_2.bin")
        val path3 = Paths.get("eventstore_12.bin")
        val maxFileNum = RotatingEventFile.getMaxFileNum(sequenceOf(path1, path2, path3)) { true }
        assertThat(maxFileNum, equalTo(12))
    }

    @Test
    fun testGetMaxFileNumReturnsZeroWhenNoEventFilesArePresent() {
        val maxFileNum = RotatingEventFile.getMaxFileNum(emptySequence()) { true }
        assertThat(maxFileNum, equalTo(0))
    }

    private fun pathOf(base: String, num: Int) = Paths.get(base, "eventstore_$num.bin")

    @Test
    fun testEventFileNamesRotateOnDemand() {
        val rotatingEventFile = RotatingEventFile(Paths.get(".")) { 5 }
        assertThat(rotatingEventFile.getCurrentEventFilePath(), equalTo(pathOf(".", 5)))
        assertThat(rotatingEventFile.getNextEventFilePath(), equalTo(pathOf(".", 6)))
        assertThat(rotatingEventFile.getCurrentEventFilePath(), equalTo(pathOf(".", 6)))
        assertThat(rotatingEventFile.getNextEventFilePath(), equalTo(pathOf(".", 7)))
        assertThat(rotatingEventFile.getCurrentEventFilePath(), equalTo(pathOf(".", 7)))
    }

    @Test
    fun testEventFileNamesStartRotationAt1() {
        val rotatingEventFile = RotatingEventFile(Paths.get("out/cfg/base/it")) { 0 }
        assertThat(rotatingEventFile.getNextEventFilePath(), equalTo(pathOf("out/cfg/base/it", 1)))
    }

    @Test
    fun testGetEventFilePathsReturnsAllAbsolutePathsSorted() {
        val path1 = Paths.get("eventstore_1.bin")
        val path2 = Paths.get("eventstore_2.bin")
        val path3 = Paths.get("eventstore_12.bin")
        val rotatingEventFile = RotatingEventFile(Paths.get(""))
        val pathsSequence = rotatingEventFile.getEventFilePaths(sequenceOf(path2, path3, path1)) { true }
        val pathSpecs = pathsSequence.toList()
        assertThat(pathSpecs.count(), equalTo(3))
        val workingDir = Paths.get("").toAbsolutePath()
        assertThat(pathSpecs[0].first, equalTo(1))
        assertThat(pathSpecs[0].second, equalTo(workingDir.resolve("eventstore_1.bin")))
        assertThat(pathSpecs[1].first, equalTo(2))
        assertThat(pathSpecs[1].second, equalTo(workingDir.resolve("eventstore_2.bin")))
        assertThat(pathSpecs[2].first, equalTo(12))
        assertThat(pathSpecs[2].second, equalTo(workingDir.resolve("eventstore_12.bin")))
    }

    @Test
    fun testGetEventFilePathsReturnsAllAbsolutePathsFilterOnFileNumFilterSorted() {
        val path1 = Paths.get("eventstore_1.bin")
        val path2 = Paths.get("eventstore_2.bin")
        val path3 = Paths.get("eventstore_12.bin")
        val rotatingEventFile = RotatingEventFile(Paths.get(""))
        val pathsSequence = rotatingEventFile.getEventFilePaths(sequenceOf(path2, path3, path1), { it > 1 }) { true }
        val pathSpecs = pathsSequence.toList()
        assertThat(pathSpecs.count(), equalTo(2))
        val workingDir = Paths.get("").toAbsolutePath()
        assertThat(pathSpecs[0].first, equalTo(2))
        assertThat(pathSpecs[0].second, equalTo(workingDir.resolve("eventstore_2.bin")))
        assertThat(pathSpecs[1].first, equalTo(12))
        assertThat(pathSpecs[1].second, equalTo(workingDir.resolve("eventstore_12.bin")))
    }

    @Test
    fun testGetEventFilePathsReturnsEmptySequenceWhenEmptyInput() {
        val rotatingEventFile = RotatingEventFile(Paths.get(""))
        val pathsSequence = rotatingEventFile.getEventFilePaths(emptySequence()) { true }
        assertThat(pathsSequence.count(), equalTo(0))
    }

    @Test
    fun testGetEventFilePathsReturnsEmptySequenceWhenFileFiltered() {
        val path1 = Paths.get("eventstore_1.bin")
        val path2 = Paths.get("eventstore_2.bin")
        val path3 = Paths.get("eventstore_12.bin")
        val rotatingEventFile = RotatingEventFile(Paths.get(""))
        val pathsSequence = rotatingEventFile.getEventFilePaths(sequenceOf(path2, path3, path1)) { false }
        assertThat(pathsSequence.count(), equalTo(0))
    }

    @Test
    fun testGetEventFilePathsReturnsEmptySequenceWhenFilenumFiltered() {
        val path1 = Paths.get("eventstore_1.bin")
        val path2 = Paths.get("eventstore_2.bin")
        val path3 = Paths.get("eventstore_12.bin")
        val rotatingEventFile = RotatingEventFile(Paths.get(""))
        val pathsSequence = rotatingEventFile.getEventFilePaths(sequenceOf(path2, path3, path1), { it > 12 }) { true }
        assertThat(pathsSequence.count(), equalTo(0))
    }

}
