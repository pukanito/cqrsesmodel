/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.infrastructure

import com.gmail.at.pukanito.cqrses.Message
import org.hamcrest.MatcherAssert
import org.hamcrest.collection.IsCollectionWithSize.hasSize
import org.hamcrest.collection.IsIterableContainingInOrder.contains
import org.junit.jupiter.api.Test
import java.util.UUID

class MessageStateContextTest {

    @Test
    fun testNotWatchedMessageStateIsIgnored() {
        val message = object : Message(UUID.randomUUID()) {}
        val messageStateContext = MessageStateContext()
        messageStateContext.addState(message, 1)
        messageStateContext.addState(message, 2)
        messageStateContext.finishState(message)
    }

    @Test
    fun testWatchedMessageStateCompletesFuture() {
        val message = object : Message(UUID.randomUUID()) {}
        val messageStateContext = MessageStateContext()
        val future = messageStateContext.getState(message)
        messageStateContext.addState(message, 1)
        messageStateContext.addState(message, "x")
        messageStateContext.finishState(message)
        val states = future.get()
        MatcherAssert.assertThat(states, hasSize(2))
        MatcherAssert.assertThat(states, contains(1 as Any, "x"))
    }

}