/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.number.OrderingComparison.lessThan
import org.junit.jupiter.api.Test
import java.util.UUID

class ModelTest {

    @Test
    fun testDataWithIdChecksIdCorrectly() {
        val id = UUID.randomUUID()
        val d = object : DataWithId(id) {}
        assertThat(d.hasId(UUID.randomUUID()), equalTo(false))
        assertThat(d.hasId(UUID.fromString(id.toString())), equalTo(true))
    }

    @Test
    fun testSequenceIdCreatedEarlierIsSmaller() {
        val s1 = SequenceId()
        val s2 = SequenceId()
        assertThat(s1, lessThan(s2))
        assertThat(s1, equalTo(s1))
        assertThat(s2, equalTo(s2))
    }

    @Test
    fun testMessagesGetIncreasingOffset() {
        val m1 = object : Message(UUID.randomUUID()) {}
        val m2 = object : Message(UUID.randomUUID()) {}
        assertThat(m1.sequenceId, lessThan(m2.sequenceId))
    }

}
