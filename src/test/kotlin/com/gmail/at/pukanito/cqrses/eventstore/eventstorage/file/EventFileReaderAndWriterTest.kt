/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import com.fasterxml.jackson.annotation.JsonCreator
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.deleteEventFile
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.testutil.TestPosition
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.number.OrderingComparison.greaterThan
import org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import org.junit.jupiter.api.assertThrows
import java.nio.file.Path
import java.util.Random
import java.util.UUID
import java.util.logging.Logger

@ExperimentalCoroutinesApi
class EventFileReaderAndWriterTest {

    private lateinit var eventFilePath: Path

    @BeforeEach
    fun deleteEventFile(testInfo: TestInfo) {
        eventFilePath = testConfig(testInfo).eventFileStorage.path
        deleteEventFile(eventFilePath)
    }

    class TestEvent @JsonCreator constructor(id: UUID): Event(id)

    private val lotsOf = 50_000

    @Test
    fun testWriteLotsOfEventsAndReadThemWhileWriting() = runBlockingTest {
        val logger = Logger.getLogger("testWriteLotsOfEventsAndReadThemWhileWriting")
        val controlCounts = setOf(lotsOf, Random().nextInt(lotsOf), Random().nextInt(lotsOf))
        val jobs = mutableListOf<Job>()
        EventFileWriter(eventFilePath).use { fileEventStorage ->
            (1..lotsOf).forEach { count ->
                val event = TestEvent(UUID.randomUUID())
                val previousSize = fileEventStorage.size()
                fileEventStorage.accept(event)
                assertThat(fileEventStorage.size(), greaterThan(previousSize))
                if (count in controlCounts) {
                    jobs += launch(Dispatchers.IO) {
                        EventFileReader(eventFilePath, Dispatchers.IO).let {
                            logger.warning("Counting... $count")
                            val events = it.get()
                            val eventsCountedInFile = events.count()
                            logger.warning("Counted... $eventsCountedInFile")
                            assertThat(eventsCountedInFile, greaterThanOrEqualTo(count)) } } } } }
        jobs.forEach { runBlocking { it.join() } }
    }

    class TestEventX @JsonCreator constructor(id: UUID, val x: Int, val y: String): Event(id)

    @Test
    fun testWriteSerializesExtraFieldsAndReadDeserializesThem() = runBlockingTest {
        val uuid = UUID.randomUUID()
        EventFileWriter(eventFilePath).use { fileEventStorage ->
            val event = TestEventX(uuid, 17, "TestY")
            fileEventStorage.accept(event)
            assertThat(fileEventStorage.size(), greaterThan(32L))
        }
        val event = EventFileReader(eventFilePath).get().first().event as TestEventX
        assertThat(event.id, equalTo(uuid))
        assertThat(event.x, equalTo(17))
        assertThat(event.y, equalTo("TestY"))
    }

    @Test
    fun testEventFileReaderWithoutFileReturnsEmptySequence() = runBlockingTest {
        val count = EventFileReader(eventFilePath).get().count()
        assertThat(count, equalTo(0))
    }

    @Test
    fun testEventFileWriterWithoutFileReturnsSizeZero() {
        EventFileWriter(eventFilePath).use { assertThat(it.size(), equalTo(0L)) }
    }

    @Test
    fun testEventFileReaderCanReadMultipleSequencesAtTheSameTime() = runBlockingTest {
        val count = 100
        EventFileWriter(eventFilePath).use { fileEventStorage ->
            (1..count).forEach { _ -> fileEventStorage.accept(TestEvent(UUID.randomUUID())) }
        }
        val reader = EventFileReader(eventFilePath)
        val events1 = reader.get()
        val events2 = reader.get()
        assertThat(events2.count(), equalTo(count))
        assertThat(events1.count(), equalTo(count))
    }

    @Test
    fun testEventFileReaderSequenceClosesChannel() = runBlockingTest {
        val count = 100
        EventFileWriter(eventFilePath).use { fileEventStorage ->
            (1..count).forEach { _ -> fileEventStorage.accept(TestEvent(UUID.randomUUID())) }
        }
        EventFileReader(eventFilePath).get().first()
    }

    @Test
    fun testEventFileWriterAppendsToExistingFile() = runBlockingTest {
        EventFileWriter(eventFilePath).use { it.accept(TestEvent(UUID.randomUUID())) }
        EventFileWriter(eventFilePath).use { it.accept(TestEvent(UUID.randomUUID())) }
        assertThat(EventFileReader(eventFilePath).get().count(), equalTo(2))
    }

    @Test
    fun testPositionedGetReturnsFromHead() = runBlockingTest {
        val position = EventFileWriter(eventFilePath).use { fileEventStorage ->
            val position = fileEventStorage.getPosition()
            (1..30).forEach { _ -> fileEventStorage.accept(TestEvent(UUID.randomUUID())) }
            position
        }
        assertThat(EventFileReader(eventFilePath).get(position).count(), equalTo(30))
    }

    @Test
    fun testPositionedGetReturnsFromPosition() = runBlockingTest {
        val position = EventFileWriter(eventFilePath).use { fileEventStorage ->
            (1..10).forEach { _ -> fileEventStorage.accept(TestEvent(UUID.randomUUID())) }
            val position = fileEventStorage.getPosition()
            (1..20).forEach { _ -> fileEventStorage.accept(TestEvent(UUID.randomUUID())) }
            position
        }
        assertThat(EventFileReader(eventFilePath).get(position).count(), equalTo(20))
    }

    @Test
    fun testPositionedGetWithWrongPositionTypeThrowsExceptionWhenUsingSequence() {
        assertThrows<IllegalStateException> { runBlocking {
            EventFileReader(eventFilePath).get(TestPosition(0)).count() } }
    }

    @Test
    fun testCountZeroPositionedSequenceReturnsAllEvents() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()
        val position = EventFileWriter(eventFilePath).use { fileEventStorage ->
            fileEventStorage.accept(TestEvent(id1))
            val position = fileEventStorage.getPosition()
            fileEventStorage.accept(TestEvent(id2))
            fileEventStorage.accept(TestEvent(id3))
            position
        }
        assertThat(EventFileReader(eventFilePath).get(0, position).count(), equalTo(2))
    }

    @Test
    fun testCountOneNullPositionedSequenceReturnsOneEvent() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()
        EventFileWriter(eventFilePath).use { fileEventStorage ->
            fileEventStorage.accept(TestEvent(id1))
            val position = fileEventStorage.getPosition()
            fileEventStorage.accept(TestEvent(id2))
            fileEventStorage.accept(TestEvent(id3))
            position
        }
        assertThat(EventFileReader(eventFilePath).get(1).count(), equalTo(1))
        assertThat(EventFileReader(eventFilePath).get(1).first().id, equalTo(id1))
    }

    @Test
    fun testCountOnePositionedSequenceReturnsOneEvent() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()
        val position = EventFileWriter(eventFilePath).use { fileEventStorage ->
            fileEventStorage.accept(TestEvent(id1))
            val position = fileEventStorage.getPosition()
            fileEventStorage.accept(TestEvent(id2))
            fileEventStorage.accept(TestEvent(id3))
            position
        }
        assertThat(EventFileReader(eventFilePath).get(1, position).count(), equalTo(1))
        assertThat(EventFileReader(eventFilePath).get(1, position).first().id, equalTo(id2))
    }

    @Test
    fun testCountManyPositionedSequenceReturnsRemaining() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()
        val position = EventFileWriter(eventFilePath).use { fileEventStorage ->
            fileEventStorage.accept(TestEvent(id1))
            val position = fileEventStorage.getPosition()
            fileEventStorage.accept(TestEvent(id2))
            fileEventStorage.accept(TestEvent(id3))
            position
        }
        assertThat(EventFileReader(eventFilePath).get(10, position).count(), equalTo(2))
    }

    @Test
    fun testCountPositionedSequenceReturnsPositionOfNextEvent() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()
        val position = EventFileWriter(eventFilePath).use { fileEventStorage ->
            val position = fileEventStorage.getPosition()
            fileEventStorage.accept(TestEvent(id1))
            fileEventStorage.accept(TestEvent(id2))
            fileEventStorage.accept(TestEvent(id3))
            position
        }
        assertThat(EventFileReader(eventFilePath).get(10, position).count(), equalTo(3))
        val position1 = EventFileReader(eventFilePath).get(10, position).first().position
        assertThat(EventFileReader(eventFilePath).get(10, position1).count(), equalTo(2))
        val position2 = EventFileReader(eventFilePath).get(10, position1).first().position
        assertThat(EventFileReader(eventFilePath).get(10, position2).count(), equalTo(1))
        assertThat(EventFileReader(eventFilePath).get(1, position2).first().id, equalTo(id3))
        val position3 = EventFileReader(eventFilePath).get(10, position2).first().position
        assertThat(EventFileReader(eventFilePath).get(10, position3).count(), equalTo(0))
    }

}
