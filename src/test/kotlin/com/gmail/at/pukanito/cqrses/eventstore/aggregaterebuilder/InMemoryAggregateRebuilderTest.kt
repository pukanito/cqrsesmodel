/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventStore.PositionedEvent
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.InMemoryEventStorage
import com.gmail.at.pukanito.testutil.TestPosition
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.IsNot.not
import org.hamcrest.core.IsNull.notNullValue
import org.hamcrest.core.IsSame.sameInstance
import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import java.util.UUID

@ExperimentalCoroutinesApi
class InMemoryAggregateRebuilderTest {

    class TestAggregate(id: DataId) : Aggregate(id)

    @Test
    fun testAggregatesAreCached() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val testAggregate1 = TestAggregate(id1)
        val testPosition1 = TestPosition(131)
        testAggregate1.version = testPosition1
        val id2 = UUID.randomUUID()
        val testAggregate2 = TestAggregate(id2)
        val testPosition2 = TestPosition(261)
        testAggregate2.version = testPosition2
        val replayer = mock<AggregateMessageReplayer>()
        whenever(replayer.replay(any(), any())).doAnswer { it.getArgument<Deferred<Aggregate>>(0).getCompleted() }
        val fallbackRebuilder = mock<AggregateRebuilder>()
        whenever(fallbackRebuilder.get(any(), any(), any())).doReturn(testAggregate1).doReturn(testAggregate2)
        val aggregateRebuilder: AggregateRebuilder = InMemoryAggregateRebuilder(replayer, 10, 3600, fallbackRebuilder, mock())
        val inMemoryEventStorage = InMemoryEventStorage(mock())
        val aggregate1 = aggregateRebuilder.get(inMemoryEventStorage, TestAggregate::class, id1) as TestAggregate
        assertThat(aggregate1, notNullValue())
        assertThat(aggregate1.version as TestPosition, equalTo(testPosition1))
        val aggregate2 = aggregateRebuilder.get(inMemoryEventStorage, TestAggregate::class, id2) as TestAggregate
        assertThat(aggregate2, notNullValue())
        assertThat(aggregate2.version as TestPosition, equalTo(testPosition2))
        val aggregate2Again = aggregateRebuilder.get(inMemoryEventStorage, TestAggregate::class, id2) as TestAggregate
        assertThat(aggregate2Again, notNullValue())
        assertThat(aggregate2Again.version as TestPosition, equalTo(testPosition2))
        verify(fallbackRebuilder, times(2)).get(any(), any(), any())
    }

    @Test
    fun testInMemoryAggregateRebuilderReturnsDifferentInstances() = runBlockingTest {
        val id = UUID.randomUUID()
        val testAggregate = TestAggregate(id)
        val testPosition = TestPosition(1)
        testAggregate.version = testPosition
        val replayer = mock<AggregateMessageReplayer>()
        whenever(replayer.replay(any(), any())).doAnswer { it.getArgument<Deferred<Aggregate>>(0).getCompleted() }
        val fallbackRebuilder = mock<AggregateRebuilder>()
        whenever(fallbackRebuilder.get(any(), any(), any())).doReturn(testAggregate)
        val aggregateRebuilder: AggregateRebuilder = InMemoryAggregateRebuilder(replayer, 10, 3600, fallbackRebuilder, mock())
        val inMemoryEventStorage = InMemoryEventStorage(mock())
        val aggregate1 = aggregateRebuilder.get(inMemoryEventStorage, TestAggregate::class, id) as TestAggregate
        val aggregate1a = aggregateRebuilder.get(inMemoryEventStorage, TestAggregate::class, id) as TestAggregate
        assertThat(aggregate1, not(sameInstance(aggregate1a)))
        verify(fallbackRebuilder).get(any(), any(), any())
    }

    @Test
    fun testGetOnlyAppliesAssociatedEvents() = runBlockingTest {
        val id = UUID.randomUUID()
        val storage = mock<EventStorage>()
        val event1 = PositionedEvent(object : Event(id) {}, mock())
        val event2 = PositionedEvent(object : Event(UUID.randomUUID()) {}, mock())
        val event3 = PositionedEvent(object : Event(id) {}, mock())
        val event4 = PositionedEvent(object : Event(UUID.randomUUID()) {}, mock())
        whenever(storage.get()) doReturn flowOf(event1, event2, event3, event4)
        val replayer = mock<AggregateMessageReplayer>()
        val testAggregate = TestAggregate(id)
        whenever(replayer.replay(any(), any())).doAnswer { it.getArgument<Deferred<Aggregate>>(0).getCompleted() }
        val fallbackRebuilder = mock<AggregateRebuilder>()
        whenever(fallbackRebuilder.get(any(), any(), any())).doReturn(testAggregate)
        val aggregateRebuilder: AggregateRebuilder = InMemoryAggregateRebuilder(replayer, 10, 3600, fallbackRebuilder, mock())
        aggregateRebuilder.get(storage, TestAggregate::class, id) as TestAggregate
        val captor = argumentCaptor<Flow<PositionedEvent>>()
        verify(replayer).replay(any(), captor.capture())
        val sequence = captor.firstValue
        assertThat(sequence.count(), equalTo(2))
        assertThat(sequence.first(), sameInstance(event1))
        assertThat(sequence.drop(1).first(), sameInstance(event3))
    }

    @Test
    fun testGetOnlyAppliesUnappliedAssociatedEvents() = runBlockingTest {
        val id = UUID.randomUUID()
        val storage = mock<EventStorage>()
        whenever(storage.get()) doReturn flowOf(
            PositionedEvent(object : Event(id) {}, mock()),
            PositionedEvent(object : Event(id) {}, mock()))
        whenever(storage.get(any<Position<*>>())) doReturn emptyFlow()
        whenever(storage.getPosition()) doReturn mock()
        val replayer = mock<AggregateMessageReplayer>()
        val testAggregate = TestAggregate(id)
        whenever(replayer.replay(any(), any())).doAnswer { it.getArgument<Deferred<Aggregate>>(0).getCompleted() }
        val fallbackRebuilder = mock<AggregateRebuilder>()
        whenever(fallbackRebuilder.get(any(), any(), any())).doReturn(testAggregate)
        val aggregateRebuilder: AggregateRebuilder = InMemoryAggregateRebuilder(replayer, 10, 3600, fallbackRebuilder, mock())
        aggregateRebuilder.get(storage, TestAggregate::class, id) as TestAggregate
        aggregateRebuilder.get(storage, TestAggregate::class, id) as TestAggregate
        val captor = argumentCaptor<Flow<PositionedEvent>>()
        verify(replayer, times(2)).replay(any(), captor.capture())
        assertThat(captor.firstValue.count(), equalTo(2))
        assertThat(captor.secondValue.count(), equalTo(0))
        val inOrder = inOrder(storage)
        inOrder.verify(storage).get()
        inOrder.verify(storage).get(any<Position<*>>())
    }

}
