/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.commandbus

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.Command
import com.gmail.at.pukanito.cqrses.CommandHandler
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.infrastructure.MessageStateContext
import com.gmail.at.pukanito.cqrses.util.messagequeue.InMemoryMessageQueue
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.hamcrest.core.StringContains.containsString
import org.junit.jupiter.api.Test
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import java.io.IOException
import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger
import kotlin.reflect.KClass

class LocalCommandBusTest {

    class TestCommand(id: DataId): Command(id) {
        val check: AtomicInteger = AtomicInteger(0)
        override fun toString(): String ="TestCommand(check=$check) ${super.toString()}"
    }

    @Test
    fun testSendCommandToAggregateWithInstanceCommandHandlerExecutesThatHandler() {
        @Suppress("unused")
        class TestAggregate(id: DataId): Aggregate(id) {
            @CommandHandler
            fun handle(command: TestCommand) { command.check.getAndIncrement() }
        }
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate::class)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(any<KClass<TestAggregate>>(), any())) doReturn TestAggregate(uuid)
        val testCommand = TestCommand(UUID.randomUUID())
        LocalCommandBus(context, InMemoryMessageQueue(), mock()).handle(testCommand)
        assertThat(testCommand.check.get(), IsEqual.equalTo(1))
        verify(context.messageStateContext).finishState(any())
        verify(context.messageStateContext, never()).addState(any(), any())
    }

    @Test
    fun testSendCommandWithoutCommandHandlerAddsIllegalStateExceptionToMessageState() {
        val context = mockContext()
        LocalCommandBus(context, InMemoryMessageQueue(), mock()).handle(TestCommand(UUID.randomUUID()))
        verify(context.messageStateContext).finishState(any())
        val captor = argumentCaptor<Any>()
        verify(context.messageStateContext).addState(any(), captor.capture())
        val exception = captor.firstValue as IllegalStateException
        assertThat(exception.message, containsString("No command handler"))
        assertThat(exception.message, containsString("TestCommand"))
    }

    @Test
    fun testExceptionInsideAggregateInstanceMessageHandlerAddsExceptionToMessageState() {
        @Suppress("unused")
        class TestAggregate(id: DataId): Aggregate(id) {
            @Suppress("UNUSED_PARAMETER")
            @CommandHandler
            fun handle(event: TestCommand) { throw IOException("IOFOUT") }
        }
        val context = mockContext()
        whenever(context.aggregateClasses) doReturn setOf(TestAggregate::class)
        val uuid = UUID.randomUUID()
        whenever(context.eventStore.retrieve(any<KClass<TestAggregate>>(), any())) doReturn TestAggregate(uuid)
        LocalCommandBus(context, InMemoryMessageQueue(), mock()).handle(TestCommand(UUID.randomUUID()))
        verify(context.messageStateContext).finishState(any())
        val captor = argumentCaptor<Any>()
        verify(context.messageStateContext).addState(any(), captor.capture())
        val exception = captor.firstValue as IOException
        assertThat(exception.message, containsString("IOFOUT"))
    }

    private fun mockContext(): CqrsContext {
        val context = mock<CqrsContext>()
        val messageStateContext = mock<MessageStateContext>()
        whenever(context.messageStateContext) doReturn messageStateContext
        val eventStore = mock<EventStore>()
        whenever(context.eventStore) doReturn eventStore
        return context
    }

}
