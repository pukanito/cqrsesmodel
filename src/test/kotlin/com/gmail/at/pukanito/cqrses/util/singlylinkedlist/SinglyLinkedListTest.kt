/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.util.singlylinkedlist

import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.testutil.TestPosition
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class SinglyLinkedListTest {

    @Test
    fun testConsumingUnknownPositionTypeThrowsException() {
        SinglyLinkedList<Int>().run {
            val position = TestPosition(1)
            assertThrows<IllegalStateException> { hasItem(position) }
            assertThrows<IllegalStateException> { get(position) } }
    }

    @Test
    fun testListIsInitiallyEmptyAndOnlyGetThrowsException() {
        SinglyLinkedList<Int>().run {
            assertThat(hasItem(), equalTo(false))
            assertThrows<NoSuchElementException> { get() }
            assertCountItems(0)
            val position = getPosition()
            assertThat(hasItem(position), equalTo(false))
            assertThrows<NoSuchElementException> { get(position) } }
    }

    @Test
    fun testWalkList() {
        SinglyLinkedList<Int>().run {
            add(1)
            assertThat(hasItem(), equalTo(true))
            add(2)
            var position: Position<*>? = null
            while (hasItem(position)) position = get(position).second
            assertCountItems(2) }
    }

    @Test
    fun testListItemsCanAllBeRemoved() {
        SinglyLinkedList<Int>().run {
            add(1, 2)
            assertThat(hasItem(), equalTo(true))
            assertThat(get().first, equalTo(1))
            removeWhile { true }
            assertThat(hasItem(), equalTo(false))
            assertThrows<NoSuchElementException> { get() }
            assertCountItems(0) }
    }

    @Test
    fun testListItemsCanPartlyBeRemoved() {
        SinglyLinkedList<Int>().run {
            add(1)
            assertCountItems(1)
            add(2)
            assertCountItems(2)
            add(1)
            assertCountItems(3)
            removeWhile { e -> e < 2 }
            assertThat(hasItem(), equalTo(true))
            assertThat(get().first, equalTo(2))
            assertCountItems(2) }
    }

    @Test
    fun testConsumingRemovedItemThrowsException() {
        SinglyLinkedList<Int>().run {
            add(1)
            val position = getPosition()
            add(2, 3)
            removeWhile { e -> e < 3 }
            assertCountItems(1)
            assertThrows<NoSuchElementException> { hasItem(position) }
            assertThrows<NoSuchElementException> { get(position) }
            assertThat(hasItem(), equalTo(true))
            assertThat(get().first, equalTo(3)) }
    }

    @Test
    fun testTailPositionUpdatesWhenAddingItems() {
        SinglyLinkedList<Int>().run {
            val position0 = getPosition()
            assertThat(hasItem(position0), equalTo(false))
            assertThrows<NoSuchElementException> { get(position0) }
            add(1)
            assertThat(hasItem(position0), equalTo(true))
            assertThat(get(position0).first, equalTo(1))
            val position1 = getPosition()
            assertThat(hasItem(position1), equalTo(false))
            assertThrows<NoSuchElementException> { get(position1) }
            add(2)
            assertThat(hasItem(position1), equalTo(true))
            assertThat(get(position1).first, equalTo(2))
            removeWhile { true }
            assertThrows<NoSuchElementException> { hasItem(position0) }
            assertThrows<NoSuchElementException> { get(position0) }
        }
    }

    private fun SinglyLinkedList<Int>.assertCountItems(expectedCount: Int, position: Position<*>? = null) {
        var count = 0
        var currentPosition: Position<*>? = position
        while (hasItem(currentPosition)) {
            count++
            currentPosition = get(currentPosition).second
        }
        assertThat(count, equalTo(expectedCount))
    }

}
