package com.gmail.at.pukanito.cqrses.util.singlylinkedlist

import com.gmail.at.pukanito.testutil.TestPosition
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class SinglyLinkedListSequenceTest {

    @Test
    fun testConsumingUnknownPositionTypeThrowsException() {
        SinglyLinkedList<Int>().run {
            val position = TestPosition(1)
            val sequence = asSequence(position)
            assertThrows<IllegalStateException> { sequence.count() } }
    }

    @Test
    fun testSinglyLinkedListIsInitiallyEmpty() {
        SinglyLinkedList<Int>().run {
            assertThat(asSequence().count(), equalTo(0)) }
    }

    @Test
    fun testSinglyLinkedListSequenceCanBeSequencedManyTimes() {
        SinglyLinkedList<Int>().run {
            val sequence = asSequence()
            add(1, 2)
            assertThat(sequence.first(), equalTo(1))
            assertThat(sequence.elementAt(1), equalTo(2)) }
    }

    @Test
    fun testSinglyLinkedListSequenceIsConsumedLazy() {
        SinglyLinkedList<Int>().run {
            val sequence0 = asSequence()
            add(1)
            val sequence1 = asSequence()
            add(2)
            val sequence2 = asSequence()
            removeWhile { true }
            val sequence3 = asSequence()
            assertThat(sequence0.count(), equalTo(0))
            assertThat(sequence1.count(), equalTo(0))
            assertThat(sequence2.count(), equalTo(0))
            assertThat(sequence3.count(), equalTo(0))
            add(3)
            assertThat(sequence0.count(), equalTo(1))
            assertThat(sequence1.count(), equalTo(1))
            assertThat(sequence2.count(), equalTo(1))
            assertThat(sequence3.count(), equalTo(1)) }
    }

    @Test
    fun testSinglyLinkedListPositionedSequenceIsConsumedLazy() {
        SinglyLinkedList<Int>().run {
            val sequence0 = asSequence(getPosition())
            add(1)
            val sequence1 = asSequence(getPosition())
            add(2)
            val sequence2 = asSequence(getPosition())
            removeWhile { true }
            val sequence3 = asSequence(getPosition())
            assertThrows<NoSuchElementException> { sequence0.count() }
            assertThrows<NoSuchElementException> { sequence1.count() }
            assertThat(sequence2.count(), equalTo(0))
            assertThat(sequence3.count(), equalTo(0))
            add(3)
            assertThat(sequence2.count(), equalTo(1))
            assertThat(sequence3.count(), equalTo(1)) }
    }

    @Test
    fun testCountZeroSequenceReturnsAllItems() {
        SinglyLinkedList<Int>().run {
            val sequence0 = asSequence(0)
            val sequence1 = asSequence(0, getPosition())
            add(1)
            val sequence2 = asSequence(0, getPosition())
            add(2)
            val sequence3 = asSequence(0, getPosition())
            add(3)
            assertThat(sequence0.count(), equalTo(3))
            assertThat(sequence1.count(), equalTo(3))
            assertThat(sequence2.count(), equalTo(2))
            assertThat(sequence3.count(), equalTo(1)) }
    }

    @Test
    fun testCountedSequenceReturnsRequestedItems() {
        SinglyLinkedList<Int>().run {
            val sequence0 = asSequence(2)
            val sequence1 = asSequence(2, getPosition())
            add(1)
            val sequence2 = asSequence(2, getPosition())
            add(2)
            val sequence3 = asSequence(2, getPosition())
            add(3)
            assertThat(sequence0.count(), equalTo(2))
            assertThat(sequence1.count(), equalTo(2))
            assertThat(sequence2.count(), equalTo(2))
            assertThat(sequence3.count(), equalTo(1)) }
    }

    @Test
    fun testCountPositionedSequenceReturnsPositionOfNextItem() {
        SinglyLinkedList<Int>().run {
            val position = getPosition()
            add(1, 2, 3)
            assertThat(asSequence(10, position).count(), equalTo(3))
            val position1 = asSequence(10, position).first().second
            assertThat(asSequence(10, position1).count(), equalTo(2))
            val position2 = asSequence(10, position1).first().second
            assertThat(asSequence(10, position2).count(), equalTo(1))
            assertThat(asSequence(1, position2).first().first, equalTo(3))
            val position3 = asSequence(10, position2).first().second
            assertThat(asSequence(10, position3).count(), equalTo(0)) }
    }

}
