/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.aggregatesnapshotrepository

import com.github.npathai.hamcrestopt.OptionalMatchers.isPresent
import com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd
import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateSnapshotQuery.AggregateSnapshot
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.EventFileWriter.EventFilePosition
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.AggregateSnapshotTable
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.SerialisedAggregate
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.SerialisedPosition
import com.zaxxer.hikari.HikariDataSource
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.beans.HasPropertyWithValue.hasProperty
import org.hamcrest.core.AllOf.allOf
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.IsNot.not
import org.hamcrest.core.IsSame
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.util.UUID

class DatabaseAggregateSnapshotQueryTest {

    private lateinit var database: Database

    private val id = UUID.randomUUID()!!

    @Suppress("unused") // Used in test.
    class TestAggregate(id: DataId, val data: String): Aggregate(id)

    class TestAggregate2(id: DataId): Aggregate(id)

    @BeforeEach
    fun cleanDatabaseAndInitialize(testInfo: TestInfo) {
        database = testConfig(testInfo).let { config ->
            Database.connect(HikariDataSource().apply {
                jdbcUrl = config.database.jdbcUrl
                driverClassName = config.database.jdbcDriverClassName
                minimumIdle = config.database.maximumIdle
                maximumPoolSize = config.database.maximumPoolSize })}
        transaction(database) { SchemaUtils.create(AggregateSnapshotTable) }
        transaction(database) { AggregateSnapshotTable.deleteAll() }
    }

    @Test
    fun testRepositoryReadsSnapshot() {
        val mapper = DatabaseAggregateSnapshotRepository.mapper()
        val aggregate = TestAggregate(id, "TEST1")
        transaction(database) { AggregateSnapshotTable.insert {
            it[id] = aggregate.id
            it[type] = aggregate::class.java.typeName
            it[position] = mapper.writeValueAsString(SerialisedPosition(EventFilePosition(999)))
            it[snapshot] = mapper.writeValueAsString(SerialisedAggregate(aggregate)) } }
        val repository = DatabaseAggregateSnapshotQuery(database)
        val snapshot = repository.get(TestAggregate::class, id)
        @Suppress("RemoveExplicitTypeArguments") // IDE error?
        assertThat(snapshot, isPresentAnd<AggregateSnapshot>(allOf(
                hasProperty("aggregate", hasProperty<TestAggregate>("id", equalTo(id))),
                hasProperty("aggregate", hasProperty<TestAggregate>("data", equalTo("TEST1"))),
                hasProperty("position", equalTo(EventFilePosition(999))))))
    }

    @Test
    fun testRepositoryDoesNotReadsSnapshotWithWrongId() {
        val mapper = DatabaseAggregateSnapshotRepository.mapper()
        val aggregate = TestAggregate(id, "TEST1")
        transaction(database) { AggregateSnapshotTable.insert {
            it[id] = aggregate.id
            it[type] = aggregate::class.java.typeName
            it[position] = mapper.writeValueAsString(SerialisedPosition(EventFilePosition(999)))
            it[snapshot] = mapper.writeValueAsString(SerialisedAggregate(aggregate)) } }
        val repository = DatabaseAggregateSnapshotQuery(database)
        val snapshot = repository.get(TestAggregate::class, UUID.randomUUID())
        assertThat(snapshot, not(isPresent()))
    }

    @Test
    fun testRepositoryDoesNotReadsSnapshotWithWrongType() {
        val mapper = DatabaseAggregateSnapshotRepository.mapper()
        val aggregate = TestAggregate(id, "TEST1")
        transaction(database) { AggregateSnapshotTable.insert {
            it[id] = aggregate.id
            it[type] = aggregate::class.java.typeName
            it[position] = mapper.writeValueAsString(SerialisedPosition(EventFilePosition(999)))
            it[snapshot] = mapper.writeValueAsString(SerialisedAggregate(aggregate)) } }
        val repository = DatabaseAggregateSnapshotQuery(database)
        val snapshot = repository.get(TestAggregate2::class, id)
        assertThat(snapshot, not(isPresent()))
    }

    @Test
    fun testRepositoryReturnsDifferentInstances() {
        val mapper = DatabaseAggregateSnapshotRepository.mapper()
        val aggregate = TestAggregate(id, "TEST1")
        transaction(database) { AggregateSnapshotTable.insert {
            it[id] = aggregate.id
            it[type] = aggregate::class.java.typeName
            it[position] = mapper.writeValueAsString(SerialisedPosition(EventFilePosition(999)))
            it[snapshot] = mapper.writeValueAsString(SerialisedAggregate(aggregate)) } }
        val repository = DatabaseAggregateSnapshotQuery(database)
        val aggregate1 = repository.get(TestAggregate::class, id)
        val aggregate1a = repository.get(TestAggregate::class, id)
        assertThat(aggregate1, not(IsSame.sameInstance(aggregate1a)))
    }

}
