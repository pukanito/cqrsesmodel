/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file

import com.fasterxml.jackson.annotation.JsonCreator
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.deleteRotatingEventFile
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfigurationTest.Companion.testConfig
import com.gmail.at.pukanito.testutil.TestPosition
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import org.junit.jupiter.api.assertThrows
import java.nio.file.Path
import java.util.Random
import java.util.UUID
import java.util.logging.Logger

@FlowPreview
@ExperimentalCoroutinesApi
class RotatingEventFileReaderAndWriterTest {

    private lateinit var eventFilePath: Path

    @BeforeEach
    fun deleteRotatingEventFile(testInfo: TestInfo) {
        eventFilePath = testConfig(testInfo).eventFileStorage.rotation.directory
        deleteRotatingEventFile(eventFilePath)
    }

    class TestEvent @JsonCreator constructor(id: UUID): Event(id)

    private val lotsOf = 50_000

    @Test
    fun testWriteLotsOfEventsAndReadThemWhileWriting() = runBlockingTest {
        val logger = Logger.getLogger("testWriteLotsOfEventsAndReadThemWhileWriting")
        val controlCounts = setOf(lotsOf, Random().nextInt(lotsOf), Random().nextInt(lotsOf))
        val jobs = mutableListOf<Job>()
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000_000)
        RotatingEventFileWriter(rotatingEventFile).use { fileEventStorage ->
            (1..lotsOf).forEach { count ->
                val event = TestEvent(UUID.randomUUID())
                fileEventStorage.accept(event)
                if (count in controlCounts) {
                    jobs += launch(Dispatchers.IO) {
                        RotatingEventFileReader(rotatingEventFile, Dispatchers.IO).let {
                            logger.warning("Counting... $count")
                            val events = it.get()
                            val eventsCountedInFile = events.count()
                            logger.warning("Counted... $eventsCountedInFile")
                            assertThat(eventsCountedInFile, greaterThanOrEqualTo(count)) } } } } }
        jobs.forEach { runBlocking { it.join() } }
    }

    class TestEventX @JsonCreator constructor(id: UUID, val x: Int, val y: String): Event(id)

    @Test
    fun testWriteSerializesExtraFieldsAndReadDeserializesThem() = runBlockingTest {
        val uuid = UUID.randomUUID()
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000_000)
        RotatingEventFileWriter(rotatingEventFile).use { fileEventStorage ->
            val event = TestEventX(uuid, 17, "TestY")
            fileEventStorage.accept(event)
        }
        val event = RotatingEventFileReader(rotatingEventFile).get().first().event as TestEventX
        assertThat(event.id, equalTo(uuid))
        assertThat(event.x, equalTo(17))
        assertThat(event.y, equalTo("TestY"))
    }

    @Test
    fun testEventFileReaderWithoutFileReturnsEmptySequence() = runBlockingTest {
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000_000)
        val count = RotatingEventFileReader(rotatingEventFile).get().count()
        assertThat(count, equalTo(0))
    }

    @Test
    fun testEventFileWriterWithoutFileReturnsSizeZero() {
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000_000)
        RotatingEventFileWriter(rotatingEventFile).use { assertThat(it.size(), equalTo(0L)) }
    }

    @Test
    fun testEventFileReaderCanReadMultipleSequencesAtTheSameTime() = runBlockingTest {
        val count = 100
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000_000)
        RotatingEventFileWriter(rotatingEventFile).use { fileEventStorage ->
            (1..count).forEach { _ -> fileEventStorage.accept(
                EventFileReaderAndWriterTest.TestEvent(UUID.randomUUID())) }
        }
        val reader = RotatingEventFileReader(rotatingEventFile)
        val events1 = reader.get()
        val events2 = reader.get()
        assertThat(events2.count(), equalTo(count))
        assertThat(events1.count(), equalTo(count))
    }

    @Test
    fun testEventFileWriterAppendsToExistingFile() = runBlockingTest {
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000_000)
        RotatingEventFileWriter(rotatingEventFile).accept(TestEvent(UUID.randomUUID()))
        RotatingEventFileWriter(rotatingEventFile).accept(TestEvent(UUID.randomUUID()))
        val reader = RotatingEventFileReader(rotatingEventFile)
        assertThat(reader.get().count(), equalTo(2))
        assertThat(rotatingEventFile.getEventFilePaths().count(), equalTo(2))
    }

    @Test
    fun testEventFileWriterAppendsToExistingFileWhenRotationSizeIsSmall() = runBlockingTest {
        val rotatingEventFile = RotatingEventFile(eventFilePath, 10)
        RotatingEventFileWriter(rotatingEventFile).accept(TestEvent(UUID.randomUUID()))
        RotatingEventFileWriter(rotatingEventFile).accept(TestEvent(UUID.randomUUID()))
        val reader = RotatingEventFileReader(rotatingEventFile)
        assertThat(reader.get().count(), equalTo(2))
        assertThat(rotatingEventFile.getEventFilePaths().count(), equalTo(4))
    }


    @Test
    fun testPositionedGetReturnsFromHead() = runBlockingTest {
        val rotatingEventFile = RotatingEventFile(eventFilePath, 500)
        val position = RotatingEventFileWriter(rotatingEventFile).use { fileEventStorage ->
            val position = fileEventStorage.getPosition()
            (1..30).forEach { _ -> fileEventStorage.accept(TestEvent(UUID.randomUUID())) }
            position
        }
        assertThat(RotatingEventFileReader(rotatingEventFile).get(position).count(), equalTo(30))
    }

    @Test
    fun testPositionedGetReturnsFromPosition() = runBlockingTest {
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000)
        repeat(20) { index ->
            val position = RotatingEventFileWriter(rotatingEventFile).use { fileEventStorage ->
                (1..index).forEach { _ -> fileEventStorage.accept(TestEvent(UUID.randomUUID())) }
                val position = fileEventStorage.getPosition()
                (1..10).forEach { _ -> fileEventStorage.accept(TestEvent(UUID.randomUUID())) }
                position
            }
            assertThat(RotatingEventFileReader(rotatingEventFile).get(position).count(), equalTo(10))
        }
    }

    @Test
    fun testPositionedGetWithWrongPositionTypeThrowsExceptionWhenUsingSequence() {
        val rotatingEventFile = RotatingEventFile(eventFilePath, 200)
        assertThrows<IllegalStateException> { runBlocking {
            RotatingEventFileReader(rotatingEventFile).get(TestPosition(1)).count() } }
    }

    @Test
    fun testCountZeroPositionedSequenceReturnsAllEvents() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000)
        val position = RotatingEventFileWriter(rotatingEventFile).use { fileEventStorage ->
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id1))
            val position = fileEventStorage.getPosition()
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id2))
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id3))
            position
        }
        assertThat(RotatingEventFileReader(rotatingEventFile).get(0, position).count(), equalTo(2))
    }

    @Test
    fun testCountOneNullPositionedSequenceReturnsOneEvent() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000)
        RotatingEventFileWriter(rotatingEventFile).use { fileEventStorage ->
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id1))
            val position = fileEventStorage.getPosition()
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id2))
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id3))
            position
        }
        assertThat(RotatingEventFileReader(rotatingEventFile).get(1).count(), equalTo(1))
        assertThat(RotatingEventFileReader(rotatingEventFile).get(1).first().id, equalTo(id1))
    }

    @Test
    fun testCountOnePositionedSequenceReturnsOneEvent() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000)
        val position = RotatingEventFileWriter(rotatingEventFile).use { fileEventStorage ->
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id1))
            val position = fileEventStorage.getPosition()
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id2))
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id3))
            position
        }
        assertThat(RotatingEventFileReader(rotatingEventFile).get(1, position).count(), equalTo(1))
        assertThat(RotatingEventFileReader(rotatingEventFile).get(1, position).first().id, equalTo(id2))
    }

    @Test
    fun testCountManyPositionedSequenceReturnsRemaining() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000)
        val position = RotatingEventFileWriter(rotatingEventFile).use { fileEventStorage ->
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id1))
            val position = fileEventStorage.getPosition()
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id2))
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id3))
            position
        }
        assertThat(RotatingEventFileReader(rotatingEventFile).get(10, position).count(), equalTo(2))
    }

    @Test
    fun testCountPositionedSequenceReturnsPositionOfNextEvent() = runBlockingTest {
        val id1 = UUID.randomUUID()
        val id2 = UUID.randomUUID()
        val id3 = UUID.randomUUID()
        val rotatingEventFile = RotatingEventFile(eventFilePath, 1_000)
        val position = RotatingEventFileWriter(rotatingEventFile).use { fileEventStorage ->
            val position = fileEventStorage.getPosition()
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id1))
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id2))
            fileEventStorage.accept(EventFileReaderAndWriterTest.TestEvent(id3))
            position
        }
        assertThat(RotatingEventFileReader(rotatingEventFile).get(10, position).count(), equalTo(3))
        val position1 = RotatingEventFileReader(rotatingEventFile).get(10, position).first().position
        assertThat(RotatingEventFileReader(rotatingEventFile).get(10, position1).count(), equalTo(2))
        val position2 = RotatingEventFileReader(rotatingEventFile).get(10, position1).first().position
        assertThat(RotatingEventFileReader(rotatingEventFile).get(10, position2).count(), equalTo(1))
        assertThat(RotatingEventFileReader(rotatingEventFile).get(1, position2).first().id, equalTo(id3))
        val position3 = RotatingEventFileReader(rotatingEventFile).get(10, position2).first().position
        assertThat(RotatingEventFileReader(rotatingEventFile).get(10, position3).count(), equalTo(0))
    }

}
