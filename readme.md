# CQRS/ES in Kotlin

CQRS = Command Query Responsibility Segregation

ES = Event Sourcing

To become more acquainted with CQRS and Event Sourcing I started writing a small framework that
implements both. The design goal was to make a framework that helps to get 
a better understanding of CQRS/ES. Performance, concurrency and scalability were not 
yet in scope, but maintainability is, hence it should be easy to modify and/or extend. 
It is written in Kotlin because that is my current development language.

THIS IS NOT PRODUCTION READY!!

## Description

The framework is a library that consists of annotations, interfaces and 
implementations to make
using CQRS/ES easier. The model of the library is described in `model.kt`. It 
contains the following definitions:

* `typealias DataId`: an alias for the data type that is used as ID for aggregates.
* `typealias AggregateClass`: an alias for the base type of all aggregate classes.
* `abstract class DataWithId`: an object with a `DataId`.
* `class SequenceId`: an object for determining order. Every instance of SequenceId is larger
than all instancess created earlier, and smaller than all instances created later on.
* `abstract class Message: DataWithId`: base class for all messages.
* `abstract class Command: Message`: a command is an intention to do something with
an aggregate. The ID of the command specifies the destination aggregate instance.
* `abstract class Event: Message`: an event is something that has happened on an 
aggregate. The ID of the event specifies the aggregate instance that was affected.
* `abstract class MessageHandlingObject: DataWithId`: base class for message handling objects
that (may) have state.
* `abstract class Aggregate: MessageHandlingObject`: an aggregate is an object that has state. It
reacts to commands and publishes events to signal change of state. In this framework
it will also be used as Reactor to events.
* `annotation class CommandHandler`: an annotation to mark a method as a command handler.
A command handler reacts to specific commands.
* `annotation class EventHandler`: an annotation to mark a method as an event handler.
An event handler reacts to specific events.
* `annotation class EventSourcingHandler`: an annotation to mark a method as an event
sourcing handler. An event sourcing handler is an event handler that also reacts to events
that are replayed when rebuilding aggregate state.

And there are three interfaces that model the basic API for interacting with the model:

* `interface CommandBus`: a command bus dispatches commands to registered aggregates.
* `interface EventBus`: an event bus dispatches events to registered aggregates. It
also replays events on aggregates that are rebuilt.
* `interface EventStore`: an event store stores and retrieves events. It also creates
aggregates for applying events and rebuilding. 

The framework should relieve the developer from standard cqrs/es functionality
so that he/she only has to concentrate on:

* framework setup,
* aggregate (reactor) definitions,
* commands and command handlers,
* events and event handlers,
* projections,
* queries,
* error handling.

### Setup

Google Guice is used for dependency injection. 

Inside the framework, the command bus, event bus and event store need each other to
perform the necessary work. For convenience, there is a class `CqrsContext` in the infrastructure package
that can be used by each, command bus, event bus and event store implementations, 
to access one of the others. For that, `CqrsContext` contains the following properties and methods:

* `val commandBus`
* `val eventBus`
* `val eventStore`
* `val aggregateClasses`
* `val messageStateContext`
* `fun <T> getInstance(type: Class<T>)`: get an instance of the specified type.

`CqrsContext` has one constructor parameter, a Guice injector. The Guice injector
is used to get instances of the command bus, event bus and event store. 
Typical instantiation and usage of a `CqrsContext` looks like:

```kotlin
val module = object : AbstractModule() {
    override fun configure() {
        bind(CqrsContext::class.java).`in`(Singleton::class.java)
        bind(CommandBus::class.java).to(LocalCommandBus::class.java).`in`(Singleton::class.java)
        bind(EventBus::class.java).to(LocalEventBus::class.java).`in`(Singleton::class.java)
        bind(EventStore::class.java).to(DelegatingEventStore::class.java).`in`(Singleton::class.java)
        bind(EventStorage::class.java).to(InMemoryEventStorage::class.java).`in`(Singleton::class.java)
        bind(AggregateMessageReplayer::class.java).`in`(Singleton::class.java)
        ... more bindings
    }
    @Provides @Singleton
    fun provideAggregates(): List<AggregateClass> = listOf(TestAggregate::class)
    ... more providers
}
val injector = Guice.createInjector(module)
val context = injector.getInstance(CqrsContext::class.java)
context.commandBus.handle(...some Command instance...)
...
```

The `List<AggregateClass>` in the above code is used by the framework
to find command handlers and event (sourcing) handlers.

### Aggregates

An aggregate is an object that has state that is only used for validating commands.
As a consequence it only exists when it has to react to a command.

If a command is accepted it may result in the publication of one or more events. 

Typical command handling will be as follows:

* the command bus receives a command for a specific aggregate,
* the command bus asks the event store to create the initial aggregate and supply a sequence
of all previous events for that aggregate,
* the command bus asks the event bus to replay the sequence of events on the initial aggregate,
resulting in an up to date aggregate state, rebuild using `@EventSourcingHandler`s,
* the command bus supplies the command to the up to date aggregate,
* the aggregate reacts to the command with `@CommandHandler`s that may publish events,
* the aggregate is no longer necessary, it will be rebuild when needed again.

In this framework the aggregate is also used as a reactor. When used as a reactor, 
the aggregate state is also used to react to events on the event bus. 
Typical event handling will be as follows:

* the event bus receives an event for a specific aggregate,
* the event bus asks the event store to create the initial aggregate and supply a sequence
of all previous events for that aggregate,
* the event bus replays the sequence of events on the initial aggregate,
resulting in an up to date aggregate state, rebuild using `@EventSourcingHandler`s,
* the event bus supplies the event to the up to date aggregate,
* the aggregate reacts to the event with `@EventHandler`s,
* the aggregate is no longer necessary, it will be rebuild when needed again.

For performance reasons it may be convenient to not always have to rebuild the complete
aggregate state but store intermediate state of the aggregate and use that when rebuilding.
Storing of intermediate state is also known as aggregate snapshots. An aggregate
snapshot contains the state of a known sequence of events. When rebuilding of an 
aggregate is necessary, the event store will use an aggregate snapshot instead of creating
an initial aggregate and supply only the events that were published after creation of the 
snapshot. This will result in faster rebuild. The framework contains an event store
implementation that supports aggregate snapshots.

Every aggregate is a descendant of the `Aggregate` class and needs a constructor
with exactly one `DataId` parameter which is used by the framework when creating an initial instance
for command handling and/or event handling:

```kotlin
class SomeAggregate(id: DataId): Aggregate(id){
    ...
}
```

### Commands and Command handlers

A command is an intention to do something with an aggregate, it is a `Message`.

It contains at least the ID of the aggregate to act upon. Before the command will be supplied
to the aggregate, the aggregate needs to be rebuild or, if it does not exist yet, 
be created. Hence, a command can be used to do something with a new aggregate or 
an existing aggregate.

Besides the ID, a command can also contain more data, necessary for doing something
with an aggregate.

In the framework, a command always is a descendant of the `Command` class:

```kotlin
class NewCommand(id: DataId [, more properties]): Command(id) {
   ... implementation if necessary ...
}
```

A command handler can be specified on an aggregate using the `@CommandHandler` annotation:

```kotlin
class SomeAggregate(id: DataId): Aggregate(id){

    @CommandHandler
    fun handleNewCommand(command: NewCommand [, more parameters]) {
        ... validate state and handle the command ...
    }
}
```

The command that is handled by the command handler is the type of the first
parameter in the parameter list that is of type Command or a subtype.
Hence the above code will handle commands of type NewCommand or subtypes of NewCommand, but:

```kotlin
@CommandHandler
fun handleNewCommand(id: DataId, command: NewCommand [, more parameters]) {
    ... validate state and handle the command ...
}
```
will also handle the same command types, because the first parameter in the parameter list
that is of type Command or a subtype is the parameter named 'command'.

The command parameter is injected into the handler by the command bus. Other types of
parameters can also be injected by the command bus:

* `DataId`: the id of the Command,
* `Context`: the CqrsContext,
* other types bound in the Guice initialization module.

A command handler method can be a:

* companion object method
* instance method

Commands are handled in sequence. If a command handler is busy handling a command
and inside that command handler another command is published on the `CommandBus`, 
the new command will not be handled until the busy command handler has finished.

### Events and Event handlers

An event is something that has happened on an aggregate that caused its state to change,
it is a `Message`. Events are used to rebuild the state of aggregates so that they can
handle new commands and/or events.

An event contains at least the ID of the aggregate that changed state. 
Since, in this framework, an aggregate also acts as a reactor, 
the event will be supplied to the aggregate 
whos state has changed. Before the event will be supplied
to the aggregate, the aggregate needs to be rebuild, using all previous events.

Besides the ID, an event should contain data that indicates what state change
occured.

In the framework, an event always is a descendant of the `Event` class:

```kotlin
class NewEvent(id: DataId [, more properties]): Event(id) {
   ... implementation if necessary ...
}
```

An event handler can be specified on an aggregate using the `@EventHandler` annotation
or the `@EventSourcingHandler` annotation:

```kotlin
class SomeAggregate(id: DataId): Aggregate(id){

    @EventHandler
    fun handleNewEvent(event: NewEvent [, more parameters]) {
        ... handle the event ...
    }
}
```

The event that is handled by the event handler is the type of the first
parameter in the parameter list that is of type Event or a subtype.
Hence the above code will handle events of type NewEvent or subtypes of NewEvent, but:

```kotlin
@EventHandler
fun handleNewEvent(id: DataId, event: NewEvent [, more parameters]) {
    ... handle the event ...
}
```
will handle the same event types, because the first parameter in the parameter list
that is of type Event or a subtype is the parameter named 'event'.

The event parameter is injected into the handler by the event bus. Other types of
parameters can also be injected by the event bus:

* `DataId`: the id of the Event,
* `Context`: the CqrsContext,
* other types bound in the Guice initialization module.

An event handler method can be a:

* companion object method
* instance method

Events are handled in sequence. If an event handler is busy handling an event
and inside that event handler another event is published on the `EventBus`, 
the new event will not be handled until the busy event handler has finished.

The difference between `@EventSourcingHandler` and `@EventHandler` is that the former is 
only executed when an aggregate (reactor) is rebuilt, while the latter is only executed 
when an event is published on the event bus and the aggregate is used as a reactor. Hence 
`@EventSourcingHandler` is useful for rebuilding aggregate (reactor) state, when handling
commands supplied to the aggregate or events supplied to the aggragate as reactor, while
`@EventHandler` is useful for side effects that must only occur once when the event
was published, but not when rebuilding aggregates (or reactors).

When an event is published on the `EventBus` it is not yet used to rebuild aggregate
(or reactor) state. Hence when an `@EventHandler` is executed the aggregate (or reactor)
state is according to the last event before this one, so that the event handler is
able to evaluate the state change caused by the event.

### Messages and MessageState

When a `Message` (`Command` or `Event`) is handled the handler might throw an exception.
This may cause problems if it is not handled in a correct way, however the framework
does not know how to handle exceptions.
Exceptions occuring during message handling are logged using the java logging framework
with level `WARNING` and otherwise ignored. 
To help handling exceptions a mechanism is built in that can
detect exceptions thrown inside message handlers. This mechanism can keep track of the
state of a message and return that state when a message has been handled.

`Context.messageState` has the following methods to facilitate message state:

* `fun getMessageState(Message): CompletableFuture<List<Any>>`: initialize message state
  for a specific message and get a future that completes when the message has been handled.
* `fun addMessageState(Message, Any)`: add a state (`Any`) to the specified message.
* `fun finishMessageState(Message)`: finish message state for the specified message,
  the future will complete with a list of states.
  
These methods are used by the `LocalCommandBus` and `LocalEventBus` to update
message state, and by message providers to get a handle of the message state.
`LocalCommandBus` and `LocalEventBus` use these methods to add an exception instance
(state) when an exception occured inside a handler, and finish message state when
a message has been handled.

If no handle has been retrieved for a specific message, message state will not be tracked
for that message. `LocalCommandBus` and `LocalEventBus` do not retrieve a handle hence
by default message state is not tracked.
To track message state a message provider has to call `getMessageState`.
A typical example:

```kotlin
val command = NewCommand(uuid)
val future = context.getMessageState(command.sequenceId)
context.handle(command)
val states = future.get()
... handle the states, if any ...
``` 

or inside a command handler:

```kotlin
@CommandHandler
fun handleNewCommand(command: NewCommand, context: CqrsContext [, more parameters]) {
    ... validate state ...
    val event = NewEvent(uuid, ...)
    val future = context.getMessageState(event.sequenceId)
    context.publish(event)
    val states = future.get()
    states.forEach { context.addMessageState(command.sequenceId, it) }
}

```

Above examples can be improved by not blocking when getting the future value, but are
only ment to explain message state tracking.

### Projections

TODO

### Queries

TODO

## Components

The CQRS model has the following components that can be used to configure an application:

* `LocalCommandBus`
* `LocalEventBus`
* `LocalEventStore`
* `InMemoryMessageQueue`
* `PersistentMessageQueue`
* `DelegatingEventStore`
* `InMemoryEventStorage`
* `FileEventStorage`
    * `EventFileReader`
    * `EventFileWriter`
    * `RotatingEventFileReader`
    * `RotatingEventFileWriter`
* `NoMemoryAggregateRebuilder`
* `InMemoryAggregateRebuilder`
* `SnapshotAggregateRebuilder`
* `DatabaseAggregateSnapshotRepository`
* `ProjectionDataRepository`
* `AggregateIdProjection`
* `AggregateSnapshotProjection`
* `AggregateIdQuery`

### CommandBus

#### LocalCommandBus

A `CommandBus` that dispatches `Commands` to `Aggregates` within the same application.

### EventBus

#### LocalEventBus

An `EventBus` that dispatches `Events` to `Aggregates` within the same application.

### EventStore

#### LocalEventStore

An `EventStore` that stores `Events` for this application only. It takes an
`EventStorage` and `AggregateProjection` as parameters.

### MessageQueue

#### InMemoryMessageQueue

A non persistent `MessageQueue`.

#### PersistentMessageQueue

A persistent `MessageQueue`.

### EventStorage

#### InMemoryEventStorage

A non persistent `EventStorage`.

#### FileEventStorage

A persistent `EventStorage`.

### AggregateRebuilder

#### NoMemoryAggregateRebuilder

Non caching `AggregateRebuilder`. An `Aggregate` is rebuilt every time it is
necessary.

#### InMemoryAggregateRebuilder

A caching - non persistent `AggregateRebuilder`. Aggregate state can be cached 
on demand.

#### SnapshotAggregateRebuilder

A persistent `AggregateRebuilder`. Aggregate state is persisted using a projection
that can be accessed by an `AggregateSnapshotRepository` when aggregate state needs
to be restored. Implementations of these are:
* DatabaseAggregateSnapshotProjection
* DatabaseAggregateSnapshotRepository

## Implementation

The repository is at: https://bitbucket.org/pukanito/cqrsesmodel/src/master/

Source code contains comments that should explain the functionality.

## Next steps

Next steps could be:

* Sagas,
* Separate reactors,
* Distributed event bus,
* ...

## Release notes

### 1.2.0

* Projections
    * `ProjectionDataRepository` (for keeping state about projections)
    * `AggregateIdProjection`
    * `AggregateSnapshotProjection`
* Queries
    * `AggregateIdQuery`
* `SnapshotAggregateRebuilder`

### 1.1.0

* Added `Message` and `MessageHandler` abstract classes.
* Seperation between `@EventHandler` and `@EventSourcingHandler`. 
    * `@EventHandler` only listens to events published on the `EventBus`.
    * `@EventSourcingHandler` only listens to events when a `MessageHandler` is rebuilt.
    * When an `@EventHandler` executes an event, that event has not yet been sourced to
    the aggregate (or reactor).
* Events are always stored in the event store, even if one or more event handlers
  throw exceptions.    
* Added message state to track exceptions inside message handlers.
* Big architectural refactoring/cleanup.  
* (Rotating) File event store.
* Persistent message queue

### 1.0.0

First release with only in memory functionality. Suited for gaining experience with
CQRS/ES.
