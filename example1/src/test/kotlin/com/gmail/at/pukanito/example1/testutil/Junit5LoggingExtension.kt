/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.testutil

import com.google.inject.AbstractModule
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import java.util.Properties
import java.util.logging.LogManager
import java.util.logging.Logger


/**
 * JUnit 5 logging extension to make java.util.logging work with logging.properties file.
 *
 * Necessary:
 * - junit-platform.properties with 'junit.jupiter.extensions.autodetection.enabled=true' for enabling autoregistration.
 * - META-INF/services/org.junit.jupiter.api.extension.Extension file for setting what to register.
 * - testutil/Junit5LoggingExtension (this class) that reads logging.properties.
 * - logging.properties
 * - logback-test.xml
 *
 *
 * Logging levels:
 *
 * - SEVERE  -> ERROR ->
 * - WARNING -> WARN  ->
 * - INFO    -> INFO  -> Command handling
 * - CONFIG  -> ??    -> not used
 * - FINE    -> DEBUG -> Event handling, Aggregate instantiation, Aggregate snapshot projection.
 * - FINER   -> DEBUG -> Event store/storage access.
 * - FINEST  -> TRACE -> Events replayed
 */
class Junit5LoggingExtension: BeforeAllCallback {

    override fun beforeAll(context: ExtensionContext?) {
        // Initialize java.util.logging configuration.
        val logManager = LogManager.getLogManager()
        val classLoader = AbstractModule::class.java.classLoader
        logManager.readConfiguration(classLoader.getResourceAsStream("logging.properties")!!)
        // Reset jUnit logger levels because Loggers already have been instantiated before reading the configuration.
        val configProperties = Properties()
        configProperties.load(classLoader.getResourceAsStream("logging.properties")!!)
        configProperties.propertyNames().toList().map { it.toString() }
            .filter { it.startsWith("org.junit") }
            .filter { it.endsWith(".level") }
            .map { it.substringBeforeLast(".level") }
            .forEach { Logger.getLogger(it) }
    }

}
