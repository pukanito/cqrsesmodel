/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.operation

import com.gmail.at.pukanito.cqrses.EventBus
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.example1.adapter.ChannelRepository
import com.gmail.at.pukanito.example1.model.Channel
import com.gmail.at.pukanito.example1.model.Channel.EMAIL
import com.gmail.at.pukanito.example1.model.Channel.MAIL
import com.gmail.at.pukanito.example1.model.Channel.MESSAGEBOX
import com.gmail.at.pukanito.example1.model.DEFAULT_CLIENT_ID
import com.gmail.at.pukanito.example1.model.Output
import com.gmail.at.pukanito.example1.model.OutputChannelEvent
import com.gmail.at.pukanito.example1.model.OutputRequest
import com.gmail.at.pukanito.example1.model.OutputRequestEvent
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argThat
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import java.util.Optional
import java.util.UUID

class OutputChannelOperationTest {

    private val id = UUID.randomUUID()
    private val output = Output(id)
    private val eventBus: EventBus = mock()
    private val context: CqrsContext = mock<CqrsContext>().also { whenever(it.eventBus) doReturn eventBus }
    private val channelRepository: ChannelRepository = mock()
    private val operation = OutputChannelOperation(context, channelRepository, mock())

    private fun setupMocks(channel: Channel?) {
        whenever(channelRepository.getChannel(any())) doReturn Optional.ofNullable(channel)
        whenever(eventBus.publish(argThat { this is OutputChannelEvent }))
            .doAnswer { output.handleOutputChannelEvent(it.getArgument(0)) }
    }

    @Test
    fun testDefaultChannelIsMail() {
        setupMocks(null)
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!")))
        operation.execute { output }
        assertThat(output.channel, equalTo(MAIL))
    }

    @Test
    fun testRequestChannelOverridesChannelRepository() {
        setupMocks(MESSAGEBOX)
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!", "EMAIL")))
        operation.execute { output }
        assertThat(output.channel, equalTo(EMAIL))
    }

    @Test
    fun testWithoutRequestChannelChannelRepositorySuppliesChannel() {
        setupMocks(MESSAGEBOX)
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!")))
        operation.execute { output }
        assertThat(output.channel, equalTo(MESSAGEBOX))
    }

}
