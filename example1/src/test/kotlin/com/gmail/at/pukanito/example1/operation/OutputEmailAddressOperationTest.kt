/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.operation

import com.gmail.at.pukanito.cqrses.EventBus
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.example1.adapter.EmailAddressRepository
import com.gmail.at.pukanito.example1.model.Channel.EMAIL
import com.gmail.at.pukanito.example1.model.DEFAULT_CLIENT_ID
import com.gmail.at.pukanito.example1.model.Output
import com.gmail.at.pukanito.example1.model.OutputChannelEvent
import com.gmail.at.pukanito.example1.model.OutputEmailAddressEvent
import com.gmail.at.pukanito.example1.model.OutputRequest
import com.gmail.at.pukanito.example1.model.OutputRequestEvent
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argThat
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.StringContains.containsString
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.Optional
import java.util.UUID

class OutputEmailAddressOperationTest {

    private val id = UUID.randomUUID()
    private val output = Output(id)
    private val eventBus: EventBus = mock()
    private val context: CqrsContext = mock<CqrsContext>().also { whenever(it.eventBus) doReturn eventBus }
    private val channelOperation: OutputChannelOperation = mock()
    private val emailAddressRepository: EmailAddressRepository = mock()
    private val operation = OutputEmailAddressOperation(context, channelOperation, emailAddressRepository, mock())

    private fun setupMocks(emailAddress: String?) {
        whenever(emailAddressRepository.getEmailAddress(any())) doReturn Optional.ofNullable(emailAddress)
        whenever(eventBus.publish(argThat { this is OutputEmailAddressEvent }))
            .doAnswer { output.handleOutputEmailAddressEvent(it.getArgument(0)) }
    }

    @Test
    fun testChannelNotEmailDoesNotExecuteOperation() {
        setupMocks(null)
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!", "MAIL")))
        operation.execute { output }
        verify(channelOperation).execute(any())
        verify(emailAddressRepository, never()).getEmailAddress(any())
    }

    @Test
    fun testChannelEmailWithEmailAddressDoesNotExecuteOperation() {
        setupMocks(null)
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!", "EMAIL")))
        output.handleOutputEmailAddressEvent(OutputEmailAddressEvent(id, "person@somewhere.domain"))
        operation.execute { output }
        verify(channelOperation).execute(any())
        verify(emailAddressRepository, never()).getEmailAddress(any())
        MatcherAssert.assertThat(output.emailAddress, equalTo("person@somewhere.domain"))
    }

    @Test
    fun testChannelNullWithoutEmailAddressAndEmailChannelOperationDoesExecuteOperation() {
        setupMocks("person@somewhere.domain")
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!")))
        whenever(channelOperation.execute(any())).doAnswer { output.handleOutputChannelEvent(OutputChannelEvent(id, EMAIL)) }
        operation.execute { output }
        verify(channelOperation).execute(any())
        verify(emailAddressRepository).getEmailAddress(any())
        MatcherAssert.assertThat(output.emailAddress, equalTo("person@somewhere.domain"))
    }

    @Test
    fun testChannelEmailWithoutEmailAddressDoesExecuteOperation() {
        setupMocks("person@somewhere.domain")
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!", "EMAIL")))
        operation.execute { output }
        verify(channelOperation).execute(any())
        verify(emailAddressRepository).getEmailAddress(any())
        MatcherAssert.assertThat(output.emailAddress, equalTo("person@somewhere.domain"))
    }

    @Test
    fun testChannelEmailWithoutMailAddressThrowsExceptionWhenRepositoryHasNoEmailAddress() {
        setupMocks(null)
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!", "EMAIL")))
        val exception = assertThrows<IllegalStateException> { operation.execute { output } }
        MatcherAssert.assertThat(exception.message, containsString("Email address not found"))
    }

}