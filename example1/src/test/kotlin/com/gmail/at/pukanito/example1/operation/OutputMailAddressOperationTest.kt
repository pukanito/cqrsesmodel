/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.operation

import com.gmail.at.pukanito.cqrses.EventBus
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.example1.adapter.MailAddressRepository
import com.gmail.at.pukanito.example1.model.Channel.MAIL
import com.gmail.at.pukanito.example1.model.DEFAULT_CLIENT_ID
import com.gmail.at.pukanito.example1.model.Output
import com.gmail.at.pukanito.example1.model.OutputChannelEvent
import com.gmail.at.pukanito.example1.model.OutputMailAddressEvent
import com.gmail.at.pukanito.example1.model.OutputRequest
import com.gmail.at.pukanito.example1.model.OutputRequestEvent
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argThat
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.core.StringContains.containsString
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.Optional
import java.util.UUID

class OutputMailAddressOperationTest {

    private val id = UUID.randomUUID()
    private val output = Output(id)
    private val eventBus: EventBus = mock()
    private val context: CqrsContext = mock<CqrsContext>().also { whenever(it.eventBus) doReturn eventBus }
    private val channelOperation: OutputChannelOperation = mock()
    private val mailAddressRepository: MailAddressRepository = mock()
    private val operation = OutputMailAddressOperation(context, channelOperation, mailAddressRepository, mock())

    private fun setupMocks(mailAddress: String?) {
        whenever(mailAddressRepository.getMailAddress(any())) doReturn Optional.ofNullable(mailAddress)
        whenever(eventBus.publish(argThat { this is OutputMailAddressEvent }))
            .doAnswer { output.handleOutputMailAddressEvent(it.getArgument(0)) }
    }

    @Test
    fun testChannelNotMailDoesNotExecuteOperation() {
        setupMocks(null)
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!", "EMAIL")))
        operation.execute { output }
        verify(channelOperation).execute(any())
        verify(mailAddressRepository, never()).getMailAddress(any())
    }

    @Test
    fun testChannelMailWithMailAddressDoesNotExecuteOperation() {
        setupMocks(null)
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!", "MAIL")))
        output.handleOutputMailAddressEvent(OutputMailAddressEvent(id, "Mail address"))
        operation.execute { output }
        verify(channelOperation).execute(any())
        verify(mailAddressRepository, never()).getMailAddress(any())
        assertThat(output.mailAddress, equalTo("Mail address"))
    }

    @Test
    fun testChannelNullWithoutMailAddressAndMailChannelOperationDoesExecuteOperation() {
        setupMocks("Mail address")
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!")))
        whenever(channelOperation.execute(any())).doAnswer { output.handleOutputChannelEvent(OutputChannelEvent(id, MAIL)) }
        operation.execute { output }
        verify(channelOperation).execute(any())
        verify(mailAddressRepository).getMailAddress(any())
        assertThat(output.mailAddress, equalTo("Mail address"))
    }

    @Test
    fun testChannelMailWithoutMailAddressDoesExecuteOperation() {
        setupMocks("Mail address")
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!", "MAIL")))
        operation.execute { output }
        verify(channelOperation).execute(any())
        verify(mailAddressRepository).getMailAddress(any())
        assertThat(output.mailAddress, equalTo("Mail address"))
    }

    @Test
    fun testChannelMailWithoutMailAddressThrowsExceptionWhenRepositoryHasNoMailAddress() {
        setupMocks(null)
        output.handleOutputRequestEvent(OutputRequestEvent(OutputRequest(id, DEFAULT_CLIENT_ID, "Hello world!", "MAIL")))
        val exception = assertThrows<IllegalStateException> { operation.execute { output } }
        assertThat(exception.message, containsString("Mail address not found"))
    }

}
