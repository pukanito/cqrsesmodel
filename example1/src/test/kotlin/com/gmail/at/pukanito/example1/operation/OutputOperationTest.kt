/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.operation

import com.gmail.at.pukanito.example1.model.Output
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test
import java.util.UUID

class OutputOperationTest {

    private val id = UUID.randomUUID()
    private val output = Output(id)

    @Test
    fun testOutputOperationExecutesRequirements() {
        val channelOperation: OutputChannelOperation = mock()
        val mailAddressOperation: OutputMailAddressOperation = mock()
        val emailAddressOperation: OutputEmailAddressOperation = mock()
        val operation = OutputOperation(mock(), channelOperation, mailAddressOperation, emailAddressOperation)
        operation.execute { output }
        verify(channelOperation).execute(any())
        verify(mailAddressOperation).execute(any())
        verify(emailAddressOperation).execute(any())
    }

}
