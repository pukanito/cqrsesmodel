/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1

import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.query.AggregatetypeIdQuery
import com.gmail.at.pukanito.example1.model.DEFAULT_CLIENT_ID
import com.gmail.at.pukanito.example1.model.Output
import com.gmail.at.pukanito.example1.model.OutputRequest
import com.google.inject.Guice
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.util.UUID

class Example1Test {

    @Suppress("unused")
    @Disabled
    fun testApp() {
        val context = Guice.createInjector(Module).getInstance(CqrsContext::class.java)
        val outputId = UUID.randomUUID()
        val outputRequest = OutputRequest(outputId, DEFAULT_CLIENT_ID, "Hello world.")
        context.commandBus.handle(outputRequest)
        context.eventStore.retrieve(Output::class, outputId)
    }

    @Test
    fun testManyCommands() {
        val context = Guice.createInjector(Module).getInstance(CqrsContext::class.java)
        repeat(1) {
            val outputId = UUID.randomUUID()
            val outputRequest = OutputRequest(outputId, DEFAULT_CLIENT_ID, "Hello world.")
            context.commandBus.handle(outputRequest)
            context.eventStore.retrieve(Output::class, outputId)
        }
        val query = context.getInstance(AggregatetypeIdQuery::class.java)
        var count = 0
        query.apply { _, _ ->  count++ }
        println(count)
    }

}
