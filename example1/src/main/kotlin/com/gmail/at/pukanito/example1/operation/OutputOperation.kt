/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.operation

import com.gmail.at.pukanito.example1.model.Output
import com.gmail.at.pukanito.process.Operation
import java.util.logging.Logger
import javax.inject.Inject

/**
 * [Operation] for building a complete [Output].
 *
 * ---------------------------------------------------------------------------------------
 * OutputRequest (Content: list of: message + template + notification template (optional))
 *   |
 *   V
 * getChannel (Strategie)
 *   |
 *   V
 * getMailAddress and/or getEmailAddress
 *   |
 *   V
 * render
 *   |
 *   V
 * distribute
 *   |
 *   V
 * notify (optional) and/or brengplicht (optional)
 * ---------------------------------------------------------------------------------------
 */
class OutputOperation @Inject constructor(logger: Logger,
                                          outputChannelOperation: OutputChannelOperation,
                                          outputMailAddressOperation: OutputMailAddressOperation,
                                          outputEmailAddressOperation: OutputEmailAddressOperation) :
    Operation<Output>("CreateOutput",
        logger,
        listOf(outputChannelOperation, outputMailAddressOperation, outputEmailAddressOperation),
        { false },
        { /* println(it) */ })

