/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.operation

import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.example1.adapter.EmailAddressRepository
import com.gmail.at.pukanito.example1.model.Channel.EMAIL
import com.gmail.at.pukanito.example1.model.Output
import com.gmail.at.pukanito.example1.model.OutputEmailAddressEvent
import com.gmail.at.pukanito.process.Operation
import java.util.logging.Logger
import javax.inject.Inject

/**
 * [Operation] for retrieving an output email address.
 */
class OutputEmailAddressOperation @Inject constructor(context: CqrsContext,
                                                      outputChannelOperation: OutputChannelOperation,
                                                      emailAddressRepository: EmailAddressRepository,
                                                      logger: Logger) :
    Operation<Output>("GetEmail",
        logger,
        listOf(outputChannelOperation),
        { it.channel != EMAIL || it.emailAddress != null },
        {   val emailAddress = emailAddressRepository.getEmailAddress(it.clientId)
                .orElseThrow { IllegalStateException("Email address not found") }
            logger.fine { emailAddress }
            context.eventBus.publish(OutputEmailAddressEvent(it.id, emailAddress)) })
