/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.operation

import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.example1.adapter.MailAddressRepository
import com.gmail.at.pukanito.example1.model.Channel.MAIL
import com.gmail.at.pukanito.example1.model.Output
import com.gmail.at.pukanito.example1.model.OutputMailAddressEvent
import com.gmail.at.pukanito.process.Operation
import java.util.logging.Logger
import javax.inject.Inject

/**
 * [Operation] for retrieving an output mail address.
 */
class OutputMailAddressOperation @Inject constructor(context: CqrsContext,
                                                     outputChannelOperation: OutputChannelOperation,
                                                     mailAddressRepository: MailAddressRepository,
                                                     logger: Logger) :
    Operation<Output>("GetMail",
        logger,
        listOf(outputChannelOperation),
        { it.channel != MAIL || it.mailAddress != null },
        {   val mailAddress = mailAddressRepository.getMailAddress(it.clientId)
                .orElseThrow { IllegalStateException("Mail address not found") }
            logger.fine { mailAddress }
            context.eventBus.publish(OutputMailAddressEvent(it.id, mailAddress)) })

