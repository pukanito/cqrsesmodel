/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.adapter

import com.gmail.at.pukanito.example1.model.ClientId
import com.gmail.at.pukanito.example1.model.MailAddress
import com.gmail.at.pukanito.example1.model.Output
import java.util.Optional

/**
 * Get a mail address for an [Output] based on clientId.
 */
interface MailAddressRepository {

    /**
     * Get the mail address for the specified clientId.
     *
     * @param clientId the client id.
     * @return a mail address Optional.
     */
    fun getMailAddress(clientId: ClientId): Optional<MailAddress>

}
