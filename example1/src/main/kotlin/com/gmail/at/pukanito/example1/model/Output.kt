/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.model

import com.gmail.at.pukanito.cqrses.Aggregate
import com.gmail.at.pukanito.cqrses.CommandHandler
import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.EventSourcingHandler
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.example1.operation.OutputOperation

/**
 * [Output] aggregate.
 *
 * @param id the [Output] id.
 */
@Suppress("unused")
class Output(id: DataId): Aggregate(id) {

    // The message to distribute.
    var message: Content = DEFAULT_CONTENT; private set
    // The id of the client.
    var clientId: ClientId = DEFAULT_CLIENT_ID; private set
    // The output channel to use (optional -> client communication preferences).
    var channel: Channel? = null; private set
    // The mail address to use for distribution (optional -> client data).
    var mailAddress: MailAddress? = null; private set
    // The email address to use for distribution (optional -> client data).
    var emailAddress: EmailAddress? = null; private set

    override fun toString(): String {
        return "Output(message='$message', clientId=$clientId, channel=$channel, mailAddress=$mailAddress, " +
                "emailAddress=$emailAddress) ${super.toString()}"
    }

    // Future extensions:
    // - isSensitiveMessge: Boolean? -> content may be distributed or not.
    // - hasBrengplicht: Boolean? -> brengplicht or not.
    // - messageboxAddress: MessageboxAddress? -> messagebox address.
    // - renderedContent: Content? -> rendered content.
    // - archiveDistributionId: UUID? -> when archived, the archive id.
    // - mailDistributionId: UUID? -> a distribution id, when distributed by mail.
    // - emailDistributionId: UUID? -> a distribution id, when distributed by email.
    // - messageboxDistributionId: UUID? -> a distribution id, when distributed by messagebox.
    // - notificationDistributionId: UUID? -> a distribution id, when distributed by notification.
    // - brengplichtId: UUID? -> an id, when brengplicht was applied.

    /**
     * Create the output.
     *
     * @param outputRequest a valid [OutputRequest].
     * @param context the [CqrsContext].
     * @param outputOperation [OutputOperation] to execute for creating the output.
     */
    @CommandHandler
    fun handleOutputRequest(outputRequest: OutputRequest, context: CqrsContext, outputOperation: OutputOperation) {
        context.eventBus.publish(OutputRequestEvent(outputRequest))
        outputOperation.execute { context.eventStore.retrieve(Output::class, outputRequest.id) }
    }

    @EventSourcingHandler
    fun handleOutputRequestEvent(event: OutputRequestEvent) {
        clientId = event.request.clientId
        channel = event.request.channel?.let { Channel.valueOf(it) }
        message = event.request.message
    }

    @EventSourcingHandler
    fun handleOutputChannelEvent(event: OutputChannelEvent) {
        channel = event.channel
    }

    @EventSourcingHandler
    fun handleOutputMailAddressEvent(event: OutputMailAddressEvent) {
        mailAddress = event.mailAddress
    }

    @EventSourcingHandler
    fun handleOutputEmailAddressEvent(event: OutputEmailAddressEvent) {
        emailAddress = event.emailAddress
    }

}
