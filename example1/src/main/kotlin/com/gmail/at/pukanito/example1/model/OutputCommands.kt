/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.model

import com.gmail.at.pukanito.cqrses.Command
import com.gmail.at.pukanito.cqrses.DataId

/**
 * Request for building an [Output].
 */
class OutputRequest(id: DataId,
                    val clientId: ClientId,
                    val message: Content,
                    val channel: String? = null): Command(id) {

    override fun toString(): String {
        return "OutputRequest(clientId=$clientId, message='$message', channel=$channel) ${super.toString()}"
    }

}
