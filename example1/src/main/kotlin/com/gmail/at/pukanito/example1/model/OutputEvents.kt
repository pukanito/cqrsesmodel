/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1.model

import com.gmail.at.pukanito.cqrses.DataId
import com.gmail.at.pukanito.cqrses.Event

/**
 * Event for updating the Output with the OutputRequest.
 */
class OutputRequestEvent(val request: OutputRequest): Event(request.id) {
    override fun toString(): String = "OutputRequestEvent(request=$request) ${super.toString()}"
}

/**
 * Event for updating the channel.
 */
class OutputChannelEvent(id: DataId, val channel: Channel): Event(id) {
    override fun toString(): String = "OutputChannelEvent(channel=$channel) ${super.toString()}"
}

/**
 * Event for updating the mail address.
 */
class OutputMailAddressEvent(id: DataId, val mailAddress: MailAddress): Event(id) {
    override fun toString(): String = "OutputMailAddressEvent(mailAddress='$mailAddress') ${super.toString()}"
}

/**
 * Event for updating the email address.
 */
class OutputEmailAddressEvent(id: DataId, val emailAddress: EmailAddress): Event(id) {
    override fun toString(): String = "OutputEmailAddressEvent(emailAddress='$emailAddress') ${super.toString()}"
}
