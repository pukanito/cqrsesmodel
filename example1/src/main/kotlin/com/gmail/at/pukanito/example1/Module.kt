/*
 * Copyright (C) 2020 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.example1

import com.gmail.at.pukanito.cqrses.AggregateClass
import com.gmail.at.pukanito.cqrses.Command
import com.gmail.at.pukanito.cqrses.CommandBus
import com.gmail.at.pukanito.cqrses.Event
import com.gmail.at.pukanito.cqrses.EventBus
import com.gmail.at.pukanito.cqrses.EventStore
import com.gmail.at.pukanito.cqrses.Position
import com.gmail.at.pukanito.cqrses.commandbus.LocalCommandBus
import com.gmail.at.pukanito.cqrses.eventbus.LocalEventBus
import com.gmail.at.pukanito.cqrses.eventstore.AggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.DelegatingEventStore
import com.gmail.at.pukanito.cqrses.eventstore.EventStorage
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateMessageReplayer
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.AggregateSnapshotQuery
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.InMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.NoMemoryAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.SnapshotAggregateRebuilder
import com.gmail.at.pukanito.cqrses.eventstore.aggregaterebuilder.aggregatesnapshotrepository.DatabaseAggregateSnapshotQuery
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorage
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorageReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.FileEventStorageWriter
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFile
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFileReader
import com.gmail.at.pukanito.cqrses.eventstore.eventstorage.file.RotatingEventFileWriter
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsConfiguration
import com.gmail.at.pukanito.cqrses.infrastructure.CqrsContext
import com.gmail.at.pukanito.cqrses.projection.AggregateSnapshotProjection
import com.gmail.at.pukanito.cqrses.projection.AggregatetypeIdProjection
import com.gmail.at.pukanito.cqrses.projection.ProjectionDataRepository
import com.gmail.at.pukanito.cqrses.projection.database.DatabaseProjectionDataRepository
import com.gmail.at.pukanito.cqrses.projection.database.DatabaseProjectionDataRepository.ProjectionDataTable
import com.gmail.at.pukanito.cqrses.projection.populator.AggregateSnapshotPopulator
import com.gmail.at.pukanito.cqrses.projection.populator.AggregatetypeIdPopulator
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregateSnapshotRepository.AggregateSnapshotTable
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregatetypeIdRepository
import com.gmail.at.pukanito.cqrses.projection.populator.database.DatabaseAggregatetypeIdRepository.AggregatetypeAndIdTable
import com.gmail.at.pukanito.cqrses.query.AggregatetypeIdQuery
import com.gmail.at.pukanito.cqrses.query.database.DatabaseAggregatetypeIdQuery
import com.gmail.at.pukanito.cqrses.util.MessageQueue
import com.gmail.at.pukanito.cqrses.util.messagequeue.PersistentMessageQueue
import com.gmail.at.pukanito.example1.adapter.ChannelRepository
import com.gmail.at.pukanito.example1.adapter.EmailAddressRepository
import com.gmail.at.pukanito.example1.adapter.MailAddressRepository
import com.gmail.at.pukanito.example1.model.Channel
import com.gmail.at.pukanito.example1.model.Channel.MAIL
import com.gmail.at.pukanito.example1.model.ClientId
import com.gmail.at.pukanito.example1.model.Output
import com.gmail.at.pukanito.example1.operation.OutputChannelOperation
import com.gmail.at.pukanito.example1.operation.OutputEmailAddressOperation
import com.gmail.at.pukanito.example1.operation.OutputMailAddressOperation
import com.gmail.at.pukanito.example1.operation.OutputOperation
import com.google.inject.AbstractModule
import com.google.inject.Provider
import com.google.inject.Provides
import com.google.inject.Singleton
import com.google.inject.TypeLiteral
import com.zaxxer.hikari.HikariDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import java.nio.file.Paths
import java.util.Optional
import java.util.logging.LogManager
import java.util.logging.Logger
import javax.inject.Inject
import javax.sql.DataSource


/**
 * TODO: projections:
 * - update AggregateIdProjection.
 * - take snapshots of aggregates.
 * - archive message queue -> save position before which all messages have been finished.
 * - archive event store -> by taking snapshots of ALL aggregates.
 */
@Suppress("TooManyFunctions") // All config in one file.
object Module: AbstractModule() {

    override fun configure() {
        // Initialize java.util.logging
        val stream = AbstractModule::class.java.classLoader.getResourceAsStream("logging.properties")!!
        LogManager.getLogManager().readConfiguration(stream)

        bind(CqrsContext::class.java).`in`(Singleton::class.java)
        bind(CommandBus::class.java).to(LocalCommandBus::class.java).`in`(Singleton::class.java)
        bind(EventBus::class.java).to(LocalEventBus::class.java).`in`(Singleton::class.java)
        bind(object: TypeLiteral<Function0<Position<*>?>>() {}).toInstance { null }
        bind(EventStore::class.java).to(DelegatingEventStore::class.java).`in`(Singleton::class.java)
        bind(EventStorage::class.java).to(FileEventStorage::class.java).`in`(Singleton::class.java)
        bind(AggregateMessageReplayer::class.java).`in`(Singleton::class.java)
        bind(AggregatetypeIdProjection::class.java).toProvider(DatabaseAggregatetypeIdProjectionProvider::class.java)
            .asEagerSingleton()
        bind(ProjectionDataRepository::class.java).to(DatabaseProjectionDataRepository::class.java)
            .`in`(Singleton::class.java)
        bind(AggregatetypeIdQuery::class.java).to(DatabaseAggregatetypeIdQuery::class.java).`in`(Singleton::class.java)
        bind(AggregateSnapshotQuery::class.java).to(DatabaseAggregateSnapshotQuery::class.java)
            .`in`(Singleton::class.java)
        bind(AggregateSnapshotProjection::class.java).toProvider(DatabaseAggregateSnapshotProjectionProvider::class.java)
            .asEagerSingleton()
        bind(OutputOperation::class.java).`in`(Singleton::class.java)
        bind(OutputChannelOperation::class.java).`in`(Singleton::class.java)
        bind(OutputMailAddressOperation::class.java).`in`(Singleton::class.java)
        bind(OutputEmailAddressOperation::class.java).`in`(Singleton::class.java)
        bind(ChannelRepository::class.java).toInstance(object : ChannelRepository {
            override fun getChannel(clientId: ClientId): Optional<Channel> = Optional.of(MAIL) })
        bind(MailAddressRepository::class.java).toInstance(object : MailAddressRepository {
            override fun getMailAddress(clientId: ClientId): Optional<String> = Optional.of("MAIL address") })
        bind(EmailAddressRepository::class.java).toInstance(object : EmailAddressRepository {
            override fun getEmailAddress(clientId: ClientId): Optional<String> =
                Optional.of("client@somewhere.domain") })
    }

    @Provides @Singleton
    fun provideCqrsConfiguration(): CqrsConfiguration {
        val resource = Module::class.java.classLoader.getResource("cqrses-config.yaml")!!
        return CqrsConfiguration.load(Paths.get(resource.toURI()).toAbsolutePath().toString())
    }

    @Provides @Singleton
    fun provideAggregates(): Set<AggregateClass> = setOf(Output::class)

    // Guice does not set @Provides injected loggers to the correct class. Do it manually.
    // Must be lazy otherwise the Logger does not get a log level from the configuration done in configure().
    private val inMemoryAggregateRebuilderLogger by lazy {
        Logger.getLogger(InMemoryAggregateRebuilder::class.qualifiedName) }
    private val snapshotAggregateRebuilderLogger by lazy {
        Logger.getLogger(SnapshotAggregateRebuilder::class.qualifiedName) }
    private val noMemoryAggregateRebuilderLogger by lazy {
        Logger.getLogger(NoMemoryAggregateRebuilder::class.qualifiedName) }

    @Provides @Singleton
    fun provideAggregateRebuilder(replayer: AggregateMessageReplayer, config: CqrsConfiguration,
                                  aggregateSnapshotQuery: AggregateSnapshotQuery): AggregateRebuilder =
        config.aggregateCache.let {
            InMemoryAggregateRebuilder(replayer, it.maximumSize, it.expireAfterWriteSeconds,
                SnapshotAggregateRebuilder(replayer, aggregateSnapshotQuery,
                    NoMemoryAggregateRebuilder(replayer, noMemoryAggregateRebuilderLogger),
                    snapshotAggregateRebuilderLogger),
                inMemoryAggregateRebuilderLogger) }

    @Provides @Singleton
    fun provideRotatingEventFile(config: CqrsConfiguration): RotatingEventFile =
        config.eventFileStorage.rotation.let{ RotatingEventFile(it.directory, it.size) }

    @Provides @Singleton
    fun provideFileEventStorageReader(rotatingEventFile: RotatingEventFile): FileEventStorageReader =
        RotatingEventFileReader(rotatingEventFile, Dispatchers.IO)

    @Provides @Singleton
    fun provideFileEventStorageWriter(rotatingEventFile: RotatingEventFile): FileEventStorageWriter =
        RotatingEventFileWriter(rotatingEventFile)

    private val commandQueueFileStorageLogger by lazy {
        Logger.getLogger("com.gmail.at.pukanito.cqrses.util.MessageQueue<Command>") }
    private val eventQueueFileStorageLogger by lazy {
        Logger.getLogger("com.gmail.at.pukanito.cqrses.util.MessageQueue<Event>") }

    @Provides @Singleton
    fun provideMessageQueueForCommands(config: CqrsConfiguration): MessageQueue<Command> =
        config.commandQueueFileStorage.rotation.let { RotatingEventFile(it.directory, it.size).let { ref ->
            PersistentMessageQueue(FileEventStorage(RotatingEventFileReader(ref, Dispatchers.IO),
                RotatingEventFileWriter(ref),
                commandQueueFileStorageLogger)) } }

    @Provides @Singleton
    fun provideMessageQueueForEvents(config: CqrsConfiguration): MessageQueue<Event> =
        config.eventQueueFileStorage.rotation.let { RotatingEventFile(it.directory, it.size).let { ref ->
            PersistentMessageQueue(FileEventStorage(RotatingEventFileReader(ref, Dispatchers.IO),
                RotatingEventFileWriter(ref),
                eventQueueFileStorageLogger)) } }

    @Provides @Singleton
    fun provideDatabase(dataSource: DataSource): Database = Database.connect(dataSource).also {
        transaction(it) { SchemaUtils.createMissingTablesAndColumns(
            ProjectionDataTable, AggregatetypeAndIdTable, AggregateSnapshotTable) } }

    @Provides @Singleton
    fun provideDatasource(config: CqrsConfiguration): DataSource = HikariDataSource().apply {
        jdbcUrl = config.database.jdbcUrl
        driverClassName = config.database.jdbcDriverClassName
        minimumIdle = config.database.maximumIdle
        maximumPoolSize = config.database.maximumPoolSize }

    private class DatabaseAggregatetypeIdProjectionProvider @Inject constructor(
            val projectionDataRepository: DatabaseProjectionDataRepository,
            val context: CqrsContext,
            val database: Database): Provider<AggregatetypeIdProjection> {
        @FlowPreview
        override fun get(): AggregatetypeIdProjection {
            val position = context.eventStore.getPosition()
            val config = projectionDataRepository.load()
            val projection = AggregatetypeIdPopulator(context, DatabaseAggregatetypeIdRepository(database))
            projection.update(config.aggregatetypeIdProjectionPosition)
            config.aggregatetypeIdProjectionPosition = position
            projectionDataRepository.save(config)
            return projection
        }
    }

    private class DatabaseAggregateSnapshotProjectionProvider @Inject constructor(
            val projectionDataRepository: DatabaseProjectionDataRepository,
            val context: CqrsContext,
            val database: Database,
            val logger: Logger): Provider<AggregateSnapshotProjection> {
        @FlowPreview
        override fun get(): AggregateSnapshotProjection {
            val position = context.eventStore.getPosition()
            val config = projectionDataRepository.load()
            val projection =
                AggregateSnapshotPopulator(context, DatabaseAggregateSnapshotRepository(context, database, logger))
            projection.update(config.aggregateSnapshotProjectionPosition)
            config.aggregateSnapshotProjectionPosition = position
            projectionDataRepository.save(config)
            return projection
        }
    }

}
