/*
 * Copyright (C) 2019 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    id("io.gitlab.arturbosch.detekt")
    maven
}

group = "com.gmail.at.pukanito"
version = "1.0.0-SNAPSHOT"

repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("com.gmail.at.pukanito:cqrsesmodel:1.2.0-SNAPSHOT")
    implementation("com.google.inject:guice:4.2.2")
    implementation("org.jetbrains.exposed", "exposed-core", "0.21.1")
    implementation("org.jetbrains.exposed", "exposed-jdbc", "0.21.1")
    implementation("org.jetbrains.exposed", "exposed-java-time", "0.21.1")
    implementation("com.zaxxer", "HikariCP", "3.4.2")
    implementation("com.h2database:h2:1.4.199")
    implementation("org.slf4j:jul-to-slf4j:1.7.25")
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("ch.qos.logback:logback-core:1.2.3")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.4.0")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.4.0")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher:1.4.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.4.0")
    testRuntimeOnly("org.junit.vintage:junit-vintage-engine:5.4.0")
    testImplementation("org.awaitility:awaitility:3.1.6")
    testImplementation("org.awaitility:awaitility-kotlin:3.1.6")
    testImplementation("org.mockito:mockito-core:2.25.0")
    testImplementation("org.exparity:hamcrest-date:2.0.5")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
    withJavadocJar()
    withSourcesJar()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
}

tasks {
    test {
        useJUnitPlatform()
        testLogging {
            showExceptions = false
            showStackTraces = false
            showStandardStreams = true
        }
    }
}

detekt {
    input = files("$projectDir/src/main/kotlin")
    config = files ("$projectDir/detekt.yml")
}
