# Roadmap

* AggregateSnapshotPopulator::update once in a while and 
  update ProjectionData::aggregateSnapshotProjectionPosition.
* Commandbus and Eventbus: MessageRegulator, handle messages
  with different id parallel/concurrently, with same id
  serialised.
* Detect deadlock in event handlers and command handlers:
  command/event publishes other command/event and waits for it
  while it cannot be handled asynchronously.

